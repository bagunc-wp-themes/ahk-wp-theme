<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ) ?>" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	
	<link rel="profile" href="https://gmpg.org/xfn/11" />
	
	<?php wp_head(); ?>
</head>
<body <?php body_class() ?>>
<?php wp_body_open() ?>

<?php require__once('includes/svgs.php') ?>

<div class="wrapper">
	<div class="wrapper__menu">
		<div class="menu">
			<div class="overlay"></div>
			<div class="menu__header d-flex justify-content-between align-items-center">
				<div class="d-flex align-items-center">
					<a href="#" id="menu-toggle" class="header__icon">
						<svg class="svg svg--burger">
							<use xlink:href="#burger"></use>
						</svg>
					</a>
					<div class="header__logo">
						<a href="<?php print home_url("/") ?>">
							<?php print site_logo() ?>
						</a>
					</div>
				</div>
				<div class="header__lang d-flex align-items-center">
					<?php print site_languages(); ?>
				</div>
			</div>
			<div class="menu__body ps ps--active-y">
				<?php wp_nav_menu([
					"menu" => "primary-left",
					"menu_id" => "primary-left",
					"theme_location" => "primary-left",
					
					"container" => "nav",
					"container_id" => "main-nav",
					"container_class" => "menu__navigation",
					
					"link_before"    => '%menu-icon%',
				]) ?>
				<div class="menu__footer">
					<?php if (!is_blog()) :
						$latest__posts = new WP_Query([
							"post_type" => "post",
							"post_status" => "publish",
							
							"order" => "DESC",
							"lazyloop" => FALSE,
							"posts_per_page" => 6,
						]);
						
						if ($latest__posts->have_posts()) : ?>
							<h4 class="footer__heading"><?php _e('Source-press', TMP__LANG) ?></h4>
							<div class="footer__content"> <?php
									$component__post = AFHPComponent::import('post', (object)[
										"template" => "thin",
										"output" => "return"
									]);
									
									while ($latest__posts->have_posts()) : $latest__posts->the_post();
										printf('<div class="mb-2">%s</div>', $component__post->render([
											"date" => get_the_date(),
											"title" => get_the_title(),
											"url" => get_the_permalink(),
											"excerpt" => get_the_excerpt(),
											"thumbnail" => get_the_post_thumbnail_url(),
										]));
									endwhile;
									wp_reset_postdata(); ?>
							</div>
						<?php endif;
					
					else :
						$ad = AFHPComponent::import('ad');
						$ad->render();
					endif; ?>
				</div>
			</div>
		</div>
	</div>
	<div class="wrapper__content">
<?php
	if (is_blog()) :
		
		$yandex_weather = get_option(tmp__prefix("yandex__weather"));
		$cba_currencies = get_option(tmp__prefix("cba__currencies"));
		if (isset($cba_currencies->data))
			$cba_currencies = $cba_currencies->data;
		
		$weather = (object)[
			"day" => 0,
			"evening" => 0,
			
			"icon" => NULL,
		];
		
		if (!empty($yandex_weather))
			if (isset($yandex_weather->data))
				$weather = (object)$yandex_weather->data;
		
		$blog_ID = get_option( 'page_for_posts' );
		
		AFHPComponent::import("blogheader")->render([
			"title" => get_the_title($blog_ID),
			"url" => get_the_permalink($blog_ID),
			"weather" => $weather,
			"exchange" => $cba_currencies
		]);
		
		AFHPSection::include("default", (object)[
			"class" => "section--advertising"
		], TRUE)->output(AFHPComponent::import("ad", (object)[
			"output" => "return"
		])->render());
	
	endif;