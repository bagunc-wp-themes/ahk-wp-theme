<?php
/**
 * Template Name:  Donation
 */

get_header();

	while (have_posts()) : the_post();

		get_template_part("template-parts/content", get_post_type());

		AFHPSection::include("donate")->output();

	endwhile;

get_footer();
