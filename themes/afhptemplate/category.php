<?php

global $wp_query;

get_header();
	
	$query_object = get_queried_object();

	$section = [];
  $sticky = get_option("sticky_posts");
	$term_slug = get_queried_object()->slug;
	
	$sticky = new WP_Query([
		"post_type" => "post",
		"post_status" => "publish",
		
		"lazyloop" => FALSE,
		"posts_per_page" => -1,
		
		"post__in" => get_option("sticky_posts"),
		
		"tax_query" => [
			[
				"field" => "term_id",
				"terms" => $query_object->term_id,
				"taxonomy" => $query_object->taxonomy,
			],
		],
	]);
	
	$has__sticky = $sticky->have_posts();
	
	if ($has__sticky)
		$section[] = AFHPSection::include("default", (object)[
			"output" => "return",
			"class" => [
				"section--categories",
				"section--blog-white",
			],
			
			"wrapper" => '<div class="row">%s</div>',
		])->output(function() use ($sticky, $query_object) {
			$output = [];
		
			/**
			 * @var AFHPComponentPost $post
			 */
			$post = AFHPComponent::import("post", (object)[
				"output" => "return",
				"template" => "thumbnail",
				"wrapper" => '<div class="col-12 col-sm-6 col-xl-4 mb-4">%s</div>',
			]);
			
			while ($sticky->have_posts()) : $sticky->the_post();
				$output[] = $post->render();
			endwhile;
			
			wp_reset_postdata();
			
			return implode("", $output);
		});
	
	
	$lazyloop__classnames = [];
	if (!$has__sticky)
		$lazyloop__classnames[] = "pt-0";
	
	$section[] = AFHPSection::include("lazyloop", (object)[
		"output" => "return",
		"buttonTheme" => "primary",
		"class" => $lazyloop__classnames,
		"node" => "#category-$term_slug-posts",
		"wrapper" => '<div id="category-' . $term_slug . '-posts" class="row">%s</div>',
		
		"component" => (object)[
			"name" => "post",
			"config" => (object)[
				"template" => "thin",
				"classnames" => "bg-white",
				"wrapper" => '<div class="col-md-6 col-lg-4 mb-4">%s</div>',
			],
		],
	])->output();
	
	AFHPSection::include("default", (object)[
		"class" => "section--content p-0"
	])->output(sprintf(
		'<div class="row">
				<div class="col-12 col-xl-9 col--main">%s</div>
				<div class="col-12 col-xl-3 col--main">%s</div>
			</div>',
		implode("", $section),
		blog__sidebar()
	));

get_footer();