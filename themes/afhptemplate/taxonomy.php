<?php
/**
 * @var string $term
 * @var string $taxonomy
 */
	
get_header();
	
	$post_type = get_post_type();
	$term_data = get_term_by("slug", $term, $taxonomy);
	
	if (AFHPCustomPostTypes::native($post_type)) :
	
		$thumbnail = get_term_meta($term_data->term_id, "thumbnail", TRUE);
		$thumbnail = wp_get_attachment_image_url($thumbnail, 'full');
		
		if (!$thumbnail)
			$thumbnail = tmp__customize("{$post_type}__banners", NULL);
		
		$banner = AFHPComponent::import('banner');
		$banner->render([
			"thumbnail" => $thumbnail,
			"title" => $term_data->name,
			"breadcrumbs" => get_breadcrumbs_map(),
		]);
		
	endif;
	
	set_query_var('term_data', $term_data);
	get_template_part("template-parts/taxonomies", $term_data->taxonomy);

get_footer();