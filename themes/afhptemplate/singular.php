<?php
get_header();
	
	$post_type = get_post_type();
	
  while (have_posts()) : the_post();
	
	  if (!in_array($post_type, ["gallery", "playlist"]))
	    if (AFHPCustomPostTypes::native($post_type))
		    AFHPComponent::import("banner")->render([
			    "title" => get_the_title(),
			    "breadcrumbs" => get_breadcrumbs_map(),
			    "thumbnail" => get_the_post_thumbnail_url(),
		    ]);
    
    get_template_part('template-parts/content', get_post_type());

  endwhile;

get_footer();
