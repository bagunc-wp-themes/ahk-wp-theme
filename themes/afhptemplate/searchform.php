<div class="header__search">
	<form role="search"
        method="get"
        id="searchform"
        class="searchform"
        action="<?php echo esc_url( home_url( '/' ) ); ?>">
		<div class="search__input">
			<label class="w-100">
				<input  name="s"
								type="text"
								placeholder="<?php _e("Search", TMP__LANG) ?>"
								class="search__input"
								value="<?php echo get_search_query() ?>"
							/>
			</label>
			<button class="input__button" type="submit">
				<svg class="svg svg--search">
					<use xlink:href="#search"></use>
				</svg>
			</button>
		</div>
	</form>
</div>