<?php print '</div>' ?>
<?php print '</div>' ?>
	<footer class="footer" id="footer">
		<div class="footer__top">
			<div class="container">
				<div class="row row--footer justify-content-center">
					<div class="col-auto col--footer">
						<a class="logo" href="<?php home_url("/"); ?>">
							<?php print site_logo(); ?>
							<h1 class="logo__text">
								<?php print str_replace(
									" ",
									"<br />",
									get_bloginfo("name")
								); ?>
							</h1>
						</a>
						<div class="contact">
							<?php
								$info = get_site_info();
								foreach ($info as $k => $i)
									printf(
										'<div class="contact__%s">
                        %s:
                        <span class="contact__text">%s</span>
                    </div>',
										$k,
										$i["label"],
										$i["value"]
									);
							?>
						</div>
						<div class="social">
							<?php print site_social_links(); ?>
						</div>
					</div>
					<div class="col-12 col-xl-auto order-3 order-xl-2 col--footer col--location">
						<div class="heading"><?php _e("We are here", TMP__LANG) ?></div>
						<div class="location">
							<?php print we_are_hear(); ?>
						</div>
					</div>
					<div class="col-auto order-xl-3 order-2 col--footer">
						<div class="heading"><?php _e("We look forward to hearing from you", TMP__LANG) ?></div>
						<form class="contact-form">
							<div class="form-row">
								<label class="col">
									<input  type="text"
									        placeholder="<?php _e("Name", TMP__LANG) ?> *"
									/>
								</label>
								<label class="col">
									<input  type="text"
									        placeholder="<?php _e("Surname", TMP__LANG) ?> *"
									/>
								</label>
							</div>
							<div class="form-row mt-2">
								<label class="col">
									<input  type="text"
									        class="w-100"
									        placeholder="<?php _e("E-mail / Tel.", TMP__LANG) ?> *"
									/>
								</label>
							</div>
							<div class="form-row mt-2">
								<label class="col">
									<textarea class="w-100" name="" rows="3" placeholder="<?php _e("Text...", TMP__LANG) ?>"></textarea>
								</label>
							</div>
							<div class="form-row mt-2">
								<div class="col"></div>
								<div class="col d-flex justify-content-end">
									<button class="button"><?php _e("Send", TMP__LANG) ?></button>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
		<div class="footer__middle">
			<div class="container">
				<div class="row justify-content-center"> <?php
						if (function_exists("dynamic_sidebar"))
							if (is_active_sidebar("footer__middle"))
								dynamic_sidebar("footer__middle"); ?>
				</div>
			</div>
		</div>
		<div class="footer__bottom">
			<div class="container">
				<div class="row justify-content-center">
					<div class="col">
						<div class="copyright">
							<div class="copyright__text"><?php _e("All rights reserved © azathayreniq.am", TMP__LANG) ?></div>
							<div class="copyright__made-by">
								<?php _e("Created by", TMP__LANG) ?>
								<a href="//sky-tech.org/" target="_blank">SkyTech Studio</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</footer>
<?php require__once("includes/templates.php");
	wp_footer();
	print '</body>';
	print '</html>';