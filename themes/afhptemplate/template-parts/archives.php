<?php
	global $wp_query;

	get_header();

	$section = AFHPSection::include("lazyloop", (object)[
		"class" => "pt-0",
		"output" => "return",
		"buttonTheme" => "primary",
		"node" => "#archive-list-posts",
		"wrapper" => '<div id="archive-list-posts" class="row">%s</div>',

		"component" => (object)[
			"name" => "post",
			"config" => (object)[
				"template" => "thin",
				"classnames" => "bg-white",
				"wrapper" => '<div class="col-md-6 col-lg-4 mb-4">%s</div>',
			],
		],
	])->output();

	AFHPSection::include("default", (object)[
		"class" => "section--content p-0"
	])->output(sprintf(
		'<div class="row">
				<div class="col-12 col-xl-9 col--main">%s</div>
				<div class="col-12 col-xl-3 col--main">%s</div>
			</div>',
		$section,
		blog__sidebar()
	));

	get_footer();
