<?php
	AFHPSection::include("lazyloop", (object)[
		"theme" => "primary",
		"buttonTheme" => "primary",
		"node" => "#gallery-wrapper",
		"wrapper" => '<div id="gallery-wrapper" class="gallery gallery--video mt-4">%s</div>',
		
		"component" => (object)[
			"name" => "gallery",
			"config" => (object)[
				"template" => "video",
			],
		],
	])->output();