<?php
	$post_type = get_post_type();

  AFHPSection::include("albums", (object)[
	  "theme" => "primary",
	  
	  "query" => [
		  "hide_empty" => TRUE,
	  	"post_type" => $post_type,
	  	"taxonomy" => get_object_taxonomies($post_type),
	  ],
	  
	  "heading" => (object)[
	  	"output" => AFHPComponent::import("tabs", (object)[
	  		"output" => "return",
			  "wrapper" => '<div class="heading">%s</div>',
		  ])->render(get_gallery_tabs_map()),
	  ],
  ])->output();
    
  AFHPSection::include("lazyloop", (object)[
  	"theme" => "white",
    "buttonTheme" => "secondary",
  	
  	"node" => "#gallery-wrapper",
    "wrapper" => sprintf(
    	'%s <div id="gallery-wrapper" class="gallery gallery--photo mt-4">%%s</div>',
	    AFHPComponent::import("separator", (object)[
	    	"output" => "return"
	    ])->render([
	    	"color" => "#D44C32"
	    ])
    ),
  	
  	"component" => (object)[
		  "ajax" => TRUE,
  		"name" => "gallery",
		  "config" => (object)[
		  	"template" => "photo",
		  ]
	  ]
  	
  ])->output();