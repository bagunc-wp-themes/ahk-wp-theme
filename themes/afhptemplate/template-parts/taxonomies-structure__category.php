<?php
/**
 * @var string $term
 * @var string $taxonomy
 * @var WP_Term $term_data
 */

AFHPSection::include("navigation", (object)[
	"taxonomy" => $taxonomy,
	"parent" => $term_data->parent,
	"term_id" => $term_data->term_id,
])->output();

AFHPSection::include("lazyloop", (object)[
	"node" => "#structures-tax-posts",
	"class" => [
		"section--structure-posts",
		"section--activity-{$term}",
	],
	
	"buttonTheme" => "primary",
	"wrapper" => '<div id="structures-tax-posts" class="row">%s</div>',
	
	"component" => (object)[
		"name" => "post",
		"config" => (object)[
			"label" => FALSE,
			"wrapper" => '<div class="col-12 col-lg-6 mb-4">%s</div>'
		]
	],
])->output();