<?php
/**
 * @var string $term
 * @var string $taxonomy
 * @var WP_Term $term_data
 */
	
AFHPSection::include("lazyloop", (object)[
	"node" => "#activities-tax-posts",
	"class" => [
		"section--activities",
		"section--activity-{$term}",
	],
	
	"buttonTheme" => "primary",
	"wrapper" => '<div id="activities-tax-posts" class="row">%s</div>',
	
	"component" => (object)[
		"name" => "post",
		"config" => (object)[
			"template" => "thumbnail",
			"wrapper" => '<div class="col-xl-3 col-lg-4 col-6 mb-4">%s</div>'
		]
	],
])->output();