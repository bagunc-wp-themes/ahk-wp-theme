<?php
AFHPComponent::import("banner")->render([
	"title" => get_the_title(),
	"breadcrumbs" => get_breadcrumbs_map(),
	"thumbnail" => get_the_post_thumbnail_url(),
]);

AFHPSection::include("default", (object)[
	"class" => "section--content overflow-hidden",
	"wrapper" => '<article class="article">%s</article>',
])->output(function() {
	the_content();
});