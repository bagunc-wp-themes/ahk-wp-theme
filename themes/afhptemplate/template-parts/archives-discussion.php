<?php

$section = AFHPSection::include("lazyloop", (object)[
	"node" => "#discussion-tax-posts",
	"wrapper" => '<div id="discussion-tax-posts" class="row">%s</div>',
	
	"component" => (object)[
		"name" => "discussion",
		"config" => (object)[
			"wrapper" => '<div class="col-12 col-lg-6 mb-4">%s</div>'
		]
	]
]);

$section->output();