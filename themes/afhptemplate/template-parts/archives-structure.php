<?php
$taxonomies = get_object_taxonomies(get_post_type());
$terms = get_terms([
	"hide_empty" => TRUE,
	"taxonomy" => $taxonomies,
	
	"order" => "ASC",
	"orderby" => "term_id",
	
	"meta_query" => [
		[
			"value" => 1,
			"key" => "show_in_main",
		]
	],
]);

AFHPSection::include("default", (object)[
	"class" => "section--structure"
])->output(function() use ($terms) {
	foreach ($terms as $index => $term) :
		$structure = AFHPComponent::import('structure', (object)[
			"style" => !($index % 2) ? "default" : "alt"
		]);
		
		$thumbnail = get_term_meta($term->term_id, 'thumbnail', TRUE);
		$thumbnail = wp_get_attachment_image_url($thumbnail, "full");
		
		$childes = get_terms([
			"hide_empty" => TRUE,
			"taxonomy" => "structure__category",
			
			"orderby" => "term_id",
			
			"parent" => $term->term_id
		]);
		
		$childes = array_map(function($item) {
			
			return (object)[
				"text" => $item->name,
				"url" => get_term_link($item, "structure__category"),
			];
		}, $childes);
		
		$structure->render([
			"childes" => $childes,
			"title" => $term->name,
			"thumbnail" => $thumbnail,
			"url" => get_term_link($term, "structure__category"),
		]);
	endforeach;
});