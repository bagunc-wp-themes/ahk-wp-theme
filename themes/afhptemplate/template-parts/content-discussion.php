<?php
	$hide_voting = (int)get_post_meta(get_the_ID(), "voting__hide", TRUE);
	if (!$hide_voting)
		AFHPSection::include("voting", NULL, (object)[
			"voted" => NULL,
			"ID" => get_the_ID(),
			"no" => (int)get_post_meta(get_the_ID(), "voting__no", TRUE),
			"yes" => (int)get_post_meta(get_the_ID(), "voting__yes", TRUE),
		])->output();
	
	AFHPSection::include("default", (object)[
		"class" => "section--discussion-single",
		"wrapper" => '<article class="article article--discussion">%s</article>',
	])->output(get_the_content());
	
