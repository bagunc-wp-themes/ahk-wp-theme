<?php
$taxonomies = get_object_taxonomies(get_post_type());
$terms = get_terms([
	"hide_empty" => TRUE,
	"taxonomy" => $taxonomies,
]);

$teams = AFHPSection::include("teams", (object)[
	"heading" => [
		"color" => "#B355AD",
		"text" => __("Group composition", TMP__LANG),
	]
]);

$sections = [];
if (!empty($terms))
	foreach ($terms as $index => $term)
		if ($term instanceof WP_Term)
			$sections[] = AFHPSection::include("activities", (object)[
				"term" => $term,
				"heading" => [
					"text" => $term->name,
					"color" => get_term_meta($term->term_id, "color", TRUE)
				]
			]);

if (!empty($teams->result))
	array_splice($sections, 1, 0, [$teams]);

foreach ($sections as $index => $section) {
	if (($index % 2))
		$section->addClass("bg-white");
	
	$section->output();
}