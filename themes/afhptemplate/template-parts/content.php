<?php
AFHPSection::include("default", (object)[
	"class" => "section--content",
])->output(function() {
	the_content();
});