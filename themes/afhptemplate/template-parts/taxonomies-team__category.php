<?php
/**
 * @var string $term
 * @var string $taxonomy
 * @var WP_Term $term_data
 */

AFHPSection::include("navigation", (object)[
	"taxonomy" => $taxonomy,
	"parent" => $term_data->parent,
	"term_id" => $term_data->term_id,
	"itemClassnames" => "col-sm-3 col-auto mb-3",
])->output();

$sections[] = AFHPSection::include("lazyloop", (object)[
	"node" => "#teams-tax-posts",
	
	"buttonTheme" => "primary",
	"wrapper" => '<div id="teams-tax-posts" class="row justify-content-center">%s</div>',
	
	"component" => (object)[
		"name" => "team",
		"config" => (object)[
			"wrapper" => '<div class="col-xl-3 col-md-4 col-sm-6 col-auto mb-4">%s</div>'
		],
	],
])->output();