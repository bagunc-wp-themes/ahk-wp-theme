<?php
	$sections = [];
	
	$parent = 0;
	$term_id = 0;
	$taxonomy = NULL;
	$taxonomies = get_object_taxonomies(get_post_type());
	
	if (!empty($taxonomies))
		$taxonomy = $taxonomies[0];
	
	$terms = wp_get_post_terms(get_the_ID(), $taxonomy);
	
	if (!empty($terms)) {
		if ($terms[0] instanceof WP_Term) {
			$parent = $terms[0]->parent;
			$term_id = $terms[0]->term_id;
		}
	}
	
	$section[] = AFHPSection::include("navigation", (object)[
		"output" => "return",
		
		"parent" => $parent,
		"term_id" => $term_id,
		"taxonomy" => $taxonomy,
	])->output();

	$section[] = AFHPSection::include("default", (object)[
		"output" => "return",
		
		"class" => "section--content",
		"wrapper" => '<article class="article">%s</article>'
	])->output(function() {
		the_content();
	});
	
	print implode("", $section);