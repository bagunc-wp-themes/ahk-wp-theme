<?php
	$post_type = get_post_type();
	
	$youtube__id = get_post_meta(get_the_ID(), "youtube__url", TRUE);
	
	AFHPComponent::import("banner", (object)[
		"style" => "thin",
	])->render([
		"breadcrumbs" => get_breadcrumbs_map(),
		"thumbnail" => tmp__customize("{$post_type}__banners", NULL),
	]);

	AFHPSection::include("default", (object)[
		"class" => [
			"overflow-hidden",
		],
	])->output(function() use ($youtube__id) {
		printf(
			'<article class="article article--gallery">
				<div class="container">
					<div class="row justify-content-center">
						<div class="col-md-10 col-12">
							<div class="gallery__thumbnail gallery__thumbnail--video">
								<iframe src="https://www.youtube.com/embed/%s" width="100%%" height="100%%" frameborder="0"></iframe>
							</div>
							<div class="gallery__content">
								<h2 class="h2 gallery__heading">%s</h2>
								%s
								<date class="gallery__date">%s</date>
							</div>
						</div>
					</div>
				</div>
			</article>',
			$youtube__id,
			get_the_title(),
			(function() {
				ob_start();
				the_content();
				return ob_get_clean();
			})(),
			get_the_date()
		);
	});
	
	$taxonomies = get_object_taxonomies($post_type);
	
	if (!empty($taxonomies)) {
		$taxonomy = $taxonomies[array_rand($taxonomies)];
		$terms = wp_get_object_terms(get_the_ID(), $taxonomies, ['fields' => 'ids']);
		
		if (!empty($terms)) {
			$term = $terms[array_rand($terms)];
			
			$videos = new WP_Query([
				"post_type" => "playlist",
				"post_status" => "publish",
				
				"lazyloop" => FALSE,
				
				"tax_query" => [
					[
						"terms" => $term,
						"field" => "term_id",
						"taxonomy" => $taxonomy,
					]
				],
			]);
			
			if ($videos->have_posts()) {
				$config = (object)[
					"class" => "bg-white",
					"theme" => "secondary",
					
					"heading" => [
						"text" => __("Related videos", TMP__LANG),
					],
					
					"wrapper" => '<div class="gallery gallery--video mt-4">%s</div>',
				];
				
				if ($videos->max_num_pages > 1)
					$config->buttons = [
						[
							"data" => [
								"text" => __("More", TMP__LANG),
								"attrs" => [
									"href" => get_term_link($term, $taxonomy),
								],
							],
						],
					];
				
				AFHPSection::include("default", $config)->output(function () use ($videos) {
					
					while ($videos->have_posts()) : $videos->the_post();
						AFHPComponent::import("gallery", (object)[
							"template" => "video",
						])->render();
					endwhile;
					
					wp_reset_postdata();
				});
			}
		}
	}