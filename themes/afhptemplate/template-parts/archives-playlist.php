<?php
	AFHPSection::include("albums", (object)[
		"theme" => "primary",
		
		"query" => [
			"hide_empty" => TRUE,
			"taxonomy" => get_object_taxonomies(get_post_type()),
		],
		
		"heading" => (object)[
			"output" => AFHPComponent::import("tabs", (object)[
				"output" => "return",
				"wrapper" => '<div class="heading">%s</div>',
			])->render(get_gallery_tabs_map()),
		],
	])->output();
	
	AFHPSection::include("lazyloop", (object)[
		"theme" => "secondary",
		"node" => "#gallery-wrapper",
		"buttonTheme" => "secondary",
		"wrapper" => sprintf(
			'%s <div id="gallery-wrapper" class="gallery gallery--video mt-4">%%s</div>',
			AFHPComponent::import("separator", (object)[
				"output" => "return"
			])->render([
				"color" => "#D44C32"
			])
		),
		
		"component" => (object)[
			"name" => "gallery",
			"config" => (object)[
				"template" => "video",
			]
		]
	
	])->output();