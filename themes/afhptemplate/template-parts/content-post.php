<?php
	$section = [];
	
	tmp_post_view(get_the_ID());
	
	$section[] = AFHPSection::include("default", (object)[
		"output" => "return",
		"class" => ["section--heading", "section--blog-white"],
	])->output(function() {
		
		$thumbnail = new stdClass();
		$thumbnail->classname = ["heading__thumbnail"];
		$thumbnail->src = get_the_post_thumbnail_url();
		
		if (empty($thumbnail->src)) {
			$thumbnail->src = placeholder_url();
			$thumbnail->classname[] = "image--placeholder";
		}
		
		printf(
			'<div class="heading d-flex">
	      <div class="heading__thumbnail position-relative">
	        <img 	alt="%s"
	              src="%s"
	              class="lazy"
	              data-src="%s"
	              data-srcset="%s"
	            />
	      </div>
	      <div class="heading__info d-flex flex-column">
	        <div class="info__heading">%s</div>
	        <date class="info__date">%s</date>
	        %s
	      </div>
	    </div>',
			get_the_title(),
			placeholder_url(),
			$thumbnail->src,
			$thumbnail->src,
			get_the_title(),
			get_the_date(),
			AFHPComponent::import("shear", (object)[
				"output" => "return",
				"classnames" => "info__social",
			])->render()
		);
	});
	
	$section[] = AFHPSection::include("default", (object)[
		"output" => "return",
		"class" => "section--article",
		"wrapper" => '<article class="article">%s</article>',
	])->output(function () {
		the_content();
	});
	
	AFHPSection::include("default", (object)[
		"class" => "section--content p-0"
	])->output(sprintf(
		'<div class="row">
				<div class="col-12 col-xl-9 col--main">%s</div>
				<div class="col-12 col-xl-3 col--main">%s</div>
			</div>',
		implode("", $section),
		blog__sidebar()
	));