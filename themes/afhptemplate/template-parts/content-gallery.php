<?php
	$post_type = get_post_type();
	
	$svg = AFHPComponent::import("svg", (object)[
		"output" => "return"
	]);
	
	$icon = $svg->render([
		"name" => "next"
	]);

	$siblings = (object)[
		"next" => get_next_post(),
		"prev" => get_previous_post(),
	];

	$navigation = [];
	$navClassnames = [
		"d-flex",
		"gitem--photo",
		"siblings__item",
		"siblings__item--%s",
		"align-items-center",
		"gallery__item--ajax",
		"justify-content-center",
	];
	foreach ($siblings as $key => $sibling)
		if ($sibling)
			$navigation[] = sprintf(
				'<a class="%s" href="%s" title="%s">
					%s
					<span>%s</span>
					%s
				</a>',
				implode(" ", array_merge($navClassnames, ["siblings__item--$key"])),
				get_post_permalink($sibling->ID),
				$sibling->post_title,
				$key === "next" ? $icon : "",
				mb_substr($sibling->post_title, 0, 12, "UTF-8") . (mb_strlen($sibling->post_title, "UTF-8") > 12 ? '...' : ''),
				$key === "prev" ? $icon : ""
			);


	AFHPComponent::import("banner", (object)[
		"style" => "thin",
	])->render([
		"breadcrumbs" => get_breadcrumbs_map(),
		"thumbnail" => tmp__customize("{$post_type}__banners", NULL),
	]);

	AFHPSection::include("default", (object)[
		"class" => [
			"overflow-hidden",
		],
	])->output(function() use ($navigation) {
		printf(
			'<article class="article article--gallery">
				<div class="container">
					<div class="row justify-content-center">
						<div class="col-md-10 col-12">
							<nav class="gallery__siblings d-flex flex-wrap align-items-center justify-content-between" role="navigation">
								%s
							</nav>
							<div class="gallery__thumbnail">%s</div>
							<div class="gallery__content">
								<h2 class="h2 gallery__heading">%s</h2>
								%s
								<date class="gallery__date">%s</date>
							</div>
						</div>
					</div>
				</div>
			</article>',
			implode("", $navigation),
			AFHPComponent::import("lazyimage", (object)[
				"output" => "return",
				"classnames" => "w-100",
			])->render([
				"thumbnail" => get_the_post_thumbnail_url(),
			]),
			get_the_title(),
			(function() {
				ob_start();
				the_content();
				return ob_get_clean();
			})(),
			get_the_date()
		);
	});