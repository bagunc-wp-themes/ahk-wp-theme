<?php
	AFHPSection::include("lazyloop", (object)[
		"theme" => "white",
		"buttonTheme" => "primary",
		"node" => "#gallery-wrapper",
		"wrapper" => '<div id="gallery-wrapper" class="gallery gallery--photo mt-4">%s</div>',
		
		"component" => (object)[
			"name" => "gallery",
			"config" => (object)[
				"template" => "photo",
			]
		]
	
	])->output();