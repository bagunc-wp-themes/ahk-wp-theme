<?php
get_header();

global $wp_query;

if ($wp_query->found_posts)
  $section = AFHPSection::include("lazyloop", (object)[
    "class" => "pt-0",
    "output" => "return",
    "buttonTheme" => "primary",
    "node" => "#archive-search-posts",
    "wrapper" => '<div id="archive-search-posts" class="row">%s</div>',

    "component" => (object)[
      "name" => "post",
      "config" => (object)[
        "template" => "thin",
        "classnames" => "bg-white",
        "wrapper" => '<div class="col-md-6 col-lg-4 mb-2">%s</div>',
      ],
    ],
  ])->output();
else
  $section = AFHPSection::include("default", (object)[
    "output" => "return",

    "class" => ["section--search", "section--empty", "p-0"],
  ])->output(function() {

    printf(
      '<div class="alert alert-warning text-center result-not-found">%s</div>',
      __("Result not found!", TMP__LANG)
    );
  });

AFHPSection::include("default", (object)[
  "class" => "section--content p-0"
])->output(sprintf(
  '<div class="row">
      <div class="col-12 col-xl-9 col--main">%s</div>
      <div class="col-12 col-xl-3 col--main">%s</div>
    </div>',
  $section,
  blog__sidebar()
));

get_footer();
