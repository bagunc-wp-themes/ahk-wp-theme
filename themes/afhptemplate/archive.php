<?php
get_header();

	$post_type = get_post_type();

  if (AFHPCustomPostTypes::native($post_type)) :
    
    $object = get_post_type_object($post_type);
  
    AFHPComponent::import('banner')->render([
	    "title" => $object->label,
	    "breadcrumbs" => get_breadcrumbs_map(),
	    "thumbnail" => tmp__customize("{$post_type}__banners", NULL),
    ]);

  endif;

  get_template_part("template-parts/archives", get_post_type());

get_footer();