<?php

	$front_data = get__front__data();

	get_header();

	while (have_posts()) : the_post(); ?>

		<header class="header">
			<div class="overlay"></div>
			<div class="container d-block d-md-flex align-items-end h-100">
				<div class="row align-items-md-end justify-content-center">
					<div class="col-12 col-md-4 order-md-2 col--person pt-4">
						<?php if (!empty($front_data["image"])) : ?>
							<img    src="<?php print $front_data["image"] ?>"
							        alt="<?php _e("President", TMP__LANG) ?>"
							/>
						<?php endif; ?>
					</div>
					<div class="col-12 col-md-8 col--blockquote">
						<?php if (!empty($front_data['speech'])) : ?>
							<blockquote class="blockquote">
								<?php print $front_data['speech']; ?>
							</blockquote>
						<?php endif; ?>
					</div>
				</div>
			</div>
		</header>

		<section class="section--about-us section--flag__1">
			<div class="container">
				<h3 class="section__heading">
					<span><?php _e("About us", TMP__LANG) ?></span>
					<div class="heading__underline red"></div>
				</h3>
				<div class="section__content"><?php the_content() ?></div>
			</div>
			<div class="slogan">
				<div class="container">
					<p><?php print $front_data['slogan'] ?></p>
				</div>
			</div>
		</section>

		<?php
		if (!empty($front_data["sections"])) :
			foreach ($front_data["sections"] as $item) :
				$section = AFHPSection::include($item);
				if ($section instanceof AFHPSection)
					$section->output();
				else
					print system__message(
						"Error [404] <b>$item</b> section is not found",
						"warning"
					);
			endforeach;
		endif;
	endwhile;

	get_footer();
