<?php

$blog_ID = get_option( 'page_for_posts' );

get_header();
	AFHPSection::include("default", (object)[
		"class" => "section--content p-0"
	])->output(sprintf(
			'<div class="row">
				<div class="col-12 col-xl-9 col--main">%s</div>
				<div class="col-12 col-xl-3 col--main">%s</div>
			</div>',
			implode("", [
				AFHPSection::include("categories", (object)[
					"output" => "return"
				])->output(),
				AFHPSection::include("lazyloop", (object)[
					"output" => "return",

					"heading" => [
						"color" => "#FF6504",
						"text" => __("Recent posts", TMP__LANG)
					],
					
					"node" => "#posts-wrapper",
					"buttonTheme" => "primary",
					"wrapper" => '<div id="posts-wrapper" class="row">%s</div>',
					
					"component" => (object)[
						"name" => "post",
						"config" => (object)[
							"template" => "thin",
							"classnames" => "bg-white",
							"wrapper" => '<div class="col-sm-6 col-xl-4 mb-4">%s</div>'
						]
					]
				])->output()
			]),
			blog__sidebar()
		));
	
get_footer();