<?php
require_once "utils.php";

/** Custom post types */
require__once('includes/post-types/index.php');

/** Customizer */
require__once('includes/customizer.php');

/** Components */
require__once('components/interface.php');
require__once('components/abstract.php');

/** Sections */
require__once('sections/interface.php');
require__once('sections/abstract.php');

/** Shortcodes */
require__once('includes/shortcodes.php');

/** Widgets */
require__once('includes/widgets/widgets.php');

/** Youtube api handler */
require__once('includes/youtube.php');

/** Yandex Weather api handler */
require__once('includes/yandex-weather.php');

/** CBA currencies handler */
require__once('includes/cba-currencies.php');


/** Theme services */
add_theme_support("menus");
add_theme_support("title-tag");
add_theme_support("custom-logo");
add_theme_support("post-thumbnails");

/** Image sizes */
add_image_size("admin-post-columns-image", 120, 120, FALSE);

/** Hooks and actions */
add_action( 'after_switch_theme', 'flush_rewrite_rules' );

/** Crones */
require__once("includes/crones.php");

/** After setup hook */
add_action("after_setup_theme", function () {
	
	/** Enabling translation */
	load_theme_textdomain( TMP__LANG, TMP__PATH . '/languages' );
	
});

/** Init hook */
add_action('init', function() {

  /** Custom post types */
  $CustomPostTypes = AFHPCustomPostTypes::execute();
  $CustomPostTypes->register();

	/** Registration menus */
	register_nav_menus([
		"primary-left" => __( 'Primary left Menu', TMP__LANG ),
	]);
	
	register_nav_menus([
		"footer-menu" => __( 'Footer Menu', TMP__LANG ),
	]);
	
});

/** Widgets init */
add_action('widgets_init', function () {
	
	/** Registration sidebars */
	register_sidebar([
		'id'          => 'blog__right',
		'name'        => __( 'Blog right', TMP__LANG),
		'description' => __('This sidebar is located blog page right side.', TMP__LANG),
		'before_widget' => '<div class="widget">',
		'after_widget' => '</div>',
		'before_title'  => '<h4 class="widget__title">',
		'after_title'   => "</h4>\n",
	]);
	
	register_sidebar([
		'id'          => 'footer__middle',
		'name'        => __( 'Footer middle', TMP__LANG),
		'description' => __('This sidebar is located in middle of footer.', TMP__LANG),
		'before_widget' => '<div class="col-12 col-sm-6 col-lg-3">',
		'after_widget' => '</div>',
		'before_title'  => '<h4 class="footer-nav__heading">',
		'after_title'   => "</h4>\n",
	]);
	
	/** Register custom widgets */
	AFHPCustomWidgets::register();
	
});

/** Global WP Query change */
add_action('pre_get_posts', function ($query) {
	
	if (is_admin()) return;
	
	/**
	 * @var WP_Query $query
	 */
	if (!is_req_ajax() && $query->get("lazyloop") !== FALSE) {
		$paged = (int)get_query_var("paged");
		
		$query->set("paged", 0);
		$query->set("original_paged", $paged);
		$query->set("posts_per_page", $paged * get_the_per_page());
	}
	
	
	if (is_category() || is_blog()) {
		$sticky = get_option("sticky_posts");
		
		if (!empty($sticky))
			$query->set("post__not_in", $sticky);
	}
	
});

add_action('wp_enqueue_scripts', function () {

	wp_register_style(tmp__prefix('magnific-popup'), tmp__assets('vendors/magnific-popup/magnific-popup.css'), NULL, __tmp_v());
	wp_register_style(tmp__prefix('slick'), tmp__assets('vendors/slick/slick.css'), [tmp__prefix('slick-theme')], __tmp_v());
	wp_register_style(tmp__prefix('bootstrap'), tmp__assets('vendors/bootstrap/bootstrap.min.css'), NULL, __tmp_v());
	wp_register_style(tmp__prefix('slick-theme'), tmp__assets('vendors/slick/slick-theme.css'), NULL, __tmp_v());
	wp_register_style(tmp__prefix('styles'), tmp__assets('css/style.css'), [tmp__prefix('bootstrap')], __tmp_v());
	wp_register_style(tmp__prefix('custom'), tmp__assets('css/custom.css'), [tmp__prefix('styles')], __tmp_v());
	
	wp_enqueue_style(tmp__prefix('bootstrap'));
	wp_enqueue_style(tmp__prefix('magnific-popup'));
	wp_enqueue_style(tmp__prefix('slick-theme'));
	wp_enqueue_style(tmp__prefix('slick'));
	wp_enqueue_style(tmp__prefix('styles'));
	wp_enqueue_style(tmp__prefix('custom'));


	wp_register_script(tmp__prefix('perfect-scrollbar'), tmp__assets('vendors/perfect-scrollbar/perfect-scrollbar.min.js'), NULL, __tmp_v(), TRUE);
	wp_register_script(tmp__prefix('magnific-popup'), tmp__assets('vendors/magnific-popup/jquery.magnific-popup.min.js'), ['jquery'], __tmp_v(), TRUE);
	wp_register_script(tmp__prefix('bootstrap'), tmp__assets('vendors/bootstrap/bootstrap.min.js'), NULL, __tmp_v(), TRUE);
	wp_register_script(tmp__prefix('youtube-player'), 'https://www.youtube.com/player_api', NULL, __tmp_v(), TRUE);
	wp_register_script(tmp__prefix('slick'), tmp__assets('vendors/slick/slick.min.js'), ['jquery'], __tmp_v(), TRUE);
	wp_register_script(tmp__prefix('script'), tmp__assets('js/script.js'), ['jquery'], __tmp_v(), TRUE);
	wp_register_script(tmp__prefix('ajax'), tmp__assets('js/ajax.js'), ['jquery'], __tmp_v(), TRUE);

	wp_enqueue_script(tmp__prefix('perfect-scrollbar'));
	wp_enqueue_script(tmp__prefix('magnific-popup'));
	wp_enqueue_script(tmp__prefix('youtube-player'));
	wp_enqueue_script(tmp__prefix('bootstrap'));
	wp_enqueue_script(tmp__prefix('slick'));
	wp_enqueue_script(tmp__prefix('script'));
	wp_enqueue_script(tmp__prefix('ajax'));

});

/** Admin scripts and styles */
add_action('admin_enqueue_scripts', function() {

	wp_register_style(tmp__prefix('admin-styles'), tmp__assets('css/admin.css'), NULL, __tmp_v(), NULL);

	wp_enqueue_style('wp-color-picker');
	wp_enqueue_style(tmp__prefix('admin-styles'));

  wp_register_script(tmp__prefix('admin-scripts'), tmp__assets('js/admin.js'), ['jquery', 'wp-color-picker'], __tmp_v(), TRUE);

  if (!did_action('wp_enqueue_media'))
    wp_enqueue_media();

  wp_enqueue_script(tmp__prefix('admin-scripts'));

  wp_localize_script(tmp__prefix('admin-scripts'), 'admin__localize', [
    "ajax__url" => admin_url('admin-ajax.php'),
    "tmp__prefix" => TMP__PREFIX,
	  "text" => [
	  	"ajax__error" => _x("Error [500] System error! Please contact development studio %s", '[SkyTech studio]')
	  ]
  ]);

});

/** Custom metaboxes */
add_action('add_meta_boxes', function () {

	$page_ID = isset($_GET['post']) ? (int)$_GET['post'] : 0;

	if (is__front__page($page_ID)) :

		add_meta_box(
			tmp__prefix('president__image'),
			__('President image', TMP__LANG),
			function ($post) {

				$thumbnail = [
					"id" => 0,
					"src" => ""
				];

				if ($post instanceof WP_Post) {
					$thumbnail['id'] = get_post_meta($post->ID, 'president__image', true);
					$thumbnail['src'] = wp_get_attachment_url($thumbnail['id']);
				}

				printf(
					'<label class="%s">
            <a href="#" class="remove">%s</a>
            <input type="number" name="president-image" value="%d" />
            %s
          </label>',
					tmp__prefix('field--file'),
					__('remove'),
					(int)$thumbnail['id'],
					!empty($thumbnail['src']) ? '<img src="' . $thumbnail['src'] .'" />' : ''
				);
			},
			'page',
			'side'
		);

		add_meta_box(
			tmp__prefix('president__speech'),
			__('President speech', TMP__LANG),
			function ($post) {
				if (!is__front__page($post->ID)) return;

				$value = get_post_meta($post->ID, 'president__speech', TRUE);

				printf(
					'<textarea class="%s" name="president-speech">%s</textarea>',
					tmp__prefix('full__width'),
					$value
				);
			},
			'page',
			'advanced'
		);

		add_meta_box(
			tmp__prefix('slogan'),
			__('Slogan', TMP__LANG),
			function ($post) {
				if (!is__front__page($post->ID)) return;

				$value = get_post_meta($post->ID, 'slogan', TRUE);

				printf(
					'<textarea class="%s" name="slogan">%s</textarea>',
					tmp__prefix('full__width'),
					$value
				);
			},
			'page',
			'advanced'
		);
		
		add_meta_box(
			tmp__prefix('address'),
			__('Address', TMP__LANG),
			function ($post) {
				if (!is__front__page($post->ID)) return;
				
				$value = get_post_meta($post->ID, 'address', TRUE);
				printf(
					'<input class="%s" name="address" value="%s" />',
					tmp__prefix('full__width'),
					$value
				);
			},
			'page',
			'advanced'
		);
		
		add_meta_box(
			tmp__prefix('donate__title'),
			__('Donate title', TMP__LANG),
			function ($post) {
				if (!is__front__page($post->ID)) return;
				
				$value = get_post_meta($post->ID, 'donate__title', TRUE);
				
				printf(
					'<input class="%s" name="donate-title" value="%s" />',
					tmp__prefix('full__width'),
					$value
				);
			},
			'page',
			'advanced'
		);
		
		add_meta_box(
			tmp__prefix('donate__desc'),
			__('Donate description', TMP__LANG),
			function ($post) {
				if (!is__front__page($post->ID)) return;
				
				$value = get_post_meta($post->ID, 'donate__desc', TRUE);
				
				printf(
					'<textarea class="%s" name="donate-desc">%s</textarea>',
					tmp__prefix('full__width'),
					$value
				);
			},
			'page',
			'advanced'
		);

	endif;

	$party__pages = get_pages([
		"meta_key" => "_wp_page_template",
		"meta_value" => "template-party.php"
	]);

	$party__pages = array_map(function($item) {
		return $item->ID;
	}, $party__pages);

	if (in_array($page_ID, $party__pages)) :

		add_meta_box(
			tmp__prefix("resources"),
			__("Party resources", TMP__LANG),
			function($post) {

				function render($resource) {
					$key = uniqid(tmp__prefix());

					return sprintf(
						'<div class="%s">
							<div class="label">
								<span>%s %s</span>
								<input type="text" name="party-resources[%s][text]" value="%s" />
							</div>
							<div class="label">
								<span>%s</span>
								<input type="text" name="party-resources[%s][link]" value="%s" />
								<button type="button" data-field="%s" class="button %s">%s</button>
							</div>
						</div>',
						tmp__prefix("resource"),
						__("Text", TMP__LANG),
						sprintf(
							'<button 	type="button"
												class="components-button is-link is-destructive">%s</button>',
							__("remove", TMP__LANG)
						),
						$key,
						!empty($resource["text"]) ? $resource["text"] : "",
						__("Link", TMP__LANG),
						$key,
						!empty($resource["link"]) ? $resource["link"] : "",
						"input[name='party-resources[$key][link]']",
						tmp__prefix("choose__file"),
						__("Choose", TMP__LANG)
					);
				}

				$resources = get_post_meta($post->ID, "party__resources", TRUE);
				$resources = unserialize($resources);

				if (!is_array($resources))
					$resources = [];

				printf(
					'<button type="button" id="afhp_resource__add" class="button">%s</button>
					<template>%s</template>',
					__("Add", TMP__LANG),
					render([
						"url" => "",
						"text" => "",
					])
				);

				foreach ($resources as $resource)
					print render($resource);

			},
			"page",
			"side"
		);

	endif;

});

/** Saving custom metaboxes data */
add_action('save_post', function(int $post_id) {

	if (isset($_POST['president-image'])) {
		$president__image = (int)$_POST['president-image'];
		update_post_meta($post_id, 'president__image', $president__image);
	}

	if (isset($_POST['president-speech'])) {
		$president__speech = trim((string)$_POST['president-speech']);
		update_post_meta($post_id, 'president__speech', $president__speech);
	}

	if (isset($_POST['slogan'])) {
		$slogan = trim((string)$_POST['slogan']);
		update_post_meta($post_id, 'slogan', $slogan);
	}

	if (isset($_POST["party-resources"])) {
		$resources = serialize($_POST["party-resources"]);
		update_post_meta($post_id, "party__resources", $resources);
	}
	
	if (isset($_POST["address"])) {
		$resources = trim($_POST["address"]);
		update_post_meta($post_id, "address", $resources);
	}
	
	if (isset($_POST["donate-title"])) {
		$resources = trim($_POST["donate-title"]);
		update_post_meta($post_id, "donate__title", $resources);
	}
	
	if (isset($_POST["donate-desc"])) {
		$resources = trim($_POST["donate-desc"]);
		update_post_meta($post_id, "donate__desc", $resources);
	}

});

add_action('category_edit_form_fields', function(WP_Term $term)  {

	$color = get_term_meta($term->term_id, 'color', TRUE);

	printf(
		/** @lang text */
		'<tr class="form-field form-required term-color-wrap">
        <th scope="row">
          <label for="color">%s</label>
        </th>
        <td>
          <input  id="color"
                  value="%s"
                  name="color"
                  class="field--color"
                 />
          <p class="description">%s</p>
        </td>
      </tr>',
		__('Color', TMP__LANG),
		$color,
		__('Category color', TMP__LANG)
	);

});

add_action('edited_category', function(int $term_id) {

	if (isset($_POST['color'])) {
		$color = trim((string)$_POST['color']);
		update_term_meta($term_id, 'color', $color);
	}

});

add_filter('manage_edit-category_columns', function($columns) {
	$columns['color'] = __('Color', TMP__LANG);

	return $columns;
});

add_filter("manage_category_custom_column", function($output, $column_name, $term_id) {

	switch ($column_name) {
		case 'color':
			$color = get_term_meta($term_id, 'color', TRUE);
			$output = sprintf(
			/** @lang text */
				'<input value="%s"
								data-term_id="%d"
                class="field--color field--ajax"
              />',
				$color,
				$term_id
			);
			break;
		default:
			break;
	}

	return $output;
}, 10, 3);

/**
 * Excerpt length
 */
add_filter('excerpt_length', function () {
	
	$query_object = get_queried_object();
	
	if ($query_object instanceof WP_Post_Type)
		if ($query_object->name === "discussion")
			return 15;
	
	if ($query_object instanceof WP_Term)
		if ($query_object->taxonomy === "structure__category")
			return 40;
	
	return 35;
}, 10, 1);


add_filter('walker_nav_menu_start_el', function($item_output, $item, $depth, $args) {
	
	if ($args->menu_id == "primary-left") {
		
		$icon = "";
		if (!$depth && !empty($item->description )) {
			$icon = AFHPComponent::import("svg", (object)[
				"output" => "return",
				"classnames" => "icon--menu",
			])->render([
				"name" => $item->description
			]);
		}
		
		$item_output = str_replace(
			$args->link_before,
			$icon,
			$item_output
		);
	}
	
	return $item_output;
}, 10, 4 );

add_filter('manage_posts_columns', 'afhp_add_post_admin_thumbnail_column', 1);
add_filter('manage_pages_columns', 'afhp_add_post_admin_thumbnail_column', 1);
function afhp_add_post_admin_thumbnail_column($columns) {
	
	$columns['afhp_thumbnail'] = __('Thumbnail', TMP__LANG);
	return $columns;
}


add_action('manage_posts_custom_column', 'afhp_show_post_thumbnail_column', 5, 2);
add_action('manage_pages_custom_column', 'afhp_show_post_thumbnail_column', 5, 1);
function afhp_show_post_thumbnail_column($column_name) {
	
	switch($column_name){
		case 'afhp_thumbnail': {
			the_post_thumbnail('admin-post-columns-image');
			break;
		}
	}
}

/**
 * Watermark hook
 * add_action('wp_handle_upload_prefilter', function($file) { });
 */