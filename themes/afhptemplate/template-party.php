<?php
/**
 * Template Name:  Party
 */

get_header();

	while (have_posts()) : the_post();

		get_template_part("template-parts/content", get_post_type());

		$resources = get_post_meta(get_the_ID(), "party__resources", TRUE);
		$resources = unserialize($resources);

		if (is_array($resources)) :

			$buttons = AFHPSection::include("buttons");
			$buttons->result = [];

			foreach ($resources as $resource)
				$buttons->result[] = (object)[
					"text" => $resource["text"],
					"attrs" => [
						"target" => "_blank",
						"href" => $resource["link"],
					],
				];

			$buttons->output();

		endif;

	endwhile;

get_footer();
