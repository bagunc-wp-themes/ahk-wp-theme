<?php
	if (!defined('DS'))
		define('DS', DIRECTORY_SEPARATOR);

	define('TMP__LANG', 'default');
	define('TMP__VERSION', 1.0);
	define('TMP__PREFIX', 'afhp_');
	define('TMP__PATH', get_template_directory());
	define('TMP__URL', get_template_directory_uri());

	/**
	 * Theme version
	 *
	 * @return float
	 */
	function __tmp_v() {

		return TMP__VERSION;
	}

	/**
	 * Random color
	 *
	 * @return string
	 */
	function random__color() {

		return '#' . implode('', [
				str_pad( dechex( mt_rand( 0, 255 ) ), 2, '0', STR_PAD_LEFT),
				str_pad( dechex( mt_rand( 0, 255 ) ), 2, '0', STR_PAD_LEFT),
				str_pad( dechex( mt_rand( 0, 255 ) ), 2, '0', STR_PAD_LEFT)
			]);
	}

	/**
	 * @param null $input
	 */
	function debug($input = null) {

		print '<pre>';
		if (is_array($input))
			print_r($input);
		else
			var_dump($input);
		print '</pre>';
	}

	/**
	 * Gluing theme prefix to text
	 *
	 * @param string $text
	 * @return string
	 */
	function tmp__prefix($text = '') {

		return TMP__PREFIX . $text;
	}

	/**
	 * Creating file system path from memory driver
	 *
	 * @param string $path
	 * @return string
	 */
	function tmp__path($path = '') {
		$filepath = TMP__PATH . DS;

		if (is_array($path))
			$filepath .=  implode(DS, $path);
		elseif (is_string($path))
			$filepath .= str_replace('/', DS, $path);

		return $filepath;
	}

	/**
	 * Require including files from template
	 *
	 * @param string $path
	 *
	 * @return mixed
	 */
	function require__once($path = '') {
		$filepath = tmp__path($path);

		if (file_exists($filepath))
			return require_once $filepath;
		else
			print system__message(
				"<b>$filepath</b> file is not found",
				"danger"
			);

		return NULL;
	}

	/**
	 * System messages printer
	 *
	 * @param string $message
	 * @param string $status
	 *
	 * @return string
	 */
	function system__message($message = "", $status = "info") {

		$cls = ["alert"];
		if (is_string($status))
			$cls[] = "alert-$status";

		return sprintf(
			'<div class="%s" role="alert">%s</div>',
			implode(' ', $cls),
			$message
		);
	}

	/**
	 * Scanning directory
	 *
	 * @param string $path
	 * @param null $callback
	 * @return array|false|void
	 */
	function scan__dir($path = '', $callback = null) {
		$dir = tmp__path($path);

		if (!is_dir($dir)) {
			print system__message(
				"<b>$dir</b> is not directory",
				"danger"
			);

			return;
		}

		$files = scandir($dir);
		$files = array_diff($files, ['.', '..', '../']);

		if ($callback)
			$files = array_filter($files, $callback);

		return $files;
	}

	/**
	 * URL to assets resources
	 *
	 * @param string $url
	 * @return string
	 */
	function tmp__assets($url = "") {
		if (is_array($url))
			$url = implode('/', $url);

		return TMP__URL . '/assets/' . $url;
	}

	/**
	 * Checking is front page by id
	 *
	 * @param int $page_id
	 * @return bool
	 */
	function is__front__page(int $page_id) {

		return $page_id == get_option('page_on_front');
	}

	function get__front__data() {

		$page__ID = get_option('page_on_front');

		return [
			"slogan" => get_post_meta($page__ID, 'slogan', TRUE),
			"sections" => ["mission", "activity", "gallery", "news", "donate"],
			"speech" => get_post_meta($page__ID, 'president__speech', TRUE),
			"image" => wp_get_attachment_image_url((int)get_post_meta($page__ID, 'president__image', TRUE), "full"),
		];
	}

	function site_logo() {

		$logo__url = tmp__assets('img/logo.png');

		if (has_custom_logo()) {
			$custom_logo_id = get_theme_mod( 'custom_logo' );

			if ($custom_logo_id)
				$logo__url = wp_get_attachment_image_url($custom_logo_id);
		}

		return sprintf(
		/** @lang text */
			'<img src="%s" alt="Logo" />',
			$logo__url
		);
	}

	function site_languages() {

		$languages = apply_filters( 'wpml_active_languages', []);

		if (is_array($languages)) {

			$lang = [];
			foreach ($languages as $language) {
				$classnames = ["lang__item"];
				if ($language["active"])
					$classnames[] = "lang__item--current";

				$lang[] = sprintf(
					'<a class="%s" href="%s">%s</a>',
					implode(" ", $classnames),
					$language["url"],
					mb_substr($language["native_name"], 0, 3, "UTF-8"),
				);
			}

			return implode("", $lang);
		}

		return "";
	}

	function placeholder_url() {

		return tmp__assets('img/svg/picture.svg');
	}

	function is_blog () {
		return ((is_archive() || is_author() || is_category() || is_home() || is_single() || is_tag()) && 'post' == get_post_type()) || is_search();
	}

	function get_breadcrumbs_map($root = NULL) {

		$map = [];

		if (!is_front_page()) {
			/**
			 * @var WP_Post $query_object
			 */
			$query_object = get_queried_object();
			$post_type = get_post_type();

			$map[] = (object)[
				"url" => home_url('/'),
				"text" => __("Home", TMP__LANG),
			];

			if (is_array($root))
				$map[] = (object)$root;

			if (!empty($post_type)) {
				if (AFHPCustomPostTypes::native($post_type)) {
					$type  = get_post_type_object($post_type);
					$map[] = (object)[
						"text" => $type->label,
						"url" => get_post_type_archive_link($post_type),
					];
				}

			}

			if (is_category() || is_single()) {
				$terms = get_object_taxonomies($query_object);
				$terms = wp_get_post_terms($query_object->ID, $terms);

				foreach ($terms as $term)
					if ($term instanceof WP_Term)
						$map[] = (object)[
							"text" => $term->name,
							"url" => get_term_link($term->term_id),
						];
			}

			if (is_tax()) {
				/**
				 * @var WP_Term $query_object
				 */
				$values = parent_terms_map($query_object->term_id, $query_object->taxonomy);

				if (!empty($values))
					foreach ($values as $item)
						$map[] = $item;
			}

			if (is_single() || is_page())
				$map[] = (object)[
					"text" => get_the_title(),
					"url" => get_the_permalink(),
				];

		}

		return count($map) > 1 ? $map : [];
	}

	function get_gallery_tabs_map() {

		$post_types = ["gallery", "playlist"];

		$result = [];

		foreach ($post_types as $post_type) {
			$post = get_post_type_object($post_type);

			$result[] = (object)[
				"text" => $post->label,
				"active" => (get_post_type() === $post_type),
				"link" => get_post_type_archive_link($post_type),
			];
		}

		return $result;
	}

	function get_the_slug() {

		return get_post_field('post_name', get_post());
	}

	function the_slug() {

		print get_the_slug();
	}

	function get_the_per_page() {

		return (int)get_option('posts_per_page');
	}

	function is_req_ajax() {

		return (isset($_SERVER['HTTP_X_REQUESTED_WITH']) &&
			!empty($_SERVER['HTTP_X_REQUESTED_WITH']) &&
			strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest');
	}

	function parent_terms_map($term_id, $taxonomy) {

		$parent = sprintf(
			'<div>%s</div>',
			get_term_parents_list($term_id, $taxonomy)
		);
		$xml_parser = xml_parser_create();
		xml_parse_into_struct($xml_parser, $parent, $values);
		xml_parser_free($xml_parser);

		$values = array_map(function(array $item) {
			if (!empty($item["value"]))
				return (object)[
					"text" => $item["value"],
					"url" => isset($item["attributes"]) ? $item["attributes"]["HREF"] : "#",
				];

			return NULL;
		}, $values);

		return array_filter($values, function($item) {
			return $item !== NULL && $item->text !== "/";
		});

	}

	function blog__sidebar() {

		ob_start(); ?>

		<aside class="sidebar--blog"><?php
		if (function_exists("dynamic_sidebar"))
			if (is_active_sidebar("blog__right"))
				dynamic_sidebar("blog__right"); ?>
		</aside><?php

		return ob_get_clean();

	}

	function tmp__customize($key, $default = NULL) {

		return get_theme_mod(tmp__prefix($key), $default);
	}

	function tmp_post_view($post_id = 0, $value = NULL) {
		$post_id = $post_id ? $post_id : get_the_ID();

	    $view_key = tmp__prefix("views__ip");
	    $views = get_post_meta($post_id, $view_key, TRUE);

	    if (!is_array($views))
	        $views = [];

        array_push($views, $_SERVER["REMOTE_ADDR"]);
        $views = array_unique($views);

        update_post_meta($post_id, $view_key, $views);

		$meta_key = tmp__prefix("views");
		update_post_meta($post_id, $meta_key, count($views));
	}

	function we_are_hear() {
	    $url = "https://api-maps.yandex.ru/services/constructor/1.0/js/";
	    $query = [
            "um" => "constructor%3A0ecc1531c6a9ca81b8311b93a6b6c7517167ddb0cc143078c87621fa0710726a",

            "width" => "100%",
            "height" => "100%",
            "scroll" => "true",
            "lang" => get_locale(),
        ];

	    $query = array_map(function ($value, $key) {
	        return "$key=$value";
        }, $query, array_keys($query));

        $query = implode("&amp;", $query);

	    return sprintf(
            '<script async type="text/javascript" charset="utf-8" src="%s"></script>',
            implode("?", [$url, $query])
        );
    }

    function get_site_info() {
	    $keys = [
            "phone" => __("Tel.", TMP__LANG),
            "email" => __("E-mail.", TMP__LANG),
        ];

	    $res = [];
	    foreach ($keys as $key => $label)
	        $res[$key] = [
                "label" => $label,
	            "value" => tmp__customize($key)
            ];

	    $address = get_post_meta(get_option('page_on_front'), "address", TRUE);
	    if (!empty($address))
	        $res["address"] = [
              "value" => $address,
              "label" => __("Address.", TMP__LANG),
            ];

	    return $res;
    }

    function site_social_links() {

	    $socials = ["facebook", "instagram", "twitter"];

	    ob_start();
	    foreach ($socials as $social) {
	        $value = tmp__customize("{$social}__link");

	        if (!empty($value))
	            printf(
                    '<a href="%s" target="_blank">
                        <img src="%s" alt="%s" />
                    </a>',
                  $value,
                  tmp__assets("img/footer/$social.png"),
                  $social
                );
        }
	    return ob_get_clean();
    }
