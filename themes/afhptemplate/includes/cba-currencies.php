<?php
class AFHPCBACurrencies {
	
	/**
	 * API data fetch uri
	 */
	const URI = "http://api.cba.am/exchangerates.asmx?wsdl";
	
	/**
	 * @var stdClass[] $result
	 */
	public $result;
	
	/**
	 * @var string[] $currencies
	 */
	public $currencies = ["USD"];
	
	/**
	 * AFHPCBACurrencies constructor.
	 *
	 * @param string[] $currencies
	 */
	public function __construct($currencies) {
		
		$this->currencies = $currencies;
	}
	
	/**
	 * Fetching data
	 *
	 * @return stdClass[]|null
	 */
	public function fetch() {
		
		try {
			$client = new SoapClient($this::URI);
			
			$this->result = $client->ExchangeRatesLatest();
			$this->result = $this->result->ExchangeRatesLatestResult->Rates->ExchangeRate;
			
			if (is_array($this->result))
				$this->result = array_filter($this->result, function($item) {
					
					return $this->currencies && in_array($item->ISO, $this->currencies);
				});
		} catch (SoapFault $e) {
			$this->result = NULL;
		}
		
		return $this->result;
	}
	
	/**
	 * Data fetching task
	 *
	 * @param datetime $updated
	 * @param number $interval
	 * @param callable $callback
	 *
	 * @return bool|null
	 */
	public function cron($updated, $interval, $callback) {
		
		if (!is_callable($callback)) return NULL;
		
		$diff = NULL;
		$now = new DateTime("now");
		
		if (!empty($updated)) {
			$diff = $now->diff($updated);
		}
		
		if (($diff && $diff->h >= $interval) || empty($updated)) {
			$this->fetch();
			
			$callback($this, $now);
			
			return TRUE;
		}
		
		return FALSE;
	}
}