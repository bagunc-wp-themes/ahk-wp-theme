<?php
add_shortcode(tmp__prefix("party_books"), function () {

	$buttons = AFHPSection::include("buttons", (object)[
		"output" => "return"
	]);
	$buttons->result = [];

	return $buttons->output();
});