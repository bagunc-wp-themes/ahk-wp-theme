<div class="template-defs m-0 p-0 overflow-hidden d-none">
    <template id="modal-template">
        <div class="modal fade modal--gallery" id="simple-modal" tabindex="-1" role="dialog" aria-labelledby="simple-modal-title" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-body"></div>
                </div>
            </div>
        </div>
    </template>

    <template id="loading-template">
        <div class="site-loading">
            <div class="logo">
                <?php print site_logo() ?>
            </div>
        </div>
    </template>
</div>