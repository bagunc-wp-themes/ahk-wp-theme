<?php
	/** Yandex weather update */
	$yandex__weather = get_option(tmp__prefix("yandex__weather"));
	if (empty($yandex__weather)) {
		$yandex__weather = (object)[
			"data" => NULL,
			"updated" => NULL,
		];
	}
	
	$key = tmp__customize("yandex__weather__api__key");
	
	$lon = tmp__customize("yandex__weather__api__longitude");
	$lat = (float)tmp__customize("yandex__weather__api__latitude");
	
	$YandexWeather = new AFHPYandexWeather($key, $lat, $lon);
	$YandexWeather->cron($yandex__weather->updated, 3, function($weather, $date) {
		
		$now = $weather->now(["icon"]);
		$today = $weather->days("today");
		
		$input = new stdClass();
		$input->updated = $date;
		
		$input->data = (object)[
			"day" => 0,
			"evening" => 0,
			
			"icon" => NULL,
		];
		
		if (!empty($today)) {
			if (isset($today->parts)) {
				$input->data->day = $today->parts->day->temp_avg;
				$input->data->evening = $today->parts->evening->temp_avg;
			}
		}
		
		if (!empty($now))
			if (isset($now->icon))
				$input->data->icon = $now->icon;
		
		update_option(tmp__prefix("yandex__weather"), $input);
	});
	
	/** CBA Currencies update */
	$cba__currencies = get_option(tmp__prefix("cba__currencies"));
	$CBACurrencies = new AFHPCBACurrencies(["USD", "EUR", "GBP", "RUB"]);
	$CBACurrencies->cron($cba__currencies->updated, 8, function($currencies, $date) {
		$input = new stdClass();
		
		if ($currencies->result) {
			$input->updated = $date;
			$input->data = $currencies->result;
			
			update_option(tmp__prefix("cba__currencies"), $input);
		}
	});