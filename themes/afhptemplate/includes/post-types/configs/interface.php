<?php

interface AFHPTypeInterface {

	/**
	 * Custom post type config
	 *
	 * @return array
	 */
	public function post();

	/**
	 * Custom taxonomies config
	 *
	 * @return array[]
	 */
	public function taxonomy();

	/**
	 * Custom post type and taxonomy additional actions and hooks
	 * Called after registration
	 */
	public function actions();
	
	/**
	 * Custom settings page
	 *
	 * @return array|object
	 */
	public function settings();
	
}