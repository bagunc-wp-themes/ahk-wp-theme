<?php
/**
 * Playlist post type and taxonomies
 *
 * Class AFHPTypePlaylist
 */
class AFHPTypePlaylist extends AFHPPostType {

	const NAME = "playlist";

	public function post() {

		return [
			"label" => __("Playlist", TMP__LANG),

			"public" => TRUE,
			"publicly_queryable" => TRUE,

			"show_ui" => TRUE,
			'show_in_rest' => TRUE,

			"rewrite" => TRUE,
			"query_var" => TRUE,

			"has_archive" => TRUE,
			"hierarchical" => TRUE,

			"capability_type" => "post",
			"menu_icon" => tmp__assets('img/svg/video.svg'),

			"taxonomies" => [
				"playlist__albums",
			],
			"supports" => [
				"title",
				"editor",
				"excerpt",
				"thumbnail",
			],
		];
	}

	public function taxonomy() {

		return [
			"playlist__albums" => [
				"label" => __("Albums", TMP__LANG),
				"description" => __("Albums for playlist", TMP__LANG),

				"public" => TRUE,

				"show_ui" => TRUE,
				"show_in_rest" => TRUE,

				"hierarchical" => TRUE,
				
				"rewrite" => [
					"slug" => "playlists",
					"with_front" => FALSE,
				],
			]
		];
	}

	public function metabox__youtube__video__url($post) {

		$youtube__url = get_post_meta($post->ID, 'youtube__url', TRUE);

		function youtube_preview($url) {
			
			if (empty($youtube__url)) return "";
			
			return sprintf(
				'<iframe 	height="auto"
									width="100%%"
									frameborder="0"
									type="text/html"
									src="http://www.youtube.com/embed/%s?autoplay=0&origin=%s"></iframe>',
				$url,
				home_url('/')
			);
		}
		
		printf(
			'<input class="%s" name="youtube-url" value="%s" /><hr />%s',
			tmp__prefix('full__width'),
			$youtube__url ? $youtube__url : '',
			youtube_preview($youtube__url)
		);
	}

	public function custom__metaboxes() {

		add_meta_box(
			tmp__prefix('playlist__url'),
			__('Youtube video id', TMP__LANG),
			[$this, 'metabox__youtube__video__url'],
			'playlist',
			'side'
		);
	}

	public function update__custom__metaboxes($post_id) {
		
		if (isset($_POST['youtube-url'])) {
			$youtube__url = isset($_POST['youtube-url']) ? trim((string)$_POST['youtube-url']) : '';
			
			$youtube__duration = AFHPYoutube::duration($youtube__url, tmp__customize("youtube__api__key"));
			
			if (!isset($youtube__duration->$youtube__url)) {
				$errors = get_option(tmp__prefix("_custom_post_type_save_error"));
				
				if (empty($errors))
					$errors = [];
				
				$errors[] = (object)[
					"status" => "error",
					"code" => "youtube_video_id_error",
					"message" => __("Invalid youtube video id", TMP__LANG)
				];
				
				update_option(tmp__prefix("_custom_post_type_save_error"), $errors);
				
				delete_post_meta($post_id, "youtube__url");
				delete_post_meta($post_id, "youtube__duration");
			} else {
				update_post_meta($post_id, 'youtube__url', $youtube__url);
				update_post_meta($post_id, 'youtube__duration', $youtube__duration->$youtube__url);
			}
		}
		
	}
	
	public function validation_notices() {
	
		$errors = get_option(tmp__prefix("_custom_post_type_save_error"));
		if (!empty($errors)) {
			foreach ($errors as $error)
				printf(
					'<div class="notice notice-%s is-dismissible"><p>%s</p></div>',
					$error->status,
					$error->message
				);
			
			delete_option(tmp__prefix("_custom_post_type_save_error"));
		}
	}

	public function actions() {

		add_action('add_meta_boxes', [$this, 'custom__metaboxes']);
		add_action('save_post', [$this, 'update__custom__metaboxes']);
		
		add_action( 'admin_notices', [$this, "validation_notices"] );
	}

}

return new AFHPTypePlaylist;