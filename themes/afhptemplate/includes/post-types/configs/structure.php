<?php
/**
 * Structure post type and taxonomies
 *
 * Class AFHPTypeStructure
 */
class AFHPTypeStructure extends AFHPPostType {

  /**
   * Custom post type system name
   *
   * @var string
   */
  const NAME = "structure";

  /**
   * Custom post type config
   *
   * @return array
   */
  public function post() {

    return [
      "label" => __("Structure", TMP__LANG),

      "public" => TRUE,
	    "query_var" => TRUE,
	    "publicly_queryable" => TRUE,

	    "show_ui" => TRUE,
	    'show_in_rest' => TRUE,

	    "rewrite" => [
	    	"slug" => "structure",
		    "with_front" => FALSE,
	    ],

	    "has_archive" => TRUE,
	    "hierarchical" => TRUE,

	    "capability_type" => "post",
	    "menu_icon" => tmp__assets('img/svg/diagram.svg'),

	    "taxonomies" => [
		    "structure__category"
	    ],
      "supports" => [
        "title",
        "editor",
        "thumbnail",
        "excerpt"
      ],
    ];
  }

  /**
   * Custom taxonomies config
   *
   * @return array[]
   */
  public function taxonomy() {

    return [
      "structure__category" => [
        "label" => __("Structure categories", TMP__LANG),
        "description" => __("Categories for structure", TMP__LANG),

        "public" => TRUE,

	      "show_ui" => TRUE,
	      "has_archive" => TRUE,
	      "show_in_rest" => TRUE,

	      "rewrite" => [
		      "with_front" => FALSE,
		      'slug' => 'structures',
	      ],
	      "hierarchical" => TRUE,
      ]
    ];
  }

  /**
   * Adding custom fields on taxonomy page
   *
   * @param WP_Term $term
   */
  public function structure__category_fields($term) {

    $checked = FALSE;
    $thumbnail = [
      "id" => 0,
      "src" => ''
    ];

    if ($term instanceof WP_Term) {
      $checked = get_term_meta($term->term_id, 'show_in_main', TRUE);
      $thumbnail['id'] = get_term_meta($term->term_id, 'thumbnail', TRUE);
      $thumbnail['src'] = wp_get_attachment_url($thumbnail['id']);
    }

    printf(
      '<tr class="form-field form-required term-thumbnail-wrap">
        <th scope="row">
          <label for="show-in-main">%s</label>
        </th>
        <td>
          <label class="%s">
            <a href="#" class="remove">%s</a>
            <input type="number" name="thumbnail" value="%d" />
            %s
          </label>
          <p class="description">%s</p>
        </td>
      </tr>',
      __('Thumbnail', TMP__LANG),
      tmp__prefix('field--file'),
      __('remove'),
      (int)$thumbnail['id'],
      !empty($thumbnail['src']) ? '<img src="' . $thumbnail['src'] .'" />' : '',
      __('Structure category thumbnail', TMP__LANG)
    );

    printf(
      '<tr class="form-field form-required term-show-in-main-wrap">
        <th scope="row">
          <label for="show-in-main">%s</label>
        </th>
        <td>
          <input  %s
                  value="1"
                  size="40"
                  type="checkbox"
                  id="show-in-main"
                  name="show-in-main"
                 />
          <p class="description">%s</p>
        </td>
      </tr>',
      __('Show in main', TMP__LANG),
      $checked ? 'checked="checked"' : '',
      __('Enable to show in structure page.')
    );
  }

  /**
   * Saving custom fields
   *
   * @param int $term_id
   */
  public function structure__category_update($term_id) {
    $thumbnail = isset($_POST['thumbnail']) ? (int)$_POST['thumbnail'] : 0;
    update_term_meta($term_id, 'thumbnail', $thumbnail);

    $show_in_main = isset($_POST['show-in-main']) ? 1 : 0;
    update_term_meta($term_id, 'show_in_main', $show_in_main);
  }

  /**
   * Custom post type and taxonomy additional actions and hooks
   * Called after registration
   */
  public function actions() {

    add_action('structure__category_edit_form_fields', [$this, 'structure__category_fields'], 10, 2);

    add_action('crated_structure__category', [$this, 'structure__category_update'], 10, 2);
    add_action('edited_structure__category', [$this, 'structure__category_update'], 10, 2);

    add_filter('manage_edit-structure__category_columns', function($columns) {
      $columns['thumbnail'] = __('Thumbnail', TMP__LANG);
      $columns['show_in_main'] = __('Show in main', TMP__LANG);

      return $columns;
    });

    add_filter("manage_structure__category_custom_column", function($output, $column_name, $term_id) {

	    switch ($column_name) {
        case 'show_in_main':
	        $show_in_main = get_term_meta($term_id, 'show_in_main', true);
	        $output = sprintf(
            /** @lang text */
            '<input %s
                    value="%d"
                    type="checkbox"
                    data-meta_key="show_in_main"
                    class="field--checkbox field--ajax"
                  />',
            $show_in_main ? 'checked="checked"' : '',
            $term_id
          );
          break;
        case 'thumbnail':
          $id = get_term_meta($term_id, 'thumbnail', true);
          $src = wp_get_attachment_url($id);

          $output = sprintf(
          	'<div>
							<label class="%s" data-term_id="%d">
								<a href="#" class="remove">%s</a>
								<input type="number" value="%d" />
								%s
							</label>
						</div>',
	          tmp__prefix('field--file field--file__ajax'),
	          $term_id,
	          __('remove', TMP__LANG),
	          $id ? $id : 0,
	          $src ? '<img src="' . $src . '" />' : ''
          );
          break;
        default:
          break;
      }

      return $output;
    }, 10, 3);
   
  }
  
}

return new AFHPTypeStructure;