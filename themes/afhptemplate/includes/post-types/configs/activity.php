<?php
/**
 * Activity post type and taxonomies
 *
 * Class AFHPTypeActivity
 */
class AFHPTypeActivity extends AFHPPostType {

	const NAME = 'activity';

	public function post() {

		return [
			"label" => __("Activity", TMP__LANG),

			"public" => TRUE,
			"publicly_queryable" => TRUE,

			"show_ui" => TRUE,
			"show_in_rest" => TRUE,

			"query_var" => TRUE,

			"has_archive" => TRUE,
			"hierarchical" => TRUE,

			"capability_type" => "post",
			"menu_icon" => tmp__assets('img/svg/support.svg'),

			"rewrite" => [
				"slug" => "activity",
				"with_font" => FALSE,
			],
			"taxonomies" => [
				"activity__category",
			],
			"supports" => [
				"title",
				"editor",
				"thumbnail",
				"excerpt"
			]
		];
	}

	public function taxonomy() {

		return [
			"activity__category" => [
				"label" => __("Activity categories", TMP__LANG),
				"description" => __("Categories for activity", TMP__LANG),

				"public" => TRUE,

				"show_ui" => TRUE,
				"show_in_rest" => TRUE,

				"rewrite" => [
					"with_front" => FALSE,
					"slug" => "activities",
				],
				"hierarchical" => TRUE,
			]
		];
	}
	
	/**
	 * @param WP_Term $term
	 */
	public function activity__category_fields($term) {
		
		$color = '';
		$show_in_front = FALSE;
		
		$thumbnail = [
			"id" => 0,
			"src" => ''
		];
		
		if ($term instanceof WP_Term) {
			$color = get_term_meta($term->term_id, 'color', TRUE);
			$show_in_front = (int)get_term_meta($term->term_id, 'show_in_front', TRUE);
			
			$thumbnail['id'] = get_term_meta($term->term_id, 'thumbnail', TRUE);
			$thumbnail['src'] = wp_get_attachment_url($thumbnail['id']);
		}

		printf(
			'<tr class="form-field form-required term-color-wrap">
        <th scope="row">
          <label for="color">%s</label>
        </th>
        <td>
          <input  id="color"
                  value="%s"
                  name="color"
                  class="field--color"
                 />
          <p class="description">%s</p>
        </td>
      </tr>',
			__('Color', TMP__LANG),
			$color,
			__('Activity category color', TMP__LANG)
		);
		
		printf(
			'<tr class="form-field form-required term-thumbnail-wrap">
        <th scope="row">
          <label for="show-in-main">%s</label>
        </th>
        <td>
          <label class="%s">
            <a href="#" class="remove">%s</a>
            <input type="number" name="thumbnail" value="%d" />
            %s
          </label>
          <p class="description">%s</p>
        </td>
      </tr>',
			__('Thumbnail', TMP__LANG),
			tmp__prefix('field--file'),
			__('remove'),
			(int)$thumbnail['id'],
			!empty($thumbnail['src']) ? '<img src="' . $thumbnail['src'] .'" />' : '',
			__('Structure category thumbnail', TMP__LANG)
		);

		printf(
			'<tr class="form-field form-required term-color-wrap">
        <th scope="row">
          <label for="show-in-front">%s</label>
        </th>
        <td>
          <input  %s
                  value="1"
                  type="checkbox"
                  id="show-in-front"
                  name="show-in-front"
                  class="field--show-in-front"
                 />
          <p class="description">%s</p>
        </td>
      </tr>',
			__('Show in front', TMP__LANG),
			$show_in_front ? 'checked="checked"' : '',
			__('Show activity in home page', TMP__LANG)
		);
		
	}

	public function activity__category_update($term_id) {

		$color = isset($_POST['color']) ? trim($_POST['color']) : NULL;
		update_term_meta($term_id, 'color', $color);

		$show_in_front = isset($_POST['show-in-front']) ? 1 : 0;
		update_term_meta($term_id, 'show_in_front', $show_in_front);
		
		$thumbnail = isset($_POST['thumbnail']) ? (int)$_POST['thumbnail'] : 0;
		update_term_meta($term_id, 'thumbnail', $thumbnail);
	}

	public function actions() {

		add_action('activity__category_edit_form_fields', [$this, 'activity__category_fields'], 10, 2);

		add_action('crated_activity__category', [$this, 'activity__category_update'], 10, 2);
		add_action('edited_activity__category', [$this, 'activity__category_update'], 10, 2);

		add_filter('manage_edit-activity__category_columns', function($columns) {
			$columns['color'] = __('Color', TMP__LANG);
			$columns['thumbnail'] = __('Thumbnail', TMP__LANG);
			$columns['show_in_front'] = __('Show in front', TMP__LANG);

			return $columns;
		});

		add_filter("manage_activity__category_custom_column", function($output, $column_name, $term_id) {
			$color = get_term_meta($term_id, 'color', TRUE);
			$show_in_front = (bool)get_term_meta($term_id, 'show_in_front', TRUE);

			switch ($column_name) {
				case 'color':
					$output = sprintf(
						/** @lang text */
						'<input value="%s"
										data-term_id="%d"
                    class="field--color field--ajax"
                  />',
						$color,
						$term_id
					);
					break;
				case 'thumbnail':
					$id = get_term_meta($term_id, 'thumbnail', true);
					$src = wp_get_attachment_url($id);
					
					$output = sprintf(
						'<div>
							<label class="%s" data-term_id="%d">
								<a href="#" class="remove">%s</a>
								<input type="number" value="%d" />
								%s
							</label>
						</div>',
						tmp__prefix('field--file field--file__ajax'),
						$term_id,
						__('remove', TMP__LANG),
						$id ? $id : 0,
						$src ? '<img src="' . $src . '" />' : ''
					);
					break;
				case 'show_in_front':
					$output = sprintf(
						/** @lang text */
						'<input %s
 										value="%d"
										type="checkbox"
 										data-meta_key="show_in_front"
 										class="field--checkbox field--ajax" />',
						$show_in_front ? 'checked="checked"' : '',
						$term_id
					);
					break;
				default:
					break;
			}

			return $output;
		}, 10, 3);

		add_action('wp_ajax_' . tmp__prefix('update__term__meta'), function () {

			$meta = isset($_POST['meta']) ? json_decode(stripslashes($_POST['meta'])) : new stdClass();

			if ($meta instanceof stdClass)
				if (!empty($meta->key) && !empty($meta->id))
					update_term_meta($meta->id, $meta->key, $meta->value);

			print json_encode($meta);

			die();
		});

	}

}

new AFHPTypeActivity;