<?php
/**
 * Discussion post type and taxonomies
 *
 * Class AFHPTypeDiscussion
 */
class AFHPTypeDiscussion extends AFHPPostType {

	const NAME = "discussion";

	public function post() {

		return [
			"label" => __("Discussion", TMP__LANG),

			"public" => TRUE,
			"publicly_queryable" => TRUE,

			"show_ui" => TRUE,
			'show_in_rest' => TRUE,

			"rewrite" => TRUE,
			"query_var" => TRUE,

			"has_archive" => TRUE,
			"hierarchical" => TRUE,

			"capability_type" => "post",
			"menu_icon" => tmp__assets('img/svg/qa.svg'),

			"supports" => [
				"title",
				"editor",
				"excerpt",
				"thumbnail",
			],
		];
	}

	public function taxonomy() {
		// TODO: Implement taxonomy() method.
	}

	public function voting__metabox($post) {

		$no = get_post_meta($post->ID, 'voting__no', TRUE);
		$yes = get_post_meta($post->ID, 'voting__yes', TRUE);
		$hide = get_post_meta($post->ID, 'voting__hide', TRUE);

		printf(
			/** @lang text */
			'<div class="%s">
				<label>
					<span>%s</span>
					<input name="voting-yes" value="%d" max="100" min="0" step="1" />
					<i>%%</i>
				</label>
				<label>
					<span>%s</span>
					<input name="voting-no" value="%d" max="100" min="0" step="1" />
					<i>%%</i>
				</label>
				<label>
					<span>%s</span>
					<input 	%s
									type="checkbox" 
									name="voting-hide" />
				</label>
			</div>',
			tmp__prefix('voting__metabox'),
			__('Yes', TMP__LANG),
			$yes,
			__('No', TMP__LANG),
			$no,
			__('Hide', TMP__LANG),
			$hide ? 'checked="checked"' : ''
		);
	}

	public function custom__metaboxes() {

		add_meta_box(
			tmp__prefix($this::NAME . '_voting'),
			__('Voting', TMP__LANG),
			[$this, 'voting__metabox'],
			$this::NAME,
			'side',
			'default'
		);

	}

	public function update__custom__metaboxes($post_id) {

		$hide = isset($_POST['voting-hide']);
		$yes = isset($_POST['voting-yes']) ? (int)$_POST['voting-yes'] : 0;
		$no = isset($_POST['voting-no']) ? (int)$_POST['voting-no'] : 0;

		update_post_meta($post_id, 'voting__no', $no);
		update_post_meta($post_id, 'voting__yes', $yes);
		update_post_meta($post_id, 'voting__hide', $hide);
	}

	public function actions() {

		add_action('add_meta_boxes', [$this, 'custom__metaboxes']);
		add_action('save_post', [$this, 'update__custom__metaboxes']);
	}

}

return new AFHPTypeDiscussion;