<?php
/**
 * Mission post type and taxonomies
 *
 * Class AFHPTypeMission
 */
class AFHPTypeMission  extends AFHPPostType {

	const NAME = "mission";

	public function post() {

		return [
			"label" => __("Mission", TMP__LANG),

			"public" => TRUE,
			"publicly_queryable" => TRUE,

			"show_ui" => TRUE,

			"rewrite" => TRUE,
			"query_var" => TRUE,

			"has_archive" => TRUE,
			"hierarchical" => TRUE,

			"capability_type" => "post",
			"menu_icon" => tmp__assets('img/svg/mission.svg'),

			"supports" => [
				"title",
				"thumbnail",
			],
		];
	}

	public function taxonomy() {
		// TODO: Implement taxonomy() method.
	}

	public function metabox__index($post) {

		$value = (int)get_post_meta($post->ID, 'index', TRUE);

		printf(
			'<input type="number" name="index" value="%d" />',
			$value
		);
	}

	public function custom__metaboxes() {

		add_meta_box(
			tmp__prefix('index'),
			__('Index', TMP__LANG),
			[$this, 'metabox__index'],
			'mission',
			'side'
		);
	}

	public function update__custom__metaboxes($post_id) {

		$value = isset($_POST['index']) ? (int)$_POST['index'] : 0;

		update_post_meta($post_id, 'index', $value);

	}

	public function actions() {

		add_action('add_meta_boxes', [$this, 'custom__metaboxes']);
		add_action('save_post', [$this, 'update__custom__metaboxes']);

	}

}

return new AFHPTypeMission;