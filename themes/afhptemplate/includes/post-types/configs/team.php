<?php
/**
 * Team post type and taxonomies
 *
 * Class AFHPTypeTeam
 */
class AFHPTypeTeam extends AFHPPostType {

	const NAME = "team";

	public function post() {

		return [
			"label" => __("Team", TMP__LANG),

			"public" => TRUE,
			"query_var" => TRUE,
			"publicly_queryable" => TRUE,

			"show_ui" => TRUE,
			'show_in_rest' => TRUE,


			"has_archive" => TRUE,
			"hierarchical" => TRUE,

			"capability_type" => "post",
			"menu_icon" => tmp__assets('img/svg/party.svg'),

			"rewrite" => [
				"slug" => "team",
				"with_front" => FALSE,
			],
			
			"taxonomies" => [
				"team__category"
			],
			
			"supports" => [
				"title",
				"thumbnail"
			],
		];
	}
	
	public function taxonomy() {

		return [
			"team__category" => [
				"label" => __("Team categories", TMP__LANG),
				"description" => __("Categories for team", TMP__LANG),

				"public" => TRUE,

				"show_ui" => TRUE,
				"show_in_rest" => TRUE,

				"hierarchical" => TRUE,
				
				"rewrite" => [
					"slug" => "teams",
					"with_front" => FALSE,
				],
			]
		];
	}
	
	/**
	 * Adding custom fields on taxonomy page
	 *
	 * @param WP_Term $term
	 */
	public function team__category_fields($term) {
		
		$thumbnail = [
			"id" => 0,
			"src" => ''
		];
		
		if ($term instanceof WP_Term) {
			$thumbnail['id'] = get_term_meta($term->term_id, 'thumbnail', TRUE);
			$thumbnail['src'] = wp_get_attachment_url($thumbnail['id']);
		}
		
		printf(
			'<tr class="form-field form-required term-thumbnail-wrap">
        <th scope="row">
          <label for="show-in-main">%s</label>
        </th>
        <td>
          <label class="%s">
            <a href="#" class="remove">%s</a>
            <input type="number" name="thumbnail" value="%d" />
            %s
          </label>
          <p class="description">%s</p>
        </td>
      </tr>',
			__('Thumbnail', TMP__LANG),
			tmp__prefix('field--file'),
			__('remove'),
			(int)$thumbnail['id'],
			!empty($thumbnail['src']) ? '<img src="' . $thumbnail['src'] .'" />' : '',
			__('Structure category thumbnail', TMP__LANG)
		);
		
	}/**
 * Saving custom fields
 *
 * @param int $term_id
 */
	public function team__category_update($term_id) {
		
		$thumbnail = isset($_POST['thumbnail']) ? (int)$_POST['thumbnail'] : 0;
		update_term_meta($term_id, 'thumbnail', $thumbnail);
	}
	
	/**
	 * Custom post type and taxonomy additional actions and hooks
	 * Called after registration
	 */
	public function actions() {
		
		add_action('team__category_edit_form_fields', [$this, 'team__category_fields'], 10, 2);
		
		add_action('crated_team__category', [$this, 'team__category_update'], 10, 2);
		add_action('edited_team__category', [$this, 'team__category_update'], 10, 2);
		
		add_filter('manage_edit-team__category_columns', function($columns) {
			$columns['thumbnail'] = __('Thumbnail', TMP__LANG);
			
			return $columns;
		});
		
		add_filter("manage_team__category_custom_column", function($output, $column_name, $term_id) {
			
			switch ($column_name) {
				case 'thumbnail':
					$id = get_term_meta($term_id, 'thumbnail', true);
					$src = wp_get_attachment_url($id);
					
					$output = sprintf(
						'<div>
							<label class="%s" data-term_id="%d">
								<a href="#" class="remove">%s</a>
								<input type="number" value="%d" />
								%s
							</label>
						</div>',
						tmp__prefix('field--file field--file__ajax'),
						$term_id,
						__('remove', TMP__LANG),
						$id ? $id : 0,
						$src ? '<img src="' . $src . '" />' : ''
					);
					break;
				default:
					break;
			}
			
			return $output;
		}, 10, 3);
		
	}

}

return new AFHPTypeTeam;