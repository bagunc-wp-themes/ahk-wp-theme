<?php
/**
 * Post types
 *
 *  1. structure +
 *  2. activity +
 *  3. gallery +
 *  4. playlist +
 *  5. team +
 *  6. discussion +
 *  7. mission +
 *
 */
class AFHPCustomPostTypes {

  private static $instance = NULL;

  private $registered = FALSE;

  private static $list = [];

  public static function execute() {
    if (self::$instance)
      return self::$instance;

    return new self;
  }

	/**
	 * Checking is our post type
	 *
	 * @param $value
	 *
	 * @return bool
	 */
  public static function native($value) {

  	if (is_string($value))
		  $value = [$value];

  	foreach ($value as $item)
  		if (!in_array($item, self::$list))
  			return FALSE;

    return TRUE;
  }

  public function register() {
    if ($this->registered)  return;

    $files = scan__dir('includes/post-types/configs', function($filename) {
      $file = pathinfo(tmp__path("includes/post-types/configs/$filename"));
      return $file["extension"] === 'php';
    });

	  require__once('includes/post-types/configs/interface.php');
	  require__once('includes/post-types/configs/abstract.php');

    foreach ($files as $file) {
      require__once("includes/post-types/configs/$file");

      $className = "AFHPType" . ucfirst(str_replace('.php', '', $file));

      if (!class_exists($className)) continue;
      
	    /**
	     * @var AFHPPostType $instance
	     */
      $instance = new $className();

      $options = $instance->post();
      $taxonomies = $instance->taxonomy();

      if (!empty($taxonomies))
        foreach ($taxonomies as $tax__name =>  $taxonomy)
          register_taxonomy($tax__name, [$instance::NAME], $taxonomy);

      if (!empty($options))
        register_post_type($instance::NAME, $options);

      if (method_exists($instance,'actions'))
        $instance->actions();

      self::$list[] = $instance::NAME;
    }

    $this->registered = TRUE;
  }
}