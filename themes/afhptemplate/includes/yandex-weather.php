<?php
class AFHPYandexWeather {
	
	/**
	 * API uri
	 *
	 * @var string
	 */
	public $uri = "https://api.weather.yandex.ru/v1/forecast";
	
	/**
	 * API key
	 *
	 * @var string
	 */
	private $key;
	
	/**
	 * Request method
	 *
	 * @var string
	 */
	private $method = "GET";
	
	/**
	 * Coords -> latitude
	 *
	 * @var float
	 */
	public $latitude;
	
	/**
	 * Coords -> longitude
	 *
	 * @var float
	 */
	public $longitude;
	
	/**
	 * API response language code
	 *
	 * available [
	 *  ru_UA => Russia
	 *  ru_UA"=> Ukraine
	 *  uk_UA => Ukraine
	 *  be_BY => Belarus
	 *  kk_KZ => Kazakhstan
	 *  tr_TR => Turkey
	 *  en_US => international english
	 * ]
	 *
	 * @var string
	 */
	public $language;
	
	/**
	 * API response language short code
	 *
	 * @var mixed|string
	 */
	private $lang;
	
	/**
	 * Server time in Unixtime format
	 *
	 * @var string
	 */
	public $now;
	
	/**
	 * Server time in UTC
	 *
	 * @var string
	 */
	public $now_utc;
	
	/**
	 * Information about the settlement.
	 *
	 * @var stdClass
	 */
	public $info;
	
	/**
	 * Actual weather information
	 *
	 * @var stdClass
	 */
	public $fact;
	
	/**
	 * Forecast weather information
	 *
	 * @var stdClass[]
	 */
	public $forecast;
	
	/**
	 * Request data collector
	 *
	 * @var stdClass
	 */
	private $request;
	
	/**
	 * Response data recipient
	 *
	 * @var stdClass
	 */
	private $response;
	
	/**
	 * Response errors list
	 *
	 * @var array
	 */
	public $errors;
	
	/**
	 * AFHPYandexWeather constructor.
	 *
	 * @param string|null $key
	 * @param float|null $lat
	 * @param float|null $lon
	 * @param string|null $lang
	 */
	public function __construct($key = NULL, $lat = NULL, $lon = NULL, $lang = "en_US") {
		
		$this->key = $key;
		
		$this->latitude = $lat;
		$this->longitude = $lon;
		
		$this->language = $lang;
		$this->lang = explode("_", $this->language)[0];
		
		$this->errors = [];
		
		/** Creating request data */
		$this->request();
	}
	
	/**
	 * Creating api request
	 *
	 * @return stdClass
	 */
	private function request() {
		
		$this->request = new stdClass();
		
		$this->request->context = stream_context_create([
			"http" => [
				"method" => $this->method,
				"header" => "X-Yandex-API-Key: {$this->key}"
			]
		]);
		
		$this->request->url = implode("?", [
			$this->uri,
			http_build_query([
				"lang" => $this->language,
				
				"lat" => $this->latitude,
				"lon" => $this->longitude,
				
				"limit" => 1,
				"hours" => "false",
				"extra" => "false",
			])
		]);

		return $this->request;
	}
	
	/**
	 * Fetching api data
	 *
	 * @return mixed|stdClass
	 */
	private function fetch() {
		
		$curl = curl_init();
		curl_setopt($curl, CURLOPT_URL, $this->request->url);
		
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);
		
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, FALSE);
		
		curl_setopt($curl, CURLOPT_HTTPHEADER, [
			"Accept-Language: {$this->lang}",
			"X-Yandex-API-Key: {$this->key}",
			"Content-Type: application/json; charset=utf-8"
		]);
		
		$this->response = curl_exec($curl);
		curl_close($curl);
		
		$this->response = json_decode(stripslashes(
			$this->response
		));
		
		$this->processing();
		
		return $this->response;
	}
	
	/**
	 * Processing data
	 *
	 * @return bool
	 */
	public function processing() {
		if (isset($this->response->status) && $this->response->status !== 200) {
			$this->errors[] = $this->response;
			
			return FALSE;
		}
		
		$this->now = $this->response->now;
		$this->now_utc = $this->response->now_dt;
		
		$this->info = $this->response->info;
		
		$this->fact = $this->response->fact;
		$this->forecast = $this->response->forecasts;
		
		return TRUE;
	}
	
	/**
	 * Getting data for now
	 *
	 * @param string|array $needs Possible values [
	 *  temp [number]           =>    Temperature (° C)
	 *  feels_like [number]     =>    Perceived temperature (° C)
	 *  temp_water [number]     =>    Water temperature (° C). The parameter is returned for settlements where this information is relevant
	 *  icon [string]           =>    Weather icon code. The icon is available at https://yastatic.net/weather/i/icons/blueye/color/svg/<value from the icon>.svg field
	 *  condition [string]      =>    Weather description decryption code. Possible values [
	 *      hail
	 *      rain
	 *      snow
	 *      clear
	 *      cloudy
	 *      drizzle
	 *      showers
	 *      overcast
	 *      wet-snow
	 *      light-rain
	 *      heavy-rain
	 *      light-snow
	 *      snow-showers
	 *      thunderstorm
	 *      partly-cloudy
	 *      moderate-rain
	 *      continuous-heavy-rain
	 *      thunderstorm-with-rain
	 *      thunderstorm-with-hail
	 *  ]
	 *  wind_speed [number]     =>    Wind speed (in m / s)
	 *  wind_gust [number]      =>    Wind gust speed (in m / s)
	 *  wind_dir [string]       =>    Direction of the wind. Possible values => [
	 *      c   =>  calm
	 *      w   =>  western
	 *      e   =>  eastern
	 *      n   =>  northern
	 *      s   =>  southern
	 *      nw  =>  northwest
	 *      ne  =>  north-east
	 *      se  =>  southeast
	 *      sw  =>  southwestern
	 *  ]
	 *  pressure_mm [number]    =>    Pressure (in mmHg)
	 *  pressure_pa [number]    =>    Pressure (in hectopascals
	 *  humidity [number]       =>    Air humidity (in percent)
	 *  uv_index [number]       =>    ???
	 *  soil_temp [number]      =>    soil temperature
	 *  soil_moisture [number]  =>    soil moisture
	 *  daytime [string]        =>    Light or dark time of day. Possible values [
	 *      n   =>  night time
	 *      d   =>  daylight hours
	 *  ]
	 *  polar [boolean]         =>    Indicates that the time of day specified in the daytime field is polar
	 *  season [string]         =>    The time of year in this locality. Possible values [
	 *      summer
	 *      autumn
	 *      winter
	 *      spring
	 *  ]
	 *  obs_time [number]       =>    Measurement time of weather data in Unixtime format
	 *  source [string]         =>    ???
	 * ]
	 *
	 * @return stdClass|array
	 */
	public function now($needs = "*") {
		
		if (empty($this->fact))
			$this->fetch();
		
		if ($this->have_error()) return $this->errors;
		
		if ($needs === "*")
			return $this->fact;
		
		if (!is_array($needs)) return NULL;
		
		$result = new stdClass();
		foreach ($needs as $key) {
			if (isset($this->fact->$key)) {
				$value = $this->fact->$key;
				switch ($key) {
					case "icon":
						$result->$key = $this->icon($value);
						break;
					default:
						$result->$key = $value;
						break;
				}
			} else {
				$result->$key = "Invalid key!";
			}
		}
		
		return (object)$result;
	}
	
	/**
	 * Getting data by days.
	 *
	 * @param string|int|int[]|null $day Possible values => [
	 *    NULL === *      =>  All available days
	 *    today === 1     =>  This day
	 *    tomorrow === 2  =>  Tomorrow
	 *    3               =>  +3 day
	 *    ...
	 * ]
	 * @param string|array $needs Possible values => [
	 *    date [string]       =>  Forecast date in YYYY-MM-DD format
	 *    date_ts [number]    =>  Forecast date in Unixtime format
	 *    week [number]       =>  Week number
	 *    sunrise [string]    =>  Sunrise time, local time (may not be available for polar regions)
	 *    sunset [string]     =>  Sunset time, local time (may not be available for polar regions)
	 *    moon_code [number]  =>  Moon phase code. Possible values => [
	 *        0       =>  full moon
	 *        1-3     =>  waning moon
	 *        4       =>  last quarter
	 *        5-7     =>  waning moon
	 *        8       =>  new moon
	 *        9-11    =>  waxing crescent
	 *        12      =>  first quarter
	 *        12-15   =>  waxing crescent
	 *    ]
	 *    moon_text [string]  =>  Moon phase text code. Possible values => [
	 *        moon-code-0   =>  full moon
	 *        moon-code-1   =>  waning moon
	 *        moon-code-2   =>  waning moon
	 *        moon-code-3   =>  waning moon
	 *        moon-code-4   =>  last quarter
	 *        moon-code-5   =>  waning moon
	 *        moon-code-6   =>  waning moon
	 *        moon-code-7   =>  waning moon
	 *        moon-code-8   =>  new moon
	 *        moon-code-9   =>  waxing crescent
	 *        moon-code-10  =>  waxing crescent
	 *        moon-code-11  =>  waxing crescent
	 *        moon-code-12  =>  first quarter
	 *        moon-code-13  =>  waxing crescent
	 *        moon-code-14  =>  waxing crescent
	 *        moon-code-15  =>  waxing crescent
	 *    ]
	 *    parts [stdClass] => Time of day forecasts. Contains the following fields. Possible values => [
	 *        day [stdclass] => All weather forecasts for the time of day have the same set of fields. The answer contains a forecast for the next 2 periods.
	 *        night [stdclass] => All weather forecasts for the time of day have the same set of fields. The answer contains a forecast for the next 2 periods.
	 *        morning [stdclass] => All weather forecasts for the time of day have the same set of fields. The answer contains a forecast for the next 2 periods.
	 *        evening [stdclass] => All weather forecasts for the time of day have the same set of fields. The answer contains a forecast for the next 2 periods.
	 *        day_short [stdclass] => All weather forecasts for the time of day have the same set of fields. The answer contains a forecast for the next 2 periods.
	 *        night_short [stdclass] => All weather forecasts for the time of day have the same set of fields. The answer contains a forecast for the next 2 periods.
	 *    ]
	 *    part_name [string]      =>  The name of the time of day. Possible values => [
	 *        night
	 *        morning
	 *        day
	 *        evening
	 *    ]
	 *    temp_min [number]       =>  Minimum temperature for time of day (° C)
	 *    temp_max [number]       =>  Maximum temperature for time of day (° C)
	 *    temp_avg [number]       =>  Average temperature for time of day (° C)
	 *    feels_like [number]     =>  Perceived temperature (° C)
	 *    icon [string]           =>  Weather icon code. The icon is available at https://yastatic.net/weather/i/icons/blueye/color/svg/<value from the icon>.svg field
	 *    condition [string]      =>  Weather description decryption code. Possible values [
	 *        hail
	 *        rain
	 *        snow
	 *        clear
	 *        cloudy
	 *        drizzle
	 *        showers
	 *        overcast
	 *        wet-snow
	 *        light-rain
	 *        heavy-rain
	 *        light-snow
	 *        snow-showers
	 *        thunderstorm
	 *        partly-cloudy
	 *        moderate-rain
	 *        continuous-heavy-rain
	 *        thunderstorm-with-rain
	 *        thunderstorm-with-hail
	 *    ]
	 *    daytime [string]        =>  Light or dark time of day. Possible values [
	 *        n   =>  night time
	 *        d   =>  daylight hours
	 *    ]
	 *    polar [boolean]         =>  Indicates that the time of day specified in the daytime field is polar
	 *    wind_speed [number]     =>  Wind speed (in m / s)
	 *    wind_gust [number]      =>  Wind gust speed (in m / s)
	 *    wind_dir [string]       =>  Direction of the wind. Possible values => [
	 *        c   =>  calm
	 *        w   =>  western
	 *        e   =>  eastern
	 *        n   =>  northern
	 *        s   =>  southern
	 *        nw  =>  northwest
	 *        ne  =>  north-east
	 *        se  =>  southeast
	 *        sw  =>  southwestern
	 *    ]
	 *    pressure_mm [number]    =>  Pressure (in mmHg)
	 *    pressure_pa [number]    =>  Pressure (in hectopascals)
	 *    humidity [number]       =>  Air humidity (in percent)
	 *    prec_mm [number]        =>  Forecast rainfall (in mm)
	 *    prec_period [number]    =>  Forecasted precipitation period (in minutes)
	 *    prec_prob [number]      =>  Probability of precipitation
	 * ]
	 *
	 * @return stdClass|stdClass[]|array
	 */
	public function days($day = NULL, $needs = "*")
	{
		
		if (!$this->forecast)
			$this->fetch();
		
		if ($this->have_error()) return $this->errors;
		
		if ($day === "*" && $needs === "*")
			return $this->forecast;
		
		$days = [];
		
		if (is_numeric($day)) {
			$days[] = $this->forecast[$day];
		} else if (is_string($day)) {
			if ($day === "today") {
				$days[] = $this->forecast[0];
			} else if ($day === "tomorrow") {
				$days[] = $this->forecast[1];
			}
		} else if (is_array($day)) {
			foreach ($day as $i)
				$days[] = $this->forecast[$i];
		}
		
		$result = [];
		
		if ($needs === "*") {
			$result = $days;
		} elseif (is_array($needs)) {
			foreach ($days as $index => $day) {
				$std = new stdClass();
				foreach ($needs as $key)
					if (isset($day->$key))
						$std->$key = $day->$key;
				
				$result[$index] = $std;
			}
		}
		
		return count($result) === 1 ? $result[0] : $result;
	}
	
	/**
	 * Response have errors checking
	 *
	 * @return bool
	 */
	public function have_error() {
		
		return count($this->errors) > 0;
	}
	
	/**
	 * Weather status icon
	 *
	 * @param $icon
	 *
	 * @return string
	 */
	public function icon($icon) {
		
		return sprintf(
			'<img alt="%s"
						class="weather__icon weather__icon--yandex weather__icon--%s"
						src="https://yastatic.net/weather/i/icons/blueye/color/svg/%s.svg"
					/>',
			$icon,
			$icon,
			$icon
		);
	}
	
	/**
	 * Data fetching task
	 *
	 * @param datetime $updated
	 * @param number $interval
	 * @param callable $callback
	 *
	 * @return null|false
	 */
	public function cron($updated, $interval, $callback) {
		
		if (!is_callable($callback)) return NULL;
		
		$diff = NULL;
		$now = new DateTime("now");
		
		if (!empty($updated)) {
			$diff = $now->diff($updated);
		}
		
		if (($diff && $diff->h >= $interval) || empty($updated)) {
			$this->fetch();
			
			$callback($this, $now);
		}
		
		return FALSE;
	}
	
}