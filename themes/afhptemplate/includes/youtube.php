<?php
class AFHPYoutube {
	
	public static $key = NULL;
	
	public static $url = "https://www.googleapis.com/youtube/v3/videos";
	
	/**
	 * @param $ids
	 * @param null $key
	 *
	 * @return stdClass|void
	 */
	public static function duration($ids, $key = NULL) {
		
		self::$key = $key ? $key : NULL;
		
		if (!self::$key) return;
		
		if (is_array($ids))
			$ids = implode(",", $ids);
		
		$uri = self::uri([
			"id" => $ids,
			"part" => "contentDetails",
		]);
		
		$response = file_get_contents($uri);
		
		$result = new stdClass();
		if (!empty($response)) {
			$response = json_decode($response);
			
			if (!empty($response->items)) {
				foreach ($response->items as $item) {
					$duration = $item->contentDetails->duration;
					
					$duration = str_replace(["PT", "S"], "", $duration);
					$duration = explode("M", $duration);
					
					$obj = (object)[
						"minutes" =>  0,
						"seconds" => 0,
					];
					
					if (isset($duration[1])) {
						$obj->minutes = sprintf('%02d', $duration[0]);
						$obj->seconds = sprintf('%02d', (int)$duration[1] - 1);
					} else {
						$obj->seconds = sprintf('%02d', (int)$duration[0] - 1);
					}
					
					$result->{$item->id} = $obj;
				}
			}
		}
		
		return $result;
	}
	
	public static function uri($args) {
		
		$default = [
			"key" => self::$key
		];
		
		$args = array_merge($args, $default);
		
		$query = [];
		
		foreach ($args as $key => $value)
			$query[] = "$key=$value";
		
		return implode(
			"?",
			[
				self::$url,
				implode("&", $query),
			]
		);
	}
	
}