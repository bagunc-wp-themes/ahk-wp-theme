<?php
$configs = [
	"structure" => [
		"banner--structure" => [
			"settings"   => tmp__prefix("structure__banners"),
			"label"      => __("Structure banner", TMP__LANG),
			"section"    => tmp__prefix("custom-settings"),
		],
	],
	"activity" => [
		"banner--activity" => [
			"settings"   => tmp__prefix("activity__banners"),
			"label"      => __("Activity banner", TMP__LANG),
			"section"    => tmp__prefix("custom-settings"),
		],
	],
	"team" => [
		"banner--team" => [
			"settings"   => tmp__prefix("team__banners"),
			"label"      => __("Team banner", TMP__LANG),
			"section"    => tmp__prefix("custom-settings"),
		],
	],
	"gallery" => [
		"banner--gallery" => [
			"settings"   => tmp__prefix("gallery__banners"),
			"label"      => __("Gallery banner", TMP__LANG),
			"section"    => tmp__prefix("custom-settings"),
		],
	],
	"playlist" => [
		"banner--playlist" => [
			"settings"   => tmp__prefix("playlist__banners"),
			"label"      => __("Playlist banner", TMP__LANG),
			"section"    => tmp__prefix("custom-settings"),
		],
	],
	"discussion" => [
		"banner--discussion" => [
			"settings"   => tmp__prefix("discussion__banners"),
			"label"      => __("Discussion banner", TMP__LANG),
			"section"    => tmp__prefix("custom-settings"),
		],
	],
	"donation__section" => [
		"banner--discussion" => [
			"settings"   => tmp__prefix("donation__section__banner"),
			"label"      => __("Donation section banner", TMP__LANG),
			"section"    => tmp__prefix("custom-settings"),
		],
	],
];

$info = [
	"phone" => [
		"label"      => __("Phone number", TMP__LANG),
		"section"    => tmp__prefix("custom-info"),
	],
	"email" => [
		"label"      => __("E-mail", TMP__LANG),
		"section"    => tmp__prefix("custom-info"),
	],
	"youtube__api__key" => [
		"label"      => __("Youtube api key", TMP__LANG),
		"section"    => tmp__prefix("custom-info"),
	],
	"yandex__weather__api__key" => [
		"label"      => __("Yandex weather api key", TMP__LANG),
		"section"    => tmp__prefix("custom-info"),
	],
	"yandex__weather__api__lat" => [
		"label"      => __("Yandex weather location latitude", TMP__LANG),
		"section"    => tmp__prefix("custom-info"),
	],
	"yandex__weather__api__lon" => [
		"label"      => __("Yandex weather location longitude", TMP__LANG),
		"section"    => tmp__prefix("custom-info"),
	],
	"facebook__link" => [
		"label"      => "Facebook",
		"section"    => tmp__prefix("custom-info"),
	],
	"instagram__link" => [
		"label"      => "Instagram",
		"section"    => tmp__prefix("custom-info"),
	],
	"twitter__link" => [
		"label"      => "Twitter",
		"section"    => tmp__prefix("custom-info"),
	],
];

add_action("customize_register", function($wp_customize) use ($configs, $info) {
	/**
	 * @var WP_Customize_Manager $wp_customize
	 */
	
	$wp_customize->add_section(
		tmp__prefix('custom-settings'),
		[
			"priority" => 0,
			"description" => NULL,
			"title" => __("Page Banners", TMP__LANG),
		]
	);
	
	foreach ($configs as $key => $config) {
		foreach ($config as $option) {
			$wp_customize->add_setting(
				$option["settings"],
				[
					"default" => NULL,
				]
			);
			
			$wp_customize->add_control(
				new WP_Customize_Image_Control(
					$wp_customize,
					$key,
					$option
				)
			);
		}
	}
	
	$wp_customize->add_section(
		tmp__prefix('custom-info'),
		[
			"priority" => 0,
			"description" => NULL,
			"title" => __("Party settings", TMP__LANG),
		]
	);
	
	foreach ($info as $key => $option) {
		$option["settings"] = tmp__prefix($key);
		
		$wp_customize->add_setting(
			$option["settings"],
			[
				"default" => NULL,
			]
		);
		
		$wp_customize->add_control(
			new WP_Customize_Control(
				$wp_customize,
				$key,
				$option
			)
		);
	}
	
});