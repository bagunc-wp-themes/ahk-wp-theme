<?php
	class AFHPCustomWidgetMostViewedPosts extends WP_Widget {
		
		/**
		 * Widget name
		 */
		const NAME = "AFHPCustomWidgetMostViewedPosts";
		
		/**
		 * Posts loop template
		 *
		 * @var array
		 */
		private $templates = [];
		
		/**
		 * AFHPCustomWidgetMostViewedPosts constructor.
		 */
		public function __construct() {
			parent::__construct(
				$this::NAME,
				__("Most viewed posts", TMP__LANG),
				[
					"description" => __("Showing in sidebar most viewed posts", TMP__LANG),
				],
			);
			
			$this->templates = [
				"thin" => __("Thin", TMP__LANG),
				"thumbnail" => __("Thumbnail", TMP__LANG),
			];
		}
		
		public function field($key, $field) {
			
			ob_start();
			
			switch ($field["type"]) {
				case "select":
					$options = [];
					foreach ($field["options"] as $value => $opt_label) {
						$options[] = sprintf(
							'<option value="%s" %s>%s</option>',
							$value,
							$value === $field["value"] ? "selected" : "",
							$opt_label
						);
					}
					
					printf(
						'<select 	id="%s"
												name="%s"
												class="widefat">%s</select>',
						$this->get_field_id($key),
						$this->get_field_name($key),
						implode("", $options)
					);
					break;
				default:
					printf(
						'<input id="%s"
											name="%s"
											type="%s"
											value="%s"
											class="widefat"
										 />',
						$this->get_field_id($key),
						$this->get_field_name($key),
						$field["type"],
						$field["value"]
					);
					break;
			}
			
			return ob_get_clean();
		}
		
		public function form($instance) {
			$count =  3;
			$template = "thin";
			$title = __("New title", TMP__LANG);
			
			if (isset($instance["title"]))
				$title = $instance["title"];
			
			if (isset($instance["count"]))
				$count = (int)$instance["count"];
			
			if (isset($instance["template"]))
				$template = trim((string)$instance["template"]);
			
			$fields = [
				"title" => [
					"type" => "text",
					"value" => esc_attr($title),
					"label" => __("Title", TMP__LANG),
				],
				"count" => [
					"type" => "number",
					"value" => $count,
					"label" => __("Count", TMP__LANG),
				],
				"template" => [
					"type" => "select",
					"value" => $template,
					"options" => $this->templates,
					"label" => __("Template", TMP__LANG),
				]
			];
			
			foreach ($fields as $key => $field) {
				printf(
					'<p>
						<label for="%s">%s</label>
						%s
					</p>',
					$this->get_field_id($key),
					$field["label"],
					$this->field($key, $field)
				);
			}
		}
		
		public function update($new_instance, $old_instance) {
			$instance = [];
			$instance["count"] = isset($new_instance["count"]) ? (int)$new_instance["count"] : 3;
			$instance["title"] = (!empty($new_instance["title"])) ? strip_tags($new_instance["title"]) : "";
			$instance["template"] = (!empty($new_instance["template"])) ? strip_tags($new_instance["template"]) : "thin";
			
			return $instance;
		}
		
		public function widget($args, $instance) {
			$title = apply_filters("widget_title", $instance["title"]);
			
			print $args["before_widget"];
			
			if (!empty($title))
				print $args["before_title"] . $title . $args["after_title"];
			
			$this->output($instance);
			
			print $args["after_widget"];
		}
		
		public function output($instance) {
			
			if (empty($instance["count"])) return;
			
			$template = "thin";
			if (!empty($instance["template"]))
				$template = $instance["template"];
			
			$query = new WP_Query([
				"post_type" => "post",
				"post_status" => "publish",
				
				"lazyloop" => FALSE,
				"orderby" => "meta_value_num",
				"meta_key" => tmp__prefix("views"),
				
				"posts_per_page" => $instance["count"],
			]);
			
			while ($query->have_posts()) : $query->the_post();
				AFHPComponent::import("post", (object)[
					"classnames" => "mb-2",
					"template" => $template,
				])->render();
			endwhile;
			$query->reset_postdata();
		}
		
	}