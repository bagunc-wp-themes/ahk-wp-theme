<?php
class AFHPCustomWidgets {
	
	public static function register() {
		
		$files = scan__dir('includes/widgets', function($filename) {
			$file = pathinfo(tmp__path("includes/widgets/$filename"));
			
			return !in_array($file["filename"], [
				"index",
				"widgets"
			]);
		});
		
		if (!empty($files)) {
			foreach ($files as $file) {
				require__once("includes/widgets/$file");
				
				$classname  = ucwords(str_replace("-", " ", $file));
				$classname = str_replace(" ", "", $classname);
				$classname = str_replace(".php", "", $classname);
				$classname = "AFHPCustomWidget{$classname}";
				register_widget($classname);
			}
		}
	}
}