/* global jQuery, admin__localize */

'use strict'

jQuery($ => {

  const ajaxReq = props => {

    let { type, dataType, url, success, error, data } = props;

    if (!data) data = {}
    if (!type) type = 'POST'
    if (!dataType) dataType = 'JSON'
    if (!url) url = admin__localize.ajax__url
    if (typeof success !== "function") success = response => console.log(response)
    if (typeof error !== "function") error = () => alert(admin__localize.text.ajax__error)

    return $.ajax({
      url,
      data,
      type,
      error,
      success,
      dataType,
    })
  }

  const updateTermMeta = (id, key, value, success, error) => {

    return ajaxReq({
      data: {
        action: `${admin__localize.tmp__prefix}update__term__meta`,
        meta: JSON.stringify({ id, key, value }),
        success,
        error
      }
    })
  }

  const wpColorPickerConfig = {
    change: (event, ui) => {
      const $this = $(event.target)

      if ($this.hasClass('field--ajax'))
        updateTermMeta(
          parseInt($this.data('term_id')),
          'color',
          ui.color.toString()
        )
    }
  }

  $(document).ajaxSuccess((event, request, settings) => {
    $('.field--color:not(.wp-color-picker)').wpColorPicker(wpColorPickerConfig)
  })

  $(document).ready(() => {
    $('.field--color').wpColorPicker(wpColorPickerConfig)
  })

  $(document).on('click', `.${admin__localize.tmp__prefix}field--file`, event => {
    event.preventDefault()

    const $this = $(event.currentTarget)

    const media = wp.media({
      multiple: false,
    }).on('select', () => {

      $this.find('img').remove()

      const attachment = media.state().get('selection').first().toJSON()
      const { url, id } = attachment
      $this.append(`<img src="${url}" />`)
      $this.find('input').val(id)

      if ($this.hasClass('field--file__ajax'))
        if ($this.data('term_id'))
          updateTermMeta(
            parseInt($this.data('term_id')),
            'thumbnail',
            id
          )

    }).open()
  })

  $(document).on("click", `.${admin__localize.tmp__prefix}choose__file`, event => {

    const $this = $(event.currentTarget)

    const $field = $($this.data("field"))

    const media = wp.media({
      multiple: false,
    }).on("select", () => {

      const attachment = media.state().get('selection').first().toJSON()
      const { url  } = attachment

      $field.val(url)

    }).open()

  });

  $(document).on("click", "#afhp_resource__add", event => {

    const $this = $(event.currentTarget);
    const $root = $($this.parent())

    const $template = $root.find("template");

    $root.append($template[0].content.cloneNode(true))
  });

  $(document).on('click', '.afhp_resource .label span button', event =>
    $(event.currentTarget).parents('.afhp_resource').remove())

  $(document).on('click', `.${admin__localize.tmp__prefix}field--file .remove`, event => {
    event.stopPropagation()
    event.preventDefault()

    const $this = $(event.currentTarget).parent()

    $this.find('img').remove()
    $this.find('input').val(0)

    if ($this.hasClass('field--file__ajax'))
      if ($this.data('term_id'))
        updateTermMeta(
          parseInt($this.data('term_id')),
          'thumbnail',
          0
        )
  })

  $(document).on('change', '.field--checkbox.field--ajax', event => {
    const $this = $(event.currentTarget)
    const meta_key = $this.data('meta_key') || 'show'

    updateTermMeta(
      parseInt($this.val()),
      meta_key,
      $this.prop('checked') ? 1 : 0
    )
  })

})