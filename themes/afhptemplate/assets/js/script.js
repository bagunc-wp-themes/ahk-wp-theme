!function r(e, n, t) {
    function o(i, f) {
        if (!n[i]) {
            if (!e[i]) {
                var c = "function" == typeof require && require;
                if (!f && c) return c(i, !0);
                if (u) return u(i, !0);
                var a = new Error("Cannot find module '" + i + "'");
                throw a.code = "MODULE_NOT_FOUND", a;
            }
            var p = n[i] = {
                exports: {}
            };
            e[i][0].call(p.exports, function(r) {
                return o(e[i][1][r] || r);
            }, p, p.exports, r, e, n, t);
        }
        return n[i].exports;
    }
    for (var u = "function" == typeof require && require, i = 0; i < t.length; i++) o(t[i]);
    return o;
}({
    1: [ function(require, module, exports) {
        "use strict";
        window.lazyLoad = function() {
            var lazyImageObserver, lazyImages = [].slice.call(document.querySelectorAll("img.lazy"));
            "IntersectionObserver" in window && (lazyImageObserver = new IntersectionObserver(function(entries, observer) {
                entries.forEach(function(entry) {
                    var lazyImage;
                    entry.isIntersecting && ((lazyImage = entry.target).src = lazyImage.dataset.src, 
                    lazyImage.srcset = lazyImage.dataset.srcset, lazyImage.classList.remove("lazy"), 
                    lazyImage.classList.add("lazy--loaded"), lazyImageObserver.unobserve(lazyImage));
                });
            }), lazyImages.forEach(function(lazyImage) {
                lazyImageObserver.observe(lazyImage);
            }));
        }, document.addEventListener("DOMContentLoaded", window.lazyLoad);
        jQuery(function($) {
            $('#main-nav a[href="#"]').click(function(e) {
                return e.preventDefault();
            }), $("#main-nav a .svg--next").click(function(e) {
                e.preventDefault(), $(e.target).parents(".menu-item").toggleClass("menu-item--open");
            }), $(document).on("click", ".button--more", function(e) {
                return $(e.currentTarget).addClass("button--loading");
            }), $(document).ready(function() {
                window.lazyLoad(), document.querySelectorAll(".ps").forEach(function(node) {
                    return new PerfectScrollbar(node);
                });
                var structures = $(".structure:not(.structure--ready)"), postThumbnails = $(".post.post--thumbnail:not(.post--ready)");
                $.each(structures, function(index) {
                    var $this = $(structures.get(index)), $row = $this.find(".row--item");
                    $this.css("height", $this.height()), $row.css("max-height", $row.height()), $this.addClass("structure--ready");
                }), $.each(postThumbnails, function(index) {
                    var $this = $(postThumbnails.get(index)), $hide = $this.find(".post__hidden");
                    $hide.length && $hide.css("height", $hide.height()), $this.addClass("post--ready");
                }), $(".carousel--slick").slick && $(".carousel--slick").slick({
                    arrows: !0,
                    autoplay: !0,
                    infinite: !0,
                    autoplaySpeed: 5e3,
                    prevArrow: '<button type="button" class="carousel__arrow carousel__arrow--prev d-none d-sm-inline"><svg><use xlink:href="#next"></use></svg></button>',
                    nextArrow: '<button type="button" class="carousel__arrow carousel__arrow--next d-none d-sm-inline"><svg><use xlink:href="#next"></use></svg></button>'
                }), $(document).on("click", "#menu-toggle", function(e) {
                    e.preventDefault(), $("body").addClass("modal-open"), $(".menu").addClass("open");
                }), $(document).on("click", ".menu .overlay", function() {
                    $("body").removeClass("modal-open"), $(".menu").removeClass("open");
                });
            });
        });
    }, {} ]
}, {}, [ 1 ]);
//# sourceMappingURL=script.js.map
