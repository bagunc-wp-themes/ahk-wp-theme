<?php
class AFHPComponentLazyimage extends AFHPComponent {
	
	const NAME = "lazyimage";
	
	private function alt() {
		
		$alt = $this->data("alt", FALSE);
		
		if (empty($alt))
			return "";
		
		return "alt=\"$alt\"";
	}
	
	private function title() {
		
		$title = $this->data("title", FALSE);
		
		if (empty($title))
			return "";
		
		return "title=\"$title\"";
	}
	
	public function template() {
		
		$this->addClass("lazy");
		
		printf(
			'<img src="%s"
						class="%s"
						data-src="%s"
						data-srcset="%s"
						%s
						%s
					/>',
			placeholder_url(),
			$this->classnames(),
			$this->thumbnail(),
			$this->thumbnail(),
			$this->alt(),
			$this->title(),
		);
	}
	
	public function detect() {
		
		return [
			"thumbnail" => placeholder_url()
		];
	}
	
}