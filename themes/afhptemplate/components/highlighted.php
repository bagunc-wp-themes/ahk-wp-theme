<?php
class AFHPComponentHighlighted extends AFHPComponent {
	
	public function template () {
		
		printf(
			'<div class="highlighted" style="background-color: %s">
        <div class="overlay overlay--highlighted"></div>
        <a href="%s">%s</a>
      </div>',
			$this->data("color", "inherit"),
			$this->data("url", "#"),
			$this->data("name", "")
		);
		
	}
	
	public function detect () {
		
		return [
			"url" => NULL,
			"name" => NULL,
			"color" => NULL,
		];
	}
	
}