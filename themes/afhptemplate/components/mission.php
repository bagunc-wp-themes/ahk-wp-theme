<?php
class AFHPComponentMission extends AFHPComponent {

	public function template() {

		printf(
			'<div class="mission d-flex flex-column h-100">
        <div class="mission__thumbnail">
        	<img 	alt="%s"
        				src="%s"
        				class="lazy"
        				data-src="%s"
        				data-srcset="%s"
        			/>
        </div>
        <div class="mission__content">
          <div class="mission__index">%d</div>
          <h3 class="mission__title">%s</h3>
        </div>
      </div>',
			$this->data('title'),
			placeholder_url(),
			$this->data('thumbnail'),
			$this->data('thumbnail'),
			$this->data('index'),
			$this->data('title')
		);
	}
	
	public function detect() {
		
		return [
			"title" => get_the_title(),
			"thumbnail" => get_the_post_thumbnail_url(),
			"index" => get_post_meta(get_the_ID(), "index", TRUE),
		];
	}
	
}