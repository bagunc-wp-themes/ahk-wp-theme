<?php
class AFHPComponentAlbum extends AFHPComponent {
	
	public function template () {
		
		$this->data->url = get_term_link(
			$this->data('term_id'),
			$this->data('taxonomy')
		);
		
		printf(
			'<div class="gitem gitem--category">
				<a class="gitem__thumbnail" href="%s">
	        %s
	        <div class="overlay overlay--dark gitem__overlay">
	          <svg class="svg gitem__zoom svg--zoom">
	            <use xlink:href="#zoom"></use>
	          </svg>
	        </div>
        </a>
	      <div class="gitem__content">
	        <h4 class="gitem__name">
	        	<a href="%s">%s</a>
          </h4>
	        <div class="gitem__excerpt">%s</div>
	        <date class="gitem__date">%s</date>
	      </div>
	    </div>',
			$this->data('url', '#'),
			$this->thumbnails(),
			$this->data('url', '#'),
			$this->data('name', ''),
			$this->data('description', ''),
			$this->data('date', '')
		);
		
	}
	
	private function thumbnails() {
		
		$this->data->thumbnails = get_posts([
			"lazyloop" => FALSE,
			"posts_per_page" => 6,
			"posts_status" => "publish",
			"post_type" => get_post_type(),
			
			"tax_query" => [
				[
					"field" => "term_id",
					"terms" => $this->data("term_id"),
					"taxonomy" => $this->data("taxonomy"),
				]
			],
		]);
		
		$this->data->thumbnails = array_map(function($item) {
			
			return get_the_post_thumbnail_url($item->ID);
		}, $this->data->thumbnails);
		
		$output = "";
		
		foreach ($this->data("thumbnails", []) as $datum)
			$output .= $this->image($datum);
   
		return sprintf(
			'<div class="thumbnail__group">%s</div>',
			$output
		);
		
	}
	
	private function image($thumbnail) {
		
		$classnames = ["group__item"];
		if (empty($thumbnail))
			$classnames[] = "image--placeholder";
		
		$thumbnail = !empty($thumbnail) ? $thumbnail : placeholder_url();
		
		return sprintf(
			'<div class="%s">
				<img 	src="%s"
							class="lazy"
				 			data-src="%s"
				 			data-srcset="%s"
			      />
      </div>',
			implode(" ", $classnames),
			placeholder_url(),
			$thumbnail,
			$thumbnail
		);
		
	}
	
	public function detect () {
		
		return [
			"url" => NULL,
			"name" => NULL,
			"date" => NULL,
			"thumbnails" => [],
			"description" => NULL,
		];
	}
	
}