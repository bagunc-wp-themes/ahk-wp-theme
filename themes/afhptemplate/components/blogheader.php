<?php
class AFHPComponentBlogheader extends AFHPComponent {
	
	public function template() {
		
		printf(
			'<header class="header--blog">
        <div class="header__top">
          <div class="container">
            <div class="row">
              <div class="col-auto col--logo">
                <a class="header__logo" href="%s">%s</a>
              </div>
              <div class="col-auto col--search">%s</div>
            </div>
          </div>
        </div>
        %s
      </header>',
			$this->data("url", "#"),
			$this->data("title", __("Blog", TMP__LANG)),
			$this->search(),
			$this->bottom()
		);
		
	}
	
	private function bottom() {
		
		$weather = $this->weather();
		$exchange = $this->exchange();
		
		if (empty($weather) && empty($exchange)) return NULL;
		
		return sprintf(
			'<div class="header__bottom">
        <div class="container">
          <div class="row justify-content-between align-items-center">
            <div class="col-auto">%s</div>
            <div class="col-auto">%s</div>
          </div>
        </div>
      </div>',
			$weather,
			$exchange
		);
	
	}
	
	private function weather() {
		
		if (!($weather = $this->data("weather", NULL))) return NULL;
		
		return sprintf(
			'<div class="header__weather d-flex align-items-center">
        %s
        <div class="weather__item weather__item--hot">%d°C</div>
        <div class="weather__item weather__item--cold">%d°C</div>
      </div>',
			$this->data("icon", "", $weather),
			$this->data("day", "?", $weather),
			$this->data("evening", "?", $weather)
		);
		
	}
	
	private function exchange() {
		$exchange = $this->data("exchange", FALSE);
		if (!is_array($exchange)) return NULL;
		
		$output = [];
		foreach ($exchange as $item)
			$output[] = sprintf(
				'<div class="exchange__item d-flex">
	        <div class="item__currency">%s:</div>
	        <div class="item__value">%.3f</div>
	      </div>',
				$item->ISO,
				$item->Rate
			);
		
		return sprintf(
			'<div class="header__exchange d-flex">%s</div>',
			implode("", $output)
		);
		
	}
	
	private function search() {
		
		return get_search_form([
			"echo" => FALSE
		]);
	}
	
	public function detect() {
		
		return [
			"url" => "#",
			"weather" => NULL,
			"exchange" => NULL,
			"title" => __("Blog", TMP__LANG),
		];
	}
	
	
}