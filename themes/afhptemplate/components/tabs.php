<?php
class AFHPComponentTabs extends AFHPComponent {
	
	/**
	 * Tab item
	 *
	 * @param stdClass $item
	 *
	 * @return string
	 */
	private function item($item) {
		
		$classnames = ["nav-link"];
		if (isset($item->active) && $item->active)
			$classnames[] = "active";
		
		return sprintf(
			'<li class="nav-item">
				<a class="%s" href="%s">%s</a>
			</li>',
			implode(" ", $classnames),
			$item->link,
			$item->text
		);
	}
	
	/**
	 * Tab items
	 *
	 * @return false|string
	 */
	private function items() {
		
		ob_start();
		
		if (!empty($this->data))
			foreach ($this->data as $datum)
				print $this->item($datum);
		
		return ob_get_clean();
		
	}
	
	/**
	 * Component template
	 */
	public function template() {
		
		$this->addClass("nav");
		
		printf(
			'<ul class="%s">%s</ul>',
			$this->classnames(),
			$this->items()
		);
		
	}
	
	/**
	 * Detecting component data
	 *
	 * @return array
	 */
	public function detect() {
	
		return [];
	}
	
}