<?php
class AFHPComponentTeam extends AFHPComponent {
	
	/**
	 * @var WP_Query $this->result = [ name, thumbnail ]
	 */
	
	/**
	 * Component template
	 *
	 * @return void|null
	 */
	public function template() {
		
		$this->addClass("person");
		
		printf(
			'<div class="%s">
        <div class="person__thumbnail">
        	<img 	class="lazy"
			          src="%s"
			          data-src="%s"
			          data-srcset="%s"
		          />
        </div>
        <div class="person__name">
          <h4 class="person__title">%s</h4>
        </div>
      </div>',
			$this->classnames(),
			placeholder_url(),
			$this->thumbnail(),
			$this->thumbnail(),
			$this->data("name", "")
		);
	
	}
	
	public function detect() {
		
		return [
			"name" => get_the_title(),
			"thumbnail" => get_the_post_thumbnail_url()
		];
	}
	
}