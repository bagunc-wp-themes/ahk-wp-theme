<?php
class AFHPComponentGallery extends AFHPComponent {
	
	/**
	 * @var string
	 *
	 * available values ['preview', 'photo', 'video']
	 * default => preview
	 * unknown type => preview
	 *
	 */
	public $template;
	
	public function template() {
		
		$this->template = $this->config('template', 'default');

		require__once("components/gallery/{$this->template}.php");

		$component = 'AFHPComponentGallery' . ucfirst($this->template);
		
		/**
		 * @var AFHPComponentGallery $component
		 */
		$component = new $component($this->config);
		
		if ($component->config("ajax", TRUE))
			$component->addClass("gallery__item--ajax");
		
		print $component->render($this->data);
	}
	
	public function detect() {
		
		$ID = get_the_ID();
		
		$youtube = get_post_meta($ID, "youtube__url", TRUE);
		$duration = get_post_meta($ID, "youtube__duration", TRUE);

		return [
			"ID" => $ID,
			"youtube" => $youtube,
			"duration" => $duration,
			"date" => get_the_date(),
			"title" => get_the_title(),
			"url" => get_the_permalink(),
			"excerpt" => get_the_excerpt(),
			"thumbnail" => get_the_post_thumbnail_url(),
		];
	}
}