<?php
class AFHPComponentGalleryPhoto extends AFHPComponentGallery {

	/**
	 * @var string
	 *
	 * available styles [preview, default]
	 */
	public $style = "default";
	
	/**
	 * Big image size
	 *
	 * @var int
	 */
	public $bigSize;
	
	/**
	 * Gallery photo preview template
	 *
	 * @var array $this->data => [
	 *    url
	 *    date
	 *    title
	 *    excerpt
	 *    thumbnail
	 * ]
	 */
	public function template__preview() {
		
		$this->addClass("gpreview");
		
		printf(
			'<a class="%s" href="%s">
        <div class="gpreview__thumbnail h-100">
          <img 	src="%s"
                alt="%s"
          			class="lazy" 
                data-src="%s"
                data-srcset="%s"
              />
        </div>
        <div class="overlay overlay--dark gpreview__overlay">
          <div class="gpreview__excerpt">
          %s
          %s
          </div>
        </div>
      </a>',
			$this->classnames(),
			$this->data('url', "#"),
			placeholder_url(),
			$this->data('title', ""),
			$this->thumbnail(),
			$this->thumbnail(),
			$this->preview__title(),
			$this->data('excerpt', "")
		);

	}
	
	/**
	 * Gallery preview item title if no excerpt
	 *
	 * @return string
	 */
	public function preview__title() {
		
		$excerpt = trim((string)$this->data("excerpt"), "");
		
		if (empty($excerpt))
			return sprintf(
				'<h4 class="gpreview__title h5 text-center">%s</h4>',
				$this->data("title", "")
			);
		
		return "";
	}

	/**
	 * Gallery photo default template
	 *
	 * @var array $this->data => [
	 *    url
	 *    date
	 *    title
	 *    thumbnail
	 * ]
	 */
	public function template__default() {
	
		$this->addClass(["gitem", "gitem--photo"]);
		
		printf(
			'<a class="%s" href="%s">
				<img 	alt="%s"
							src="%s"
							data-src="%s"
							data-srcset="%s"
							class="gitem__thumbnail lazy"
					  />
	      <div class="overlay overlay--gradients gitem__overlay">
	        <svg class="svg gitem__zoom svg--zoom">
	          <use xlink:href="#zoom"></use>
	        </svg>
	        <div class="gitem__meta">
	          <h4 class="gitem__name">%s</h4>
	          <date class="gitem__date">%s</date>
	        </div>
	      </div>
			</a>',
			$this->classnames(),
			$this->data('url', '#'),
			$this->data('title', ''),
			placeholder_url(),
			$this->thumbnail(),
			$this->thumbnail(),
			$this->data('title', ''),
			$this->data('date', '')
		);

	}

	/**
	 * Component template
	 *
	 * available modes [big, vertical, horizontal]
	 */
	public function template() {
		
		$large_size_w = (float)get_option("large_size_w");
		$large_size_h = (float)get_option("large_size_h");
		
		$large_size = (object)[
			"width" => $large_size_w ? $large_size_w : 1200,
			"height" => $large_size_h ? $large_size_h : 1200
		];
		
		
		$this->bigSize = $this->config('bigSize', $large_size);
		$this->style = $this->config('style', 'default');

		$method = "template__{$this->style}";
		
		if ($this->config("dens", TRUE) && $this->data("thumbnail", FALSE)) {
			$attachment = getimagesize($this->data("thumbnail"));
			
			$width = $attachment[0];
			$height = $attachment[1];
			
			$is_vertical = ($height / 2 > $width);
			$is_horizontal = ($width / 2 > $height);
			$is_big = ($this->bigSize && ($height >= $this->bigSize->height) && ($width >= $this->bigSize->width));
			
			if ($is_big)
				$this->addClass("gallery__item--big");
			elseif ($is_horizontal)
				$this->addClass("gallery__item--horizontal");
			elseif ($is_vertical)
				$this->addClass("gallery__item--vertical");
			
		}
		
		if (method_exists($this, $method))
			return $this->$method();

		return $this->template__default();
		
	}

}