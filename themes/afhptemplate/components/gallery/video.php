<?php

class AFHPComponentGalleryVideo extends AFHPComponentGallery {

	/**
	 * @var string
	 *
	 * available styles [preview, default]
	 */
	public $style = "default";

	/**
	 * Gallery video preview template
	 *
	 * @var array $this->data => [
	 *    url
	 *    thumbnail
	 * ]
	 */
	public function template__preview() {
	
		$this->addClass(["gpreview", "gpreview--video"]);
		
		printf(
			'<a href="%s"
					class="%s"
					data-v="%s">
        <div class="gpreview__thumbnail h-100">
        	<img 	src="%s"
        				class="lazy" 
        				data-src="%s"
        				data-srcset="%s"
        			/>
        </div>
        <div class="overlay gpreview__overlay">
          <svg class="svg gpreview__play svg--play">
            <use xlink:href="#play"></use>
          </svg>
        </div>
      </a>',
			$this->data('url', '#'),
			$this->classnames(),
			$this->data('youtube', '#'),
			placeholder_url(),
			$this->thumbnail(),
			$this->thumbnail()
		);

	}

	/**
	 * Gallery video preview template
	 *
	 * @var array $this->data => [
	 *    url
	 *    date
	 *    delay
	 *    title
	 *    excerpt
	 *    thumbnail
	 * ]
	 */
	public function template__default() {
		
		$this->addClass([
			"gitem",
			"gitem--video",
			"d-flex",
			"flex-column",
		]);
		
		printf(
			'<div class="%s">
				<a 	href="%s"
						data-v="%s"
						class="gitem__thumbnail flex-grow-1">
					<img 	src="%s"
								alt="%s"
								class="lazy"
								data-src="%s"
								data-srcset="%s"
							/>
					%s
	        <div class="overlay overlay--dark gitem__overlay">
	          <svg class="svg svg--play">
	            <use xlink:href="#play"></use>
	          </svg>
	        </div>
        </a>
	      <div class="gitem__content flex-shrink-1">
	        <h4 class="gitem__name">
	        	<a href="%s">%s</a>
          </h4>
	        <div class="gitem__excerpt">%s</div>
	        <date class="gitem__date">%s</date>
	      </div> 
    	</div>',
			$this->classnames(),
			$this->data('url', '#'),
			$this->data('youtube', '#'),
			placeholder_url(),
			$this->data('title', ''),
			$this->thumbnail(),
			$this->thumbnail(),
			$this->duration(),
			$this->data('url', '#'),
			$this->data('title', ''),
			$this->data('excerpt', ''),
			$this->data('date', '')
		);

	}
	
	public function duration() {
		
		$data = $this->data('duration', FALSE);
		if (is_object($data))
			if (!empty($data->minutes) || !empty($data->seconds))
				return sprintf(
					'<span class="gitem__duration">%02d:%02d</span>',
					$data->minutes,
					$data->seconds,
				);
		
		return "";
	}

	public function template() {
		
		$this->style = $this->config('style', 'default');

		$method = "template__{$this->style}";

		if (method_exists($this, $method))
			return $this->$method();

		return $this->template__default();
	}
	
}