<?php
class AFHPComponentShear extends AFHPComponent {
	
	public $services = ["facebook", "twitter", "instagram"];
	
	public function services() {
		
		ob_start();
		
		foreach ($this->services as $service) {
			$config = (object)$this->getService($service);
			
			printf(
				'<a href="%s">
          <img 	alt="%s"
          			src="%s"
          			class="social__icon"
              />
        </a>',
				$config->url,
				$config->name,
				$config->logo
			);
		}
			
		return ob_get_clean();
	}
	
	public function template() {
		
		$this->addClass("shear");
		
		printf(
			'<div class="%s">
        %s
      </div>',
			$this->classnames(),
			$this->services()
		);
		
	}
	
	private function getService($name) {
		
		$config = [
			"facebook" => [
				"url" => "#",
				"name" => "Facebook",
				"logo" => tmp__assets('img/footer/facebook.png'),
			],
			"twitter" => [
				"url" => "#",
				"name" => "Twitter",
				"logo" => tmp__assets('img/footer/twitter.png'),
			],
			"instagram" => [
				"url" => "#",
				"name" => "Instagram",
				"logo" => tmp__assets('img/footer/instagram.png'),
			]
		];
		
		return isset($config[$name]) ? $config[$name] : (object)[];
	}
	
	public function detect() {
		
		return [
			"services" => "*"
		];
	}
	
}