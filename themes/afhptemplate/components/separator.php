<?php
class AFHPComponentSeparator extends AFHPComponent {
	
	public function template() {
		
		$this->addClass("separator");
		
		printf(
			'<div class="%s">
				<div class="separator__line" style="background-color: %s"></div>
				<div class="separator__circle" style="background-color: %s"></div>
				<div class="separator__line" style="background-color: %s"></div>
			</div>',
			$this->classnames(),
			$this->data("color", "inherit"),
			$this->data("color", "inherit"),
			$this->data("color", "inherit")
		);
		
	}
	
	public function detect() {
		
		return [
			"color" => "#eee"
		];
	}
	
}