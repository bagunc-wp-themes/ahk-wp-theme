<?php
class AFHPComponentBanner extends AFHPComponent {
	
	public function breadcrumbs() {

		$output = "";
		foreach ($this->data('breadcrumbs', []) as $datum)
			if (isset($datum->link))
				$output .= sprintf(
					'<li>%s</li>',
					$this->text($datum->link)
				);
			else
				$output .= sprintf(
					'<li title="%s"><a href="%s">%s</a></li>',
					$datum->text,
					$datum->url,
					$this->text($datum->text)
				);

		return sprintf(
			'<nav class="banner__breadcrumbs" role="breadcrumbs">
        <ul>%s</ul>
      </nav>',
			$output
		);
	}
	
	private function heading() {
		
		if ($this->config("style") === "thin")
			return "";
		
		return sprintf(
			'<h1 class="banner__heading">%s</h1>',
			$this->data('title', '')
		);
	}

	public function template() {
		
		$this->addClass([
			"banner",
			"overflow-hidden",
			"banner--" . $this->config("style", "default")
		]);
		
		printf(
			'<div class="%s">
				%s
        <div class="overlay overlay--gradient"></div>
        <div class="banner__content">
          <div class="content__wrapper">
          	%s
						%s
          </div>
        </div>
      </div>',
			$this->classnames(),
			AFHPComponent::import("lazyimage", (object)[
				"output" => "return",
				"classnames" => "banner__thumbnail",
			])->render([
				"alt" => $this->data('title', ''),
				"thumbnail" => $this->data("thumbnail", NULL)
			]),
			$this->heading(),
			$this->breadcrumbs()
		);

	}
	
	private function text($text) {
		
		$length = mb_strlen($text, "UTF-8");
		
		if ($length > 15)
			return mb_substr($text, 0, 15, "UTF-8") . "...";
		
		return $text;
	}
	
	public function detect() {
		
		return [
			"breadcrumbs" => [],
			"title" => get_the_title(),
			"thumbnail" => get_the_post_thumbnail_url(),
		];
	}
	
}