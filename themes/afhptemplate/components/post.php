<?php

class AFHPComponentPost extends AFHPComponent {

	/**
	 * @var string $config
	 *
	 * available values [default, thin, thumbnail, slider]
	 * default => default
	 * unknown type => default
	 *
	 */
	public $template = "default";

	private function switch() {

		$this->template = $this->config('template', 'default');

		require__once("components/post/{$this->template}.php");

		$component = 'AFHPComponentPost' . ucfirst($this->template);

		if (!class_exists($component))
			return print system__message(
				"Error [404] <b>{$this->template}</b> post style is not found",
				"danger"
			);

		$component = new $component($this->config);
		
		$component->addClass($this->classnames);
		$component->addClass("post--{$component->template}");
		
		
		$component->data = $this->data;
		
		return print $component->template();
	}
	
	protected function label() {
		
		if (!$this->config("label", TRUE) || !$this->data('term'))
			return "";
		
		$term = $this->data('term', (object)[]);
		$name = $this->data('name', '', $term);
		$color = $this->data('color', 'inherit', $term);
	
		return sprintf(
			'<label class="post__category" style="background-color: %s">%s</label>',
			$color,
			$name
		);
	}

	public function template() {
		$this->addClass("post");
		
		$this->switch();
	}
	
	public function detect() {
		
		$term = NULL;
		$taxonomy = NULL;
		$queried = get_queried_object();
		
		if ($queried instanceof WP_Term) {
			$taxonomy = $queried->taxonomy;
		} else {
			$taxonomies = get_object_taxonomies(get_post_type());
			if (!empty($taxonomies))
				$taxonomy = $taxonomies[0];
		}
		
		$list = get_the_terms(get_the_ID(), $taxonomy);
		if (!empty($list))
			$term = (object)[
				"name" => $list[0]->name,
				"color" => get_term_meta($list[0]->term_id, 'color', TRUE),
			];
		
		return [
			"term" => $term,
			"ID" => get_the_ID(),
			"date" => get_the_date(),
			"title" => get_the_title(),
			"url" => get_the_permalink(),
			"excerpt" => get_the_excerpt(),
			"thumbnail" => get_the_post_thumbnail_url(NULL, $this->config("thumbnail_size", "full")),
		];
		
	}
	
}