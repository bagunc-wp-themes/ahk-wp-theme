<?php
class AFHPComponentStructure extends AFHPComponent {

	/**
	 * Component styles
	 * Available values [default, alt]
	 *
	 * @var string
	 */
	public $style = "default";

	private function childes() {

		$output = "";

		foreach ($this->data('childes', []) as $datum)
			$output .= sprintf(
				'<div class="col-sm-6">
					<a href="%s">%s</a>
				</div>',
				$datum->url ? $datum->url : '#',
				$datum->text
			);

		return sprintf('<div class="row row--items align-items-lg-center">%s</div>', $output);
	}

	public function template() {

		$this->style = $this->config("style", "default");

		$this->addClass(["structure", "d-flex"]);
		$thumbClassnames = ["structure__thumbnail", "h-100"];

		if ($this->style === "alt") {
			$thumbClassnames[] = "order-2";
			$this->addClass("structure--alt");
		}

		printf(
			'<div class="%s">
        <div class="%s">
        	<img 	src="%s"
        				alt="%s"
        				class="lazy"
        				data-src="%s"
        				data-srcset="%s"
              />
        </div>
        <div class="structure__content">
          <div class="d-flex h-100 justify-content-center flex-column">
            <div class="structure__heading">
              <h3 class="structure__title">
              	<a href="%s">%s</a>
							</h3>
            </div>
            %s
          </div>
        </div>
      </div>',
			$this->classnames(),
			implode(" ", $thumbClassnames),
			placeholder_url(),
			$this->data('title', ''),
			$this->thumbnail(),
			$this->thumbnail(),
			$this->data('url', '#'),
			$this->data('title', ''),
			$this->childes()
		);

	}

	public function detect() {

		return [
			"url" => "#",
			"title" => NULL,
			"childes" => [],
			"thumbnail" => NULL,
		];
	}

}
