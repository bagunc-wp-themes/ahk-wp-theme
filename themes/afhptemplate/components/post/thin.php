<?php
class AFHPComponentPostThin extends AFHPComponentPost {

	/**
	 * @var array $this->data => [
	 *  url
	 *  date
	 *  term
	 *  title
	 *  thumbnail
	 * ]
	 */

	/**
	 * Component template
	 *
	 * @var string
	 */
	public $template = "thin";

	public function template() {

		$this->addClass([
			"d-flex",
			"post--thin",
		]);
		
		printf(
			'<div class="%s">
				<a class="post__thumbnail" href="%s">
					%s
					<img 	src="%s"
								alt="%s"
								class="lazy"
								data-src="%s"
								data-srcset="%s"
							/>
				</a>
        <div class="post__content">
          <date class="post__date">%s</date>
          <h4 class="post__title">
          	<a href="%s">%s</a>
          </h4>
        </div>
      </div>',
			$this->classnames(),
			$this->data('url', '#'),
			$this->data('term') ? sprintf(
				'<label class="post__group d-lg-none" style="background-color: %s"></label>',
				$this->data('color', '#ddd', $this->data('term'))
			) : "",
			placeholder_url(),
			$this->data('title', ''),
			$this->thumbnail(),
			$this->thumbnail(),
			$this->data('date', ''),
			$this->data('url', '#'),
			$this->data('title', '')
		);

	}

}