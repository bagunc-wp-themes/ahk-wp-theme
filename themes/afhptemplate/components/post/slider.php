<?php
class AFHPComponentPostSlider extends AFHPComponentPost {

	/**
	 * Component template
	 *
	 * @var string
	 */
	public $template = "slider";

	public function template() {

		$this->addClass("post--slider");

		printf(
			'<a class="%s" href="%s">
				<div class="post__thumbnail">
					<img class="lazy" src="%s" data-src="%s" data-srcset="%s" alt="%s" />
					%s
        </div>
        <div class="overlay overlay--gradient post__overlay"></div>
        <div class="post__content">
          <h3 class="post__title">%s</h3>
          <p class="post__excerpt">%s</p>
          <date class="post__date">%s</date>
        </div>
			</a>',
			$this->classnames(),
			$this->data('url', '#'),
			placeholder_url(),
			$this->thumbnail(),
			$this->thumbnail(),
			$this->data('title', placeholder_url()),
			$this->data('term') ? sprintf(
				'<label class="post__category" style="background-color: %s">%s</label>',
				$this->data('color', 'inherit', $this->data('term')),
				$this->data('name', '', $this->data('term'))
			) : '',
			$this->data('title'),
			$this->data('excerpt'),
			$this->data('date')
		);

	}

}