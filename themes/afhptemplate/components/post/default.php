<?php
class AFHPComponentPostDefault extends AFHPComponentPost {

	/**
	 * @var stdClass $data => [
	 *  url,
	 *  date,
	 *  title,
	 *  excerpt,
	 *  thumbnail,
	 *  term => [
	 *    name,
	 *    color
	 *  ]
	 * ]
	 */

	/**
	 * Component template
	 *
	 * @var string
	 */
	public $template = "default";

	public function template() {

		$thumb__default__cls = ["col--thumbnail"];

		$thumb__cls = $this->config("thumbnail_classnames");
		if (empty($thumb__cls))
			$thumb__cls = ["col-12", "col-sm-4", "col-md-4", "col-lg-12", "col-xl-4"];

		if (is_string($thumb__cls))
			$thumb__cls = explode(" ", $thumb__cls);

		$thumb__cls = array_merge($thumb__cls, $thumb__default__cls);

		printf(
			'<div class="%s">
        <div class="row">
          <div class="%s">
          	<a class="post__thumbnail" href="%s">
              %s
              <img 	class="lazy"
              			src="%s"
              			data-src="%s"
              			data-srcset="%s"
              			alt="%s"
              		/>
            </a>
          </div>
          <div class="col col--content">
            <div class="post__content">
              <h3 class="post__title">
              	<a href="%s">%s</a>
              </h3>
              <div class="post__excerpt">%s</div>
              <date class="post__date">%s</date>
            </div>
          </div>
        </div>
      </div>',
			$this->classnames(),
			implode(" ", $thumb__cls),
			$this->data('url', '#'),
			$this->label(),
			placeholder_url(),
			$this->thumbnail(),
			$this->thumbnail(),
			$this->data('title'),
			$this->data('url'),
			$this->data('title'),
			$this->data('excerpt'),
			$this->data('date')
		);

	}

}
