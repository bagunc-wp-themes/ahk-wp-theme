<?php
	class AFHPComponentPostThumbnail extends AFHPComponentPost {
		
		/**
		 * @var array $this->data => [
		 *  url
		 *  date
		 *  title
		 *  excerpt
		 *  thumbnail
		 * ]
		 */
		
		/**
		 * Component template
		 *
		 * @var string
		 */
		public $template = "thumbnail";
		
		public function template() {
			
			$this->addClass([
				"post--thumbnail",
			]);
			
			printf(
				'<a class="%s" href="%s">
          <div class="post__thumbnail">
          	<img 	alt="%s"
          				src="%s"
          				class="lazy"
          				data-src="%s"
          				data-srcset="%s"
                />
          </div>
          <div class="overlay post__overlay"></div>
          <div class="post__content">
            <h3 class="post__title">%s</h3>
            <div class="post__hidden">
              <div class="post__excerpt">%s</div>
              <date class="post__date">%s</date>
            </div>
          </div>
        </a>',
				$this->classnames(),
				$this->data('url', '#'),
				$this->data('title', ''),
				placeholder_url(),
				$this->thumbnail(),
				$this->thumbnail(),
				$this->data('title', ''),
				$this->data('excerpt', ''),
				$this->data('date', '')
			);
			
		}
		
	}