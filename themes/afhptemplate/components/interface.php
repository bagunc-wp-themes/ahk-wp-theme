<?php
interface AFHPComponentInterface {

	public function template();

	public function render($data = []);
	
	public function detect();

}