<?php
class AFHPComponentHeading extends AFHPComponent {

	public function template() {

		printf(
			/** @lang text */
			'<%s class="section__heading">
				<span>%s</span>
        <div class="heading__underline" %s></div>
      </%s>',
			isset($this->data->tag) ? $this->data->tag : 'h3',
			isset($this->data->text) ? $this->data->text : 'Section heading',
			isset($this->data->color) ? 'style="color: ' . $this->data->color . '"' : '',
			isset($this->data->tag) ? $this->data->tag : 'h3'
		);

	}
	
	public function detect() {
		
		return [
			"tag" => "h3",
			"color" => "#eee",
			"text" => get_the_title(),
		];
	}
	
}