<?php
class AFHPComponentButton extends AFHPComponent {

	/**
	 * Component style
	 * available styles ["default", "more", "icon"]
	 *
	 * @var string
	 */
	public $style = "default";

	public function tag() {

		if ($this->attr('href', FALSE))
			return 'a';

		return 'button';
	}

	private function icon() {

		if ($this->style !== "icon") return "";

		return $this->data("icon", "");
	}

	public function attributes() {

		$attrs = $this->data('attrs', []);

		if (!is_array($attrs))
			$attrs = [];

		if (empty($attrs["class"]))
			$attrs["class"] = [];
		
		if (is_string($attrs["class"]))
			$attrs["class"] = [$attrs["class"]];

		$attrs["class"][] = "button";
		
		$style = $this->config("style", "default");
		$theme = $this->config("theme");
		
		if (is_string($theme))
			$attrs["class"][] = "button--$theme";
		
		switch ($style) {
			case "more":
				$attrs["class"][] = "button--more";
				break;
			case "icon":
				$attrs["class"][] = "button--icon";
				break;
			default:
				break;
		}

		$attrs["class"] = implode(" ", $attrs["class"]);

		$attributes = [];
		foreach ($attrs as $key => $value)
			$attributes[] = "$key=\"$value\"";

		if (!isset($attributes['class']))
			$attributes[] = "class=\"button\"";

		return implode(" ", $attributes);

	}

	public function template__more() {

		return '<span class="button__content">
							<i></i>
							<i></i>
							<i></i>
						</span>';
	}

	public function template__default() {

		return $this->data('text', __('Button', TMP__LANG));
	}
	
	public function overlay() {
		
		$overlay = $this->config("overlay", FALSE);
		
		return $overlay ? '<div class="button__overlay"></div>' : '';
		
	}
	
	public function template() {

		$this->style = $this->config("style", "default");
		$method = "template__{$this->style}";

		if (!method_exists($this, $method))
			$method = "template__default";

		$output = $this->$method();

		printf(
			'<%s %s %s>%s %s %s</%s>',
			$this->tag(),
			$this->attributes(),
			$this->data('color', FALSE) ? "style=\"color: {$this->data('color')}\"" : "",
			$this->icon(),
			$output,
			$this->overlay(),
			$this->tag()
		);

	}

	public function attr($key, $default = NULL) {

		$attrs = (array)$this->data('attrs', []);

		return isset($attrs[$key]) ? $attrs[$key] : $default;

	}
	
	public function detect() {
		
		return [
			"attrs" => [],
			"icon" => NULL,
			"color" => "inherit",
			"text" => __("Button", TMP__LANG),
		];
	}
	
}