<?php
class AFHPComponentDiscussion extends AFHPComponent {
	
	public function template() {
		
		$this->addClass(["vote", "d-flex"]);
		
		printf(
			'<div class="%s">
	      <div class="row">
	        <div class="col-4">
	        	<a class="vote__thumbnail" href="%s">
	        		<img 	src="%s"
	        					alt="%s"
	        					class="lazy"
				            data-src="%s"
				            data-srcset="%s"
                  />
            </a>
          </div>
	        <div class="col">
	          <div class="vote__content d-flex flex-column h-100">
	            <h3 class="vote__title"><a href="%s">%s</a></h3>
	            <div class="vote__excerpt flex-grow-1">%s</div>
	            %s
	          </div>
	        </div>
	      </div>
	    </div>',
			$this->classnames(),
			$this->data("url", "#"),
			placeholder_url(),
			$this->data("title", ""),
			$this->thumbnail(),
			$this->thumbnail(),
			$this->data("url", "#"),
			$this->data("title", ""),
			$this->data("excerpt", ""),
			$this->voting()
		);
		
	}
	
	public function voting() {
		
		$voting = $this->data("voting", NULL);
		
		if (!$voting || $voting->hide || (empty($voting->no) && empty($voting->yes))) return "";
		
		return sprintf(
			'<div class="vote__result">
        <div class="result__text d-flex justify-content-between">
          <div class="text__pros">%s</div>
          <div class="text__cons">%s</div>
        </div>
        <div class="result__bar d-flex flex-row">
          <div class="bar__pros" style="width: %d%%"></div>
          <div class="bar__cons" style="width: %d%%"></div>
        </div>
      </div>',
			__("pros", TMP__LANG),
			__("cons", TMP__LANG),
			$voting->yes,
			$voting->no
		);
	}
	
	public function detect() {
		
		return [
			"title" => get_the_title(),
			"url" => get_the_permalink(),
			"excerpt" => get_the_excerpt(),
			"thumbnail" => get_the_post_thumbnail_url(),
			"voting" => (object)[
				"no" => (int)get_post_meta(get_the_ID(), 'voting__no', TRUE),
				"yes" => (int)get_post_meta(get_the_ID(), 'voting__yes', TRUE),
				"hide" => (int)get_post_meta(get_the_ID(), 'voting__hide', TRUE),
			],
		];
	}
	
}