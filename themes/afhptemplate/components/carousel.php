<?php

class AFHPComponentCarousel extends AFHPComponent {
	
	public function html($data) {
		
		ob_start();
		
		debug($data);
		
		return ob_get_clean();
	}

	public function template() {

		$post = AFHPComponent::import('post', (object)[
			"template" => "slider",

			"output" => "return",
		]);

		$output = "";
		if ($this->data instanceof WP_Query) {
			while ($this->data->have_posts()) : $this->data->the_post();
				$output .= $post->render();
			endwhile;
			$this->data->reset_postdata();
		} else if (is_array($this->data)) {
			foreach ($this->data as $datum)
				$output .= $this->html($datum);
		}

		printf(
			'<div class="carousel carousel--slick">%s</div>',
			$output
		);
	}
	
	public function detect() {
		
		return [];
	}
	
}