<?php
class AFHPComponentAd extends AFHPComponent {

	public function template() {

		printf(
			'<div class="advertisement w-100" style="background-image: %s">
        <div class="advertisement__text">%s</div>
      </div>',
			$this->data('image', 'none'),
			$this->data('image', FALSE) ? '' : sprintf(
				'<div class="advertisement__text">%s</div>',
					$this->data('text', __('YOUR ADVERTISEMENT', TMP__LANG))
			)
		);

	}
	
	public function detect() {
		
		return [
			"link" => NULL,
			"resource" => NULL,
		];
	}
	
}