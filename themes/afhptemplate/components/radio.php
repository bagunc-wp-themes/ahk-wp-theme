<?php
class AFHPComponentRadio extends AFHPComponent {
	
	public function template() {
		
		$name = $this->data("name");
		
		printf(
			'<label class="radio">
        <input 	%s
        				value="%s"
        				type="radio"
              />
        <div class="radio__wrapper">
          <div class="radio__virtual"></div>
          <div class="radio__text">%s</div>
        </div>
      </label>',
			$name ? "name=\"$name\"" : "",
			$this->data("value", ""),
			$this->data("label", $this->data("value", ""))
		);
		
	}
	
	public function detect() {
		
		return [
			"name" => NULL,
			"value" => NULL,
			"label" => NULL,
		];
	}
	
}