<?php

/**
 * Abstract parent class for components
 *
 * Class AFHPComponent
 */
abstract class AFHPComponent implements AFHPComponentInterface {
	
	/**
	 * Component name
	 */
	const NAME = "abstract";
	
	/**
	 * Component config
	 *
	 * @var stdClass
	 */
	public $config;

	/**
	 * Component data
	 *
	 * @var stdClass
	 */
	public $data;
	
	/**
	 * Component wrapper
	 *
	 * @var string
	 */
	private $wrapper;
	
	/**
	 * @var string[]
	 */
	public $classnames = ["component"];

	/**
	 * AFHPComponent constructor.
	 *
	 * @param string[] $config
	 */
	public function __construct($config = ["output" => "print"]) {

		$this->config = (object)$config;
		$this->wrapper = $this->config("wrapper", "%s");
		
		if ($this->config("classnames", NULL))
			$this->addClass($this->config("classnames"));
			
	}

	/**
	 * Getting data from $this->data
	 *
	 * @param string $key
	 * @param null $default
	 * @param stdClass|null $object
	 *
	 * @return mixed|null
	 */
	public function data(string $key, $default = NULL, stdClass $object = NULL) {

		$data = $object ? $object : $this->data;

		return isset($data->$key) ? $data->$key : $default;
	}

	/**
	 * Getting config from $this->config
	 *
	 * @param string $key
	 * @param null $default
	 * @param stdClass|null $object
	 *
	 * @return mixed|null
	 */
	public function config(string $key, $default = NULL, stdClass $object = NULL) {

		$config = $object ? $object : $this->config;

		return isset($config->$key) ? $config->$key : $default;
	}

	/**
	 * Component template
	 *
	 * Data accessing by $this->data
	 * Configs accessing by $this->config
	 *
	 * @returns string|NULL
	 */
	public abstract function template();
	
	/**
	 * Returning component template output in wrapper
	 *
	 * @return false|string
	 */
	private function wrapper() {
		
		if ($this->need("thumbnail") && empty($this->data("thumbnail")))
			$this->addClass("image--placeholder");
		else
			$this->removeClass("image--placeholder");
		
		ob_start();
		$this->template();
		return ob_get_clean();
	}
	
	/**
	 * Rendering component
	 *
	 * @param mixed $data
	 *
	 * @return false|string
	 */
	public function render($data = NULL) {

		$this->data = !empty($data) ? (object)$data : (object)$this->detect();
		
		if ($this->config('output') === 'return')
			ob_start();
		
		printf($this->wrapper, $this->wrapper());
		
		if ($this->config('output') === 'return')
			return ob_get_clean();
		
		return NULL;
	}

	/**
	 * Importing component by name
	 *
	 * @param string $name
	 * @param stdClass|null $config
	 *
	 * @return AFHPComponent|int
	 */
	public static function import(string $name, stdClass $config = NULL) {

		require__once("components/$name.php");

		$component = "AFHPComponent" . ucfirst($name);

		if (class_exists($component))
			$component = new $component($config);

		if ($component instanceof AFHPComponent)
			return $component;

		return print system__message(
			"Error [404] <b>$name</b> component is not found",
			"warning"
		);
	}
	
	/**
	 * Determining data from the response
	 */
	public abstract function detect();
	
	/**
	 * Need component data
	 *
	 * @param $key
	 *
	 * @return bool
	 */
	public function need($key) {
		
		$data = (object)$this->detect();
		return isset($data->$key);
	}
	
	/**
	 * Adding classnames to component
	 *
	 * @param $value
	 *
	 * @return string[]
	 */
	public function addClass($value) {
		
		if (!is_array($value))
			$value = [$value];
		
		$this->classnames = array_merge($value, $this->classnames);
		$this->classnames = array_unique($this->classnames);
		
		return $this->classnames;
	}
	
	public function hasClass($classname) {
		
		if (is_string($classname))
			$classname = [$classname];
		
		foreach ($classname as $item)
			if (!in_array($item, $this->classnames))
				return FALSE;
			
		return TRUE;
	}
	
	public function removeClass($classname) {
		
		if (!$this->hasClass($classname)) return FALSE;
		
		if (is_string($classname))
			$classname = [$classname];
		
		foreach ($classname as $item) {
			$index = array_search($item, $this->classnames);
			
			array_splice($this->classnames, $index, 1);
		}
		
		return TRUE;
	}
	
	/**
	 * Component class names string
	 *
	 * @return string
	 */
	public function classnames() {
		
		return implode(" ", $this->classnames);
	}
	
	/**
	 * Component thumbnail || placeholder image
	 *
	 * @return mixed|string|null
	 */
	public function thumbnail() {
		
		$thumbnail = $this->data("thumbnail", FALSE);
		if (!$thumbnail)
			$thumbnail = placeholder_url();
		
		return $thumbnail;
	}
	
}