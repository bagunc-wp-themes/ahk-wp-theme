<?php
class AFHPComponentSvg extends AFHPComponent {
	
	public function template() {
		
		$name = $this->data("name", "default");
		
		$this->addClass([
			"svg",
			"svg--icon",
			"svg--$name"
		]);
		
		printf(
			'<svg class="%s">
				<use xlink:href="#%s"></use>
			</svg>',
			$this->classnames(),
			$name
		);
		
	}
	
	public function detect() {
		
		return [
			"name" => "default"
		];
	}
	
}