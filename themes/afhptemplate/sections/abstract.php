<?php
/**
 * Abstract parent class for sections
 *
 * Class AFHPSection
 */
abstract class AFHPSection implements AFHPSectionInterface {

	/**
	 * Section name
	 */
	const NAME = 'abstract';

	/**
	 * Section configs
	 *
	 * @var stdClass
	 */
	public $config;

	/**
	 * @var WP_Query|array
	 */
	public $result;

	/**
	 * @var array => ['section', ...]
	 */
	public $classNames = ['section'];
	
	/**
	 * Section root tag attributes
	 *
	 * @var array
	 */
	public $attributes;

	/**
	 * Force disable empty template output in empty data
	 *
	 * @var bool
	 */
	public $disableEmptyTemplate = FALSE;
	
	/**
	 * Section content wrapper
	 *
	 * @var string
	 */
	public $wrapper;
	
	/**
	 * AFHPSection constructor.
	 *
	 * @param stdClass|null $config
	 * @param null $result
	 */
	public function __construct(stdClass $config = NULL, $result = NULL) {

		$this->result = $result ? $result : NULL;
		$this->config = $config ? $config : new stdClass();
		$this->attributes = $this->config("attributes", []);
		$this->wrapper = $this->config("wrapper", "%s");
		
		$this->addClass("section--" . $this::NAME);

		do_action(tmp__prefix($this::NAME . '_before_fetch'), $this->config);

		if (!$this->result)
			$this->fetch();

		do_action(tmp__prefix($this::NAME . '_after_fetch'), $this->result, $this->config);
	}

	/**
	 * Simple fetching handle
	 * Fetching data from WP_Query by $this->config
	 * Returning and saving data in $this->result
	 *
	 * @return array|WP_Query
	 */
	public function fetch() {

		return $this->result =
							$this->config('query', FALSE) ?
								new WP_Query($this->config('query')) :
									NULL;
	}
	
	/**
	 * Returned template output
	 *
	 * @param string $output
	 *
	 * @return false|string
	 */
	public function content($output = "") {

		ob_start();
		
		if (!empty($output))
			print is_callable($output) ? $output() : $output;
		else
			print $this->is__empty() ? $this->template__empty() : $this->template();

		return ob_get_clean();
	}

	/**
	 * Section empty template
	 */
	public function template__empty() {

		printf(
			'<div class="alert alert-light text-center p-5 h3">%s</div>',
			__("No data yet", TMP__LANG)
		);
	}

	/**
	 * Section template
	 */
	public abstract function template();

	/**
	 * Section heading
	 *
	 * @return false|string|null
	 */
	public function heading() {

		if (!empty($this->config('heading'))) {
			
			$heading = $this->config("heading");
			if (!empty($heading->output)) {
				return $heading->output;
			} else {
				return AFHPComponent::import('heading', (object)[
					"output" => "return"
				])->render($heading);
			}
		}

		return "";
	}

	/**
	 * Section buttons
	 *
	 * @return string
	 */
	public function buttons() {

		$output = "";

		if (empty($this->config('buttons')))
			return "";

		$default = ["output" => "return"];
		
		if ($this->config("buttonTheme"))
			$default["theme"] = $this->config("buttonTheme");
		else if ($this->config("theme"))
			$default["theme"] = $this->config("theme");
		
		foreach ($this->config('buttons') as $item) {
			$config = array_merge(
				!empty($item["config"]) ? $item["config"] : [],
				$default
			);

			$button = AFHPComponent::import('button', (object)$config);
			$output .= $button->render(
				!empty($item["data"]) ? $item["data"] : NULL
			);
		}

		return sprintf('<div class="section__buttons d-flex justify-content-center mt-4">%s</div>', $output);
	}
	
	/**
	 * Before output handler
	 */
	public function beforeOutput() {}
	
	/**
	 * Section output
	 *
	 * @param string $output
	 *
	 * @return false|mixed|string|null
	 */
	public function output($output = "") {
		
		$this->beforeOutput();

		if ($this->config('output') === 'return')
			ob_start();

		printf(
			'<section class="%s" %s>
				<div class="container">
					%s
					<div class="section__content">%s</div>
					%s
				</div>
			</section>',
			$this->classNames(),
			$this->attributes(),
			$this->heading(),
			sprintf(
				$this->wrapper,
				$this->content($output)
			),
			$this->buttons()
		);

		if ($this->config('output') === 'return')
			return ob_get_clean();

		return NULL;
	}

	/**
	 * Adding section class
	 *
	 * @param $className
	 *
	 * @return array|string[]
	 */
	public function addClass($className) {

		if (is_string($className))
			$this->classNames[] = $className;

		if (is_array($className))
			array_merge($className, $this->classNames);

		return $this->classNames;
	}

	/**
	 * Section html classes
	 * default => section
	 * Can be added by config->classNames string|array
	 *
	 * @return string
	 */
	public function classNames() {
		
		$theme = $this->config("theme");
		$additional = $this->config('class', []);

		if (!is_array($additional))
			$additional = [$additional];
		
		$this->classNames = array_merge($additional, $this->classNames);

		if (!empty($theme))
			$this->classNames[] = "section--$theme";
		
		$this->classNames = array_unique($this->classNames);
		
		$this->classNames = apply_filters(tmp__prefix($this::NAME . '_classnames'), $this->classNames);

		return implode(' ', $this->classNames);
	}
	
	/**
	 * Setting attributes to root node
	 *
	 * @param string|array $arg1
	 * @param mixid|null $arg2
	 *
	 * @return array
	 */
	public function setAttr($arg1, $arg2 = NULL) {
		
		if (is_array($arg1))
			$this->attributes = array_merge($arg1, $this->attributes);
		
		if (is_string($arg1)) {
			$arg2 = is_array($arg2) ? implode(",", $arg2) : $arg2;
			
			$this->attributes[$arg1] = $arg2;
		}
		
		return $this->attributes;
	}
	
	/**
	 * Attributes by html format
	 *
	 * @return string
	 */
	public function attributes() {
		
		if (empty($this->attributes)) return "";
		
		$attrs = [];
		foreach ($this->attributes as $key => $value) {
			
			$attrs[] = "$key=\"$value\"";
		}
		
		return implode(" ", $attrs);
	}

	/**
	 * Getting config data from $this->config
	 *
	 * @param string $key
	 * @param null $default
	 * @param stdClass|null $object
	 *
	 * @return mixed|null
	 */
	public function config(string $key, $default = NULL, stdClass $object = NULL) {

		$config = $object ? $object : $this->config;

		return isset($config->$key) ? $config->$key : $default;
	}

	/**
	 * Checking data status
	 * true => empty
	 * false => no empty
	 *
	 * @return array|bool|mixed|WP_Query|null
	 */
	public function is__empty() {

		if ($this->disableEmptyTemplate) return FALSE;

		if ($this->result instanceof WP_Query)
			return !$this->result->have_posts();

		if (is_array($this->result))
			return empty($this->result);

		return empty($this->result);
	}

	public static function include(string $name, stdClass $config = NULL, $result = NULL) {

		require__once("sections/$name.php");

		$section = "AFHPSection" . ucfirst($name);

		if (class_exists($section))
			return new $section($config, $result);

		return print system__message(
			"Error [404] <b>$name</b> section not found",
			"warning"
		);
	}

}