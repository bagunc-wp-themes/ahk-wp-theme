<?php
class AFHPSectionDonate extends AFHPSection {

	/**
	 * Section name
	 */
	const NAME = "donate";
	
	public $disableEmptyTemplate = TRUE;
	
	public $radios = [1000, 2000, 5000];
	
	public function beforeOutput() {
		$bg = tmp__customize("donation__section__banner");
		
		if (!empty($bg))
			$this->setAttr("style", "background-image: url($bg)");
		
		$this->config->theme = "secondary";
	}
	
	public function cards() {

		$cards = [
			"Visa" => "img/donate/visa.png",
			"Mastercard" => "img/donate/master.png",
			"Maestro" => "img/donate/maestro.png",
		];

		$output = "";

		foreach ($cards as $title => $card)
			$output .= sprintf('<img src="%s" alt="%s" />', tmp__assets($card), $title);

		return sprintf('<div class="submit__cards">%s</div>', $output);
	}
	
	public function radios() {
		
		$output = [];
		
		foreach ($this->radios as $radio)
			$output[] = sprintf(
				'<div class="col-auto col--radio">%s</div>',
				AFHPComponent::import("radio", (object)[
					"output" => "return",
				])->render([
					"value" => $radio,
					"name" => "price",
					"label" => sprintf('%d <span class="currency">֏</span>', $radio),
				])
			);
		
		return implode("", $output);
	}
	
	public function template() {
		
		$heading = AFHPComponent::import('heading', (object)[
			'output' => "return"
		]);
		
		$title = trim((string)get_post_meta(get_option('page_on_front'), "donate__title", TRUE));
		$description = trim((string)get_post_meta(get_option('page_on_front'), "donate__desc", TRUE));

		printf(
			'<div class="row">
        <div class="col-auto ml-auto">
          <form class="donate">
            %s
            <div class="section__content">
              <div class="donate__description">
                <div class="description__promise">%s</div>
                <div class="description__info">%s</div>
              </div>
              <div class="donate__prices">
              	<div class="row no-gutters">
	                %s
	                <div class="col col--input">
		                <div class="prices__input">
		                  <input type="text" placeholder="%s" />
		                  <div class="currency">֏</div>
		                </div>
	                </div>
                </div>
              </div>
              <div class="donate__submit d-flex justify-content-between align-items-center">
                %s
                <button class="submit__button">%s</button>
              </div>
            </div>
          </form>
        </div>
      </div>',
			$heading->render([
				"color" => "#FF6504",
				"text" => __("Donation", TMP__LANG),
			]),
			$title,
			$description,
			$this->radios(),
			__("other", TMP__LANG),
			$this->cards(),
			__("Donate", TMP__LANG)
		);

	}

}