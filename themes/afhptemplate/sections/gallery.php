<?php
class AFHPSectionGallery extends AFHPSection {

	/**
	 * Section name
	 */
	const NAME = "gallery";

	/**
	 * @var WP_Query
	 */
	public $video;

	/**
	 * @var WP_Query
	 */
	public $gallery;

	/**
	 * AFHPSectionGallery constructor.
	 *
	 * @param stdClass|null $config
	 * @param null $result
	 */
	public function __construct(stdClass $config = NULL, $result = NULL) {
		parent::__construct((object)[
			"theme" => "flag__1",

			"heading" => [
				"text" => __("Gallery", TMP__LANG)
			],

			"buttons" => [
				[
					"data" => [
						"text" => __("More", TMP__LANG),
						"attrs" => [
							"href" => get_post_type_archive_link("gallery")
						],
					]
				],
			],
		]);
	}

	public function fetch() {

		$this->video = new WP_Query([
			"post_type" => "playlist",
			"post_status" => "publish",

			"orderby" => "rand",
			"lazyloop" => FALSE,
			"posts_per_page" => 1,
		]);

		$this->gallery = new WP_Query([
			"post_type" => "gallery",
			"post_status" => "publish",

			"lazyloop" => FALSE,
			"orderby" => "rand",
			"posts_per_page" => 6,
		]);

		$this->result = (object)[
			"gallery" => $this->gallery,
			"video" => $this->video
		];
	}

	public function template() {

		$output = "";

		$photo = AFHPComponent::import("gallery", (object)[
			"dens" => FALSE,
			"style" => "preview",
			"output" => "return",
			"template" => "photo",
			"classnames" => ["flex-grow-1", "col--gallery-item"],
		]);

		$video = AFHPComponent::import("gallery", (object)[
			"dens" => FALSE,
			"ajax" => FALSE,
			"output" => "return",
			"style" => "preview",
			"template" => "video",
			"classnames" => ["flex-grow-2", "col--gallery-item"],
		]);

		$items = [];
		while ($this->gallery->have_posts()) : $this->gallery->the_post();
				$items[] = $photo->render();
		endwhile;
		$this->gallery->reset_postdata();

		while ($this->video->have_posts()) : $this->video->the_post();
			$items[] = $video->render();
		endwhile;
		$this->video->reset_postdata();

		$cols = [
			0 => [
				"col" => 4,
				"count" => 1,
				"start" => 0,
			],
			2 => [
				"col" => 3,
				"count" => 2,
				"start" => 2,
			],
			5 => [
				"col" => 5,
				"count" => 2,
				"start" => 5,
			]
		];

		$col = NULL;
		foreach ($items as $index => $item) {

			if (isset($cols[$index]))
				$col = $cols[$index];

			$output .= sprintf(
				'%s
				%s
				%s',
				$col && $col["start"] === $index ? sprintf('<div class="col-6 col-lg-%d col--gallery"><div class="d-flex flex-column h-100">', $col["col"]) : '',
				$item,
				$col ? $col["start"] + $col["count"] === $index ? '</div></div>' : '' : ''
			);
		}

		printf(
			'<div class="gallery-grid">
      	<div class="row row--gallery">%s</div>
      </div>',
			$output
		);

	}

}
