<?php
class AFHPSectionStructure extends AFHPSection {

	/**
	 * Section name
	 */
	const NAME = "structure-posts";

	/**
	 * AFHPSectionNews constructor.
	 *
	 * @param stdClass|null $config
	 * @param null $result
	 */
	public function __construct(stdClass $config = NULL, $result = NULL) {

		$query__object = get_queried_object();

		$query = [
			"post_type" => "structure",
			"post_status" => "publish",
		];

		if ($query__object instanceof WP_Term)
			$query["tax_query"] = [
				[
					"field" => "term_id",
					"include_children" => FALSE,
					"terms" => $query__object->term_id,
					"taxonomy" => $query__object->taxonomy,
				]
			];

		$paged = (int)get_query_var('paged');
		$paged = $paged ? $paged : 1;

		if (!is_req_ajax()) {
			$query["paged"] = 1;
			$query["posts_per_page"] = $paged * get_the_per_page();
		} else {
			$query["paged"] = $paged;
		}

		parent::__construct((object)[
			"query" => $query
		]);

		if ($this->result instanceof WP_Query) {
			$query["paged"] += 1;

			$show__button = (is_req_ajax() && $this->result->max_num_pages > $paged) ||
											(!is_req_ajax() && $this->result->max_num_pages > 1);

			if ($show__button) {
				$this->config->buttons = [
					[
						"config" => [
							"style" => "more",
						],
						"data" => [
							"attrs" => [
								"class" => ["button--ajax"],
								"id" => "structures__loader",
								"href" => get_pagenum_link($paged + 1),
								"data-node" => ".section--structure-posts .section__content > .row",
							],
						]
					],
				];
			}
		}

	}

	public function template__empty() {

		printf(
			'<div class="section__empty text-center">
				<div class="empty__icon">
					<svg>
						<use xlink:href="#diagram"></use>
					</svg>
				</div>
				<h4>%s</h4>
			</div>',
			__('There is no data here yet.', TMP__LANG)
		);

	}

	public function template() {

		$output = "";

		$loop = AFHPComponent::import('post', (object)[
			"output" => "return"
		]);

		if ($this->result instanceof WP_Query) :
			while ($this->result->have_posts()) : $this->result->the_post();
				$output .= sprintf(
					'<div class="col-12 col-lg-6 mb-4">%s</div>',
					$loop->render((object)[
						"date" => get_the_date(),
						"title" => get_the_title(),
						"url" => get_the_permalink(),
						"excerpt" => get_the_excerpt(),
						"thumbnail" => get_the_post_thumbnail_url(),
					])
				);
			endwhile;
		endif;

		printf('<div class="row">%s</div>', $output);

	}

}