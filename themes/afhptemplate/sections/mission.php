<?php
class AFHPSectionMission extends AFHPSection {

	/**
	 * Section name
	 */
	const NAME = "our-mission";

	public function __construct(stdClass $config = NULL, $result = NULL) {
		parent::__construct((object)[
			"theme" => "flag__2",
			
			"query" => [
				"post_type" => "mission",
				"post_status" => "publish",
				
				"lazyloop" => FALSE,
				"posts_per_page" => -1,
				
				"order" => "ASC",
				"meta_key" => "index",
				"orderby" => "meta_value",
			],
			
			"heading" => (object)[
				"text" => __("Our mission", TMP__LANG),
			]
		]);
		
		$this->wrapper = '<div class="row">%s</div>';
	}

	public function template() {

		$mission = AFHPComponent::import('mission', (object)[
			"wrapper" => '<div class="col col-12 col-md-6 col-lg-4 mb-3">%s</div>'
		]);

		$output = "";

		if ($this->result instanceof WP_Query) :

			while ($this->result->have_posts()) : $this->result->the_post();
				$mission->render();
			endwhile;
			
			$this->result->reset_postdata();

		endif;
	}

}