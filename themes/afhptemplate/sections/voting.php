<?php
class AFHPSectionVoting extends AFHPSection {
	
	const NAME = "contact-voting";
	
	public function template () {
		printf(
			/** @l  */
			'<div class="row align-items-center no-gutters">
        <button class="button button--vote button--pros" data-id="%d">%s</button>
        <div class="voting-bar d-flex flex-row flex-fill">
          <div class="bar__pros text-center text-white" style="width: %d%%">%d%%</div>
          <div class="bar__cons text-center text-white" style="width: %d%%">%d%%</div>
        </div>
        <button class="button button--vote button--cons" data-id="%d">%s</button>
      </div>',
			$this->result->ID,
			$this->yes(),
			$this->result->yes,
			$this->result->yes,
			$this->result->no,
			$this->result->no,
			$this->result->ID,
			$this->no()
		);
	
	}
	
	private function no() {
		
		return $this->result->voted === "no" ?
			'<svg class="svg svg--icon svg--close">
				<use xlink:href="#close"></use>
			</svg>' : __("cons", TMP__LANG);
	}
	
	private function yes() {
		
		return $this->result->voted === "yes" ?
			'<svg class="svg svg--icon svg--tick">
				<use xlink:href="#tick"></use>
			</svg>' : __("pros", TMP__LANG);
	}
	
}