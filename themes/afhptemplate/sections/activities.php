<?php
class AFHPSectionActivities extends AFHPSection {
	
	/**
	 * Section name
	 */
	const NAME = "activities";
	
	/**
	 * Current term object
	 *
	 * @var WP_Term
	 */
	public $term;
	
	/**
	 * AFHPSectionActivities constructor.
	 *
	 * @param stdClass|null $config
	 * @param null $result
	 */
	public function __construct (stdClass $config = NULL, $result = NULL) {
		
		$this->term = $config->term;
		
		$config->query = [
			"post_type" => "activity",
			"post_status" => "publish",
			
			"lazyloop" => FALSE,
			"posts_per_page" => $this->term->count > 4 ? 3 : 4,
			
			"tax_query" => [
				[
					"field" => "term_id",
					"terms" => $this->term->term_id,
					"taxonomy" => $this->term->taxonomy,
				]
			]
		];
		
		parent::__construct($config, $result);
	}
	
	/**
	 * Section template
	 */
	public function template () {
		
		$output = "";
		
		$component = AFHPComponent::import("post", (object)[
			"output" => "return",
			"template" => "thumbnail",
		]);
		
		if ($this->result->have_posts()) {
			while ($this->result->have_posts()) : $this->result->the_post();
				$output .= sprintf(
					'<div class="col-xl-3 col-lg-4 col-6 mb-4">%s</div>',
					$component->render()
				);
			endwhile;
			wp_reset_postdata();

			if ($this->term->count > 4) {
				$button = AFHPComponent::import("button", (object)[
					"output" => "return",
				]);
				
				$output .= sprintf(
					'<div class="col-xl-3 col-lg-4 col-6 mb-4">%s</div>',
					$button->render([
						"text" => __("More", TMP__LANG),
						"attrs" => [
							"href" => get_term_link(
								$this->term->term_id,
								$this->term->taxonomy
							),
							"class" => "button--squire"
						]
					])
				);
			}
		}
		
		printf(
			'<div class="row">%s</div>',
			$output
		);
	}
}