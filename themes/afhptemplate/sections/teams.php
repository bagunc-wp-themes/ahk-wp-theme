<?php
class AFHPSectionTeams extends AFHPSection {
	
	/**
	 * Section name
	 */
	const NAME = "team-list";
	
	/**
	 * @var WP_Term[] $result
	 */
	
	/**
	 * AFHPSectionTeams constructor.
	 *
	 * @param stdClass|null $config
	 * @param null $result
	 */
	public function __construct (stdClass $config = NULL, $result = NULL) {
		parent::__construct($config, get_terms([
			"hide_empty" => TRUE,
			"taxonomy" => get_object_taxonomies("team"),
		]));
	}
	
	public function template () {
		
		$output = "";
		$button = AFHPComponent::import("button", (object)[
			"overlay" => TRUE,
			"output" => "return",
			"wrapper" => '<div class="col-xl-3 col-lg-4 col-6 mb-sm-3 mb-2">%s</div>'
		]);
		
		if (!empty($this->result))
			foreach($this->result as $term)
				$output .= $button->render([
					"text" => $term->name,
					"attrs" => [
						"href" => get_term_link($term->term_id, $term->taxonomy),
						"class" => [
							"button--squire",
							"button--squire-alt",
						]
					]
				]);
			
		printf(
			'<div class="row row--list">%s</div>',
			$output
		);
	}
	
}