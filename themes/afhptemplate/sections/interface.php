<?php

interface AFHPSectionInterface {

	/**
	 * AFHPSectionInterface constructor.
	 *
	 * @param stdClass $config
	 * @param $result
	 */
	public function __construct(stdClass $config, $result);

	/**
	 * Simple fetching handle
	 * Fetching data from WP_Query by $this->config
	 * Returning and saving data in $this->result
	 *
	 * @return array|WP_Query
	 */
	public function fetch();

	/**
	 * Returned template output
	 *
	 * @return false|string
	 */
	public function content();

	/**
	 * Section template
	 */
	public function template();
	
	/**
	 * Section output
	 *
	 * @param string $output
	 *
	 * @return mixed
	 */
	public function output($output = "");

	/**
	 * Adding section class
	 *
	 * @param $className
	 *
	 * @return array|string[]
	 */
	public function addClass($className);

	/**
	 * Section html classes
	 * default => section
	 * Can be added by config->classNames string|array
	 *
	 * @return string
	 */
	public function classNames();

	/**
	 * Getting config data from $this->config
	 *
	 * @param string $key
	 * @param null $default
	 * @param stdClass|null $object
	 *
	 * @return mixed|null
	 */
	public function config(string $key, $default = NULL, stdClass $object = NULL);

}