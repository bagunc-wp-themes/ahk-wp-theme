<?php
class AFHPSectionDefault extends AFHPSection {
	
	const NAME = "default";

  public function template ($output = "") {
		
  	if (is_callable($output))
  		return print $output();
  	
  	return print $output;
	}

}