<?php
class AFHPSectionAlbums extends AFHPSection {
	
	/**
	 * Section name
	 */
	const NAME = "gallery-categories";
	
	/**
	 * Fetching section result
	 *
	 * @return array|void|WP_Query|null
	 */
	public function fetch () {
		
		$this->result = get_terms($this->config("query"));
		
	}
	
	/**
	 * Section template
	 */
	public function template () {
		$output = "";
		
		$post_type = $this->config("post_type", get_post_type());
		
		foreach ($this->result as $item) {
			
			$latest = get_posts([
				"post_type" => $post_type,
				"tax_query" => [
					[
						"field" => "term_id",
						"terms" => $item->term_id,
						"include_children" => FALSE,
						"taxonomy" => $item->taxonomy,
					],
				],
			]);
			
			if (count($latest))
				$item->date = get_the_date('', $latest[0]->ID);
			
			$output .= AFHPComponent::import("album", (object)[
				"output" => "return",
				"wrapper" => '<div class="col-12 col-md-6 col-xl-4 mt-4">%s</div>',
			])->render($item);
		}
		
		printf(
			'<div class="row mt-2">%s</div>',
			$output
		);
	}
	
}