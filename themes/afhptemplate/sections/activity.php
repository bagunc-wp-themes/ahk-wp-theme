<?php
/**
 * Section Activity
 *
 * Class AFHPSectionActivity
 */
class AFHPSectionActivity extends AFHPSection {

	/**
	 * Section name
	 */
	const NAME = "our-activity";

	/**
	 * AFHPSectionActivity constructor.
	 *
	 * @param stdClass|null $config
	 * @param null $result
	 */
	public function __construct(stdClass $config = NULL, $result = NULL) {
		parent::__construct((object)[
			"theme" => "flag__3",

			"heading" => [
				"text" => __("Our activity", TMP__LANG)
			],

			"buttons" => [
				[
					"data" => [
						"text" => __("More", TMP__LANG),
						"attrs" => [
							"href" => get_post_type_archive_link("activity")
						],
					],
				],
			],
		]);
	}

	public function fetch() {

		$this->result = [];

		$terms = get_terms([
			"hide_empty" => TRUE,
			"taxonomy" => "activity__category",

			"order" => "DESC",

			"meta_query" => [
				[
					"value" => 1,
					"key" => "show_in_front",
				]
			],
		]);

		if (!empty($terms))
			foreach ($terms as $term)
				$this->result[] = new WP_Query([
					"post_type" => "activity",
					"post_status" => "publish",

					"lazyloop" => FALSE,
					"posts_per_page" => 3,

					'tax_query' => [
						[
							'field' => 'term_id',
							'terms' => $term->term_id,
							'taxonomy' => 'activity__category',
						],
					],
				]);

	}

	public function template() {

		$post = AFHPComponent::import('post', (object)[
			"output" => "return",

			"wrapper" => '<div class="wrapper--post col-12 col-md-6 col-xl-12">%s</div>',

			"thumbnail_classnames" => "col-12 col-xl-4",
		]);

		$carousel = AFHPComponent::import('carousel', (object)[
			"output" => "return"
		]);

		$output = new stdClass();

		if (!empty($this->result)) :

			$output->posts = "";
			$output->carousel = $carousel->render(array_shift($this->result));

			foreach ($this->result as $key => $query) :

				while ($query->have_posts()) : $query->the_post();
					$term = NULL;

					$category = get_the_terms(get_the_ID(), 'activity__category');
					if (!empty($category))
						$term = (object)[
							"name" => $category[0]->name,
							"color" => get_term_meta($category[0]->term_id, 'color', TRUE)
						];

					$output->posts .= $post->render([
						"term" => $term,
						"date" => get_the_date(),
						"title" => get_the_title(),
						"url" => get_the_permalink(),
						"excerpt" => get_the_excerpt(),
						"thumbnail" => get_the_post_thumbnail_url(),
					]);

				endwhile;

				wp_reset_postdata();

			endforeach;

		endif;

		printf(
			'<div class="row">
          <div class="col-12 col-xl-6 mb-4 m-xl-0">%s</div>
          <div class="col-12 col-xl-6 d-xl-block">
          	<div class="d-flex flex-wrap flex-xl-column">%s</div>
          </div>
      </div>',
			$output->carousel,
			$output->posts
		);

	}

}
