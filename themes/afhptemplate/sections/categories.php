<?php
class AFHPSectionCategories extends AFHPSection {
	
	const NAME = "categories";
	
	public function fetch () {
		$this->wrapper = '<div class="row">%s</div>';
		$this->addClass("section--blog-white");
		
		$this->result = get_terms([
			"hide_empty" => TRUE,
			"taxonomy" => "category",
		]);
		
	}
	
	public function template () {
		
		/**
		 * @var WP_Term $item
		 */
		if (!empty($this->result))
			foreach ($this->result as $item)
				AFHPComponent::import("highlighted", (object)[
					"wrapper" => '<div class="col-6 col-sm-4 mb-4">%s</div>'
				])->render([
					"name" => $item->name,
					"url" => get_term_link($item->term_id, $item->taxonomy),
					"color" => get_term_meta($item->term_id, "color", TRUE),
				]);
		
	}
	
	
}