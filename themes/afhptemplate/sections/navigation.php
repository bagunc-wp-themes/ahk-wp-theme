<?php
class AFHPSectionNavigation extends AFHPSection {

	const NAME = "navigation";

	public $disableEmptyTemplate = TRUE;

	public $hierarchical = TRUE;
	
	public $itemClassnames;
	
	/**
	 * Getting terms childes || parent
	 * config => [parent, term_id, taxonomy]
	 *
	 * @return array|object[]|WP_Query
	 */
	public function fetch() {
		
		$this->hierarchical = $this->config("hierarchical", TRUE);
		
		$this->result = $this->hierarchical ? get_terms([
			"hide_empty" => TRUE,
			"taxonomy" => $this->config("taxonomy"),

			"parent" => $this->config("term_id")
		]) : [];

		if (empty($this->result))
			$this->result = get_terms([
				"hide_empty" => TRUE,
				"taxonomy" => $this->config("taxonomy"),

				"parent" => $this->config("parent", 0)
			]);

		if ($this->result instanceof WP_Error) {
			foreach ($this->result->errors as $key => $error)
				print system__message(
					"Error <b>[$key]</b> {$this->config("taxonomy")} " . implode(", ", $error),
					"danger"
				);

			return $this->result = [];
		}

		return $this->result = array_map(function (WP_Term $item) {

			return (object)[
				"text" => $item->name,
				"url" => get_term_link($item),
				"active" => $item->term_id === $this->config("term_id")
			];
		}, $this->result);

	}

	public function template() {
		
		$this->itemClassnames = $this->config("itemClassnames", "col-md-4 col-sm-6 col-12 mb-3");
		
		$output = "";

		if (is_array($this->result))
			foreach ($this->result as $item)
				$output .= sprintf(
					'<div class="%s">
						<a class="%s" href="%s">
							<span class="link__before">›</span>
							<span class="link__text">%s</span>
						</a>
					</div>',
					$this->itemClassnames,
					'navigation__link d-flex' . (isset($item->active) && $item->active ? ' active' : ''),
					$item->url,
					$item->text
				);

		printf('<div class="row justify-content-between">%s</div>', $output);
	}

}