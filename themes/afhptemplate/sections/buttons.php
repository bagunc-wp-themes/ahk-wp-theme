<?php
class AFHPSectionButtons extends AFHPSection {

	const NAME = "party-links";

	public $disableEmptyTemplate = TRUE;

	private function html() {

		if (empty($this->result)) return "";

		$output = "";
		$button = AFHPComponent::import("button", (object)[
			"style" => "icon",
			"output" => "return",
		]);

		foreach ($this->result as $item) {
			$ext = "book";
			$info = pathinfo(isset($item->attrs) ? $item->attrs["href"] : "");

			if (isset($info["extension"]))
				$ext = $info["extension"] === "pdf" ? "pdf" : "book";

			$item->icon = sprintf(
				'<svg class="svg svg--icon svg--%s">
					<use xlink:href="#%s"></use>
				</svg>',
				$ext,
				$ext
			);

			$output .= $button->render($item);
		}

		return $output;
	}

	public function template() {

		printf('<div class="row">%s</div>', $this->html());
	}

}