<?php
class AFHPSectionNews extends AFHPSection {

	/**
	 * Section name
	 */
	const NAME = "latest-news";

	/**
	 * AFHPSectionNews constructor.
	 *
	 * @param stdClass|null $config
	 * @param null $result
	 */
	public function __construct(stdClass $config = NULL, $result = NULL) {
		parent::__construct((object)[
			"theme" => "flag__2",

			"heading" => [
				"text" => __("News", TMP__LANG),
				"color" => "blue",
			],

			"buttons" => [
				[
					"data" => [
						"text" => __("More", TMP__LANG),
						"attrs" => [
							"href" => get_post_type_archive_link('post')
						],
					]
				],
			],
		]);
	}

	public function fetch() {

		$this->result = get_terms([
			"hide_empty" => "TRUE",
			"taxonomy" => "category",

			"number" => 4,
			"exclude" => 1,
			"order" => "DESC",
		]);

	}

	public function posts(stdClass $term) {

		$result = new WP_Query([
			"post_type" => "post",
			"post_status" => "publish",

			"lazyloop" => FALSE,
			"posts_per_page" => 5,

			"cat" => $term->term_id,
			"cat__not_in" => 1
		]);

		ob_start();

		$post = AFHPComponent::import('post', (object)[
			"template" => "thin",
			"output" => "return",
		]);

		while ($result->have_posts()) : $result->the_post();

			printf(
				'<div class="mb-2">%s</div>',
				$post->render([
					"term" => $term,
					"date" => get_the_date(),
					"title" => get_the_title(),
					"url" => get_the_permalink(),
					"excerpt" => get_the_excerpt(),
					"thumbnail" => get_the_post_thumbnail_url(),
				])
			);

		endwhile;
		wp_reset_postdata();

		return ob_get_clean();

	}

	public function col(WP_Term $term) {

		$color = get_term_meta($term->term_id, 'color', TRUE);

		return sprintf(
			'<div class="col-12 col-sm-6 col-lg-4 col-xl-3">
        <div class="group">
          <div class="group__title" style="color: %s">%s</div>
        </div>
				%s
			</div>',
			$color ? $color : "#999",
			$term->name,
			$this->posts((object)[
				"color" => $color,
				"name" => $term->name,
				"term_id" => $term->term_id,
			])
		);
	}

	public function template() {

		$output = "";

		foreach ($this->result as $term)
			$output .= $this->col($term);

		printf('<div class="row">%s</div>', $output);

	}

}
