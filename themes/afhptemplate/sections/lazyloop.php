<?php
class AFHPSectionLazyloop extends AFHPSection {
	
	const NAME = "lazyloop";
	
	public static $called = 0;
	
	public $component;
	
	public $disableEmptyTemplate = TRUE;
	
	public function __construct (stdClass $config = NULL, $result = NULL) {
		
		global $wp_query;
		
		$paged = (int)get_query_var('paged');
		if (!$paged)
			$paged = !empty($wp_query->get('original_paged')) ?
				$wp_query->get('original_paged') : 1;
		
		$max_num_pages = $wp_query->max_num_pages;
		
		if (!empty($config->query)) {
			if (!is_req_ajax()) {
				$config->query["paged"] = 0;
				$config->query["posts_per_page"] = $paged * get_the_per_page();
			} else {
				$config->query["paged"] = $paged;
			}
		}
		
		parent::__construct($config, $result);
		
		$this->component = $this->config("component", NULL);
		
		if ($this->result instanceof WP_Query)
			$max_num_pages = $this->result->max_num_pages;
		
		$show__button = (is_req_ajax() && $max_num_pages > $paged) ||
			(!is_req_ajax() && $max_num_pages > 1);
		
		if ($show__button) {
			$this->config->buttons = [
				[
					"config" => [
						"style" => "more",
						"theme" => $this->config("buttonTheme"),
					],
					"data" => [
						"attrs" => [
							"class" => ["button--ajax"],
							"data-node" => $this->config("node"),
							"href" => get_pagenum_link($paged + 1),
							"id" => tmp__prefix(sprintf(
								"%s__%s__%s",
								$this::NAME,
								isset($this->config("query")["post_type"]) ?
									$this->config("query")["post_type"] : get_post_type(),
								self::$called
							)),
						],
					],
				],
			];
		}
		
		self::$called++;
	}

	public function template () {
		
		if (empty($this->component)) {
			print system__message(
				sprintf(
					"Error [404] <b>%s</b> not have component for loop",
					$this::NAME
				),
				"warning"
			);
		} else {
			$loop = $this->component;
			if (is_object($loop) && !is_callable($loop))
				$loop = AFHPComponent::import(
					$this->component->name,
					isset($this->component->config) ?
						$this->component->config :
							new stdClass()
				);
			
			if ($this->result instanceof WP_Query) {
				$index = 0;
				while ($this->result->have_posts()) : $this->result->the_post();
					
					if ($loop instanceof AFHPComponent)
						print $loop->render($loop->detect());
					else if (is_callable($loop))
						print $loop($this->result->posts[$index]);
					
					$index++;
				endwhile;
				wp_reset_postdata();
			} else if (is_array($this->result)) {
				foreach ($this->result as $item)
					if ($loop instanceof AFHPComponent)
						print $loop->render($item);
					else if (is_callable($loop))
						print $loop($item);
			} else {
				while (have_posts()) : the_post();
					if ($loop instanceof AFHPComponent)
						print $loop->render();
					else if (is_callable($loop))
						print $loop($loop->detect());
				endwhile;
			}
		}
	}
	
}